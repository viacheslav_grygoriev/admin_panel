import formatDate from './formatDate';

// Returns date format which is appropriate for input with date format
export default function generateInputDate(date) {
  const time = formatDate(date, {
    year: 'numeric',
    month: '2-digit',
    day: '2-digit',
  }); // eslint-disable-line  space-in-parens
  const arrayWithSplittedDate = time.split('/');
  return `${arrayWithSplittedDate[2].slice(0, 4)}-${arrayWithSplittedDate[0]}-${
    arrayWithSplittedDate[1]
  }`;
}
