import Types from './Types';

export default (typesEntity, entitiesValue) => {
  if (!Number.isFinite(entitiesValue)) throw Error('Entity is not a number');
  return Object.keys(Types[typesEntity]).find(key => Types[typesEntity][key] === entitiesValue);
};
