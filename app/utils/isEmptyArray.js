export default array => typeof array !== 'undefined' && !!array.length;
