/*
I put CSS constants here (e.g. colors), which I use across all the project
*/

// colors
export const WHITE = '#fff';
export const GREEN = '#449d44';
export const LIGHT_GRAY = '#e5e5e5';
export const BLUE = 'rgba(51, 122, 183,1)';
export const NOT_ACTIVE_BLUE = 'rgba(51, 122, 183,0.5)';

// links colors
export const WHITE_LINK = '#fff';
export const WHITE_LINK_HOVER = '#ddd';
export const BLACK_LINK = '#777';
export const BLACK_LINK_HOVER = '#000';

// sidebar constants
export const SIDEBAR_BACKGROUND = '#f4f4f5';

// input constants
export const INPUT_BORDER = '#d2d6de';

// header
export const HEADER_BLACK = '#3a3943';

// feed
export const BIG_TABLE_HEIGHT = '50px';

// errors
export const ERROR_COLOR = '#ff462d';
