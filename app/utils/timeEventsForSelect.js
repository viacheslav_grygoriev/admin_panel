import formatDate from './formatDate';

export default timeEventsObject => timeEventsObject.map((timeEvent) => {
  const { EventId, AwayTeam, HomeTeam, StartDate, AwayAlias, HomeAlias } = timeEvent;
  const label = `${AwayTeam}@${HomeTeam} (${AwayAlias}@${HomeAlias}, ${formatDate(StartDate, {
    year: 'numeric',
    month: '2-digit',
    day: '2-digit',
  })})`;
  return { value: EventId, label };
});
