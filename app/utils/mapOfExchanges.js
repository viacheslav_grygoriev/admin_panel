/* eslint-disable */

export default function mapOfExchanges(arrayOfObjects, settings) {
  const resultMap = new Map();
  const {id, path = '', fields} = settings;
  arrayOfObjects.forEach((object) => {
    const mainPath = path ? object[path] : object;
    const resObj = {};
    fields.forEach(option => {
      resObj[option] = mainPath[option]
    });
    resultMap.set(mainPath[id], {
      ...resObj,
    });
  });
  return resultMap;
}
