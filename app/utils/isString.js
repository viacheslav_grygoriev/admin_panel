export default function isString(x) {
  return Object.prototype.toString.call(x) === '[object String]';
}
