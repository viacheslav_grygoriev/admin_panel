export default (longerList, smallerList) => longerList.filter(item => !smallerList.includes(Number(item)));

