export const RESTART_ON_REMOUNT = '@@saga-injector/restart-on-remount';
export const DAEMON = '@@saga-injector/daemon';
export const ONCE_TILL_UNMOUNT = '@@saga-injector/once-till-unmount';

// Links
// '10.20.35.91'
const HOSTNAME = __DEV__ ? 'http://10.20.35.218' : ''; // eslint-disable-line no-undef
export const DEFAULT_SITENAME = `${HOSTNAME}/Api/admin`;
export const TOKEN_URL = `${HOSTNAME}/api/token`;

export const GET_FEED_EVENTS_URL = `${DEFAULT_SITENAME}/Feed/getEvents`;
export const GET_TIME_EVENTS = `${DEFAULT_SITENAME}/Feed/GetTimeEvents`;
export const GET_PLAYERS = `${DEFAULT_SITENAME}/Feed/GetPlayers`;
export const GET_DEFENSE = `${DEFAULT_SITENAME}/Feed/GetDefense`;


export const GET_MENU_URL = `${DEFAULT_SITENAME}/category/getmenu`;
export const REMOVE_CATEGORY_URL = `${DEFAULT_SITENAME}/category/Delete`;

export const EXCHANGE_CONTROLLER_URL = `${DEFAULT_SITENAME}/Exchange`;
export const GET_NEWFEED_URL = `${EXCHANGE_CONTROLLER_URL}/CreateFantasy`;
export const GET_EXCHANGES_URL = `${EXCHANGE_CONTROLLER_URL}/Get`;
export const GET_EXCHANGE_URL = `${EXCHANGE_CONTROLLER_URL}/GetExchangeByName`;
export const SAVE_FANTASY_URL = `${EXCHANGE_CONTROLLER_URL}/AddFantasy`;
export const EDIT_FANTASY_URL = `${EXCHANGE_CONTROLLER_URL}/EditFantasy`;
export const UPDATE_FANTASY_URL = `${EXCHANGE_CONTROLLER_URL}/UpdateFantasy`;


export const SET_STATUS_SETTLEMENT = 'Settlement';
export const SET_STATUS_APPROVED = 'Approve';
export const SET_STATUS_SUSPENDED = 'Suspend';
export const SET_STATUS_REMOVED = 'Delete';

export const NOTIFICATION_TYPE_WARNING = 'WARNING';
export const NOTIFICATION_TYPE_ERROR = 'ERROR';
export const NOTIFICATION_TYPE_SUCCESS = 'SUCCESS';
