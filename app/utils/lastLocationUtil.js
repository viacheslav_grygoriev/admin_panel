export default (props, route) => (cb) => {
  const { lastLocation } = props;
  if (lastLocation) {
    cb(lastLocation.pathname);
  } else {
    cb(route);
  }
};
