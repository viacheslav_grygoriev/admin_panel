export default function (sortName) {
  const sortNames = ['Asc', 'Desc'];
  if (!sortNames.includes(sortName)) throw Error(`SortName function doesn't recognize ${sortName}`);
  return sortName === 'Asc' ? 'Desc' : 'Asc';
}
