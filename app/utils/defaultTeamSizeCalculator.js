export default (positionsArr) => {
  let amountOfPlayersInTeam = 0;
  positionsArr.forEach((position) => {
    amountOfPlayersInTeam += position.Quantity;
  });
  return amountOfPlayersInTeam;
};
