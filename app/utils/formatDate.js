export default function formatDate(date, customOpts, locale = 'en-US') {
  const dataOpts = {
    month: 'short',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
    ...customOpts,
  };
  let _date; // eslint-disable-line no-underscore-dangle
  let result;

  const formatter = new Intl.DateTimeFormat(locale, dataOpts);

  const re = /\/Date\(\d+\)\//;
  if (!date) return '';
  if (re.test(date)) {
    const re1 = /\d+/;
    const numericDate = Number(re1.exec(date)[0]);

    // _date = new Date(eval(`new ${date.replace(/\//g,'')}`))
    _date = new Date(new Date(numericDate).toJSON());
  } else {
    _date = new Date(date);
  }
  try {
    result = formatter.format(_date);
  } catch (error) {
    throw Error(error.message);
    // debugger
  }
  return result;
}
