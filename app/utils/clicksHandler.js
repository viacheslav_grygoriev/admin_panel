export default function getClickHandler(onClick, onDblClick, delayTime) {
  let timeoutID = null;
  const delay = delayTime || 250;
  return (event) => {
    if (!timeoutID) {
      timeoutID = setTimeout(() => {
        onClick(event);
        timeoutID = null;
      }, delay);
    } else {
      timeoutID = clearTimeout(timeoutID);
      onDblClick(event);
    }
  };
}
