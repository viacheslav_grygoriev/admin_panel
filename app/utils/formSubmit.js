export default action => (values, dispatch, props) => {
  dispatch(action(props.form, values, props));
};
