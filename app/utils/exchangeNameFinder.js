import Types from './Types';

export default (OptionExchange) => {
  switch (Number(OptionExchange)) {
    case Types.OptionExchange.Spread:
      return 'Spread';

    case Types.OptionExchange.Moneyline:
      return 'Moneyline';

    case Types.OptionExchange.TotalPoints:
      return 'Total Points';

    default:
      return '';
  }
};
