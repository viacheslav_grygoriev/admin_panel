export default {
  StatusEvent: {
    Scheduled: 0,
    Live: 1,
    Closed: 2,
    Cancelled: 3,
  },
  StatusExchange: {
    New: 0,
    Approved: 1,
    Suspended: 4,
    Ended: 2,
    Settlement: 3,
  },
  TypeEvent: {
    Sport: 1,
    // Full: 2,
    Fantasy: 3,
    FantasySport: 4,
  },
  Aliases: {
    Away: 1,
    Home: 2,
  },
  OptionExchange: {
    Spread: 0,
    Moneyline: 1,
    TotalPoints: 2,
  },
  ExchangeType: {
    SPREAD: 'Spread',
    MONEYLINE: 'Moneyline',
    TOTALPOINTS: 'TotalPoints',
  },
};
