import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import Select from 'react-select';

import H3 from './styledComponents/H3';
import H4 from './styledComponents/H4';
import FlexWrapper from '../../components/FlexWrapper';
import Button from './styledComponents/Button';
import { Types, namesComparator } from '../../utils';
import ExchangeButtonWithLimit from './ExchangeButtonWithLimit';
import {
  changeTypeEvent,
  changeTeamSize, changeExchangeLimit,
  changeDefenseState,
} from './actions';
import { typeEventSelector, teamSizeSelector, defensePressedSelector } from './selectors';
import FlexItem from '../../components/FlexItem';
import { FlexItemForSection, FlexWrapperRow } from './styledComponents/styledComponents';


const { OptionExchange: { Spread, Moneyline, TotalPoints }, TypeEvent: { Fantasy, FantasySport } } = Types;


class Options extends Component {
  teamSizeGenerator = (start, end) => {
    const res = [];
    for (let i = start; i <= end; i += 1) {
      res.push({ value: i, label: i });
    }
    return res;
  };
  isCurrentTypeEvent = (current) => {
    const { currentTypeEvent } = this.props;
    return namesComparator(currentTypeEvent, current);
  };

  render() {
    const {
      onChangeTeamSize, onChangeTypeEvent, teamSize, defensePressed, onChangeDefenseState,
    } = this.props;
    return (
      <FlexItemForSection>
        <H3>Options</H3>
        <FlexWrapperRow>
          <FlexItem>
            <H4>Number of teams</H4>
            <FlexWrapper>
              <Button
                pressed={this.isCurrentTypeEvent(FantasySport)}
                onClick={onChangeTypeEvent(FantasySport)}
              >Single Fantasy</Button>
              <Button
                pressed={this.isCurrentTypeEvent(Fantasy)}
                onClick={onChangeTypeEvent(Fantasy)}
              >Multiple Fantasy</Button>
            </FlexWrapper>
          </FlexItem>
          <FlexItem>
            <H4>Team size</H4>
            <Select
              options={this.teamSizeGenerator(2, 24)}
              value={{ value: teamSize, label: teamSize }}
              onChange={onChangeTeamSize}
            />
          </FlexItem>
        </FlexWrapperRow>
        <FlexWrapperRow>
          <FlexItem>
            <H4>Exchange types</H4>
            <FlexWrapper>
              <ExchangeButtonWithLimit
                buttonName="Spread"
                optionExchange={Spread}
              />
              <ExchangeButtonWithLimit
                buttonName="Moneyline"
                optionExchange={Moneyline}
              />
              <ExchangeButtonWithLimit
                buttonName="Total Points"
                optionExchange={TotalPoints}
              />
            </FlexWrapper>
          </FlexItem>
          <FlexItem width="auto">
            <H4>Defense</H4>
            <Button
              pressed={defensePressed}
              onClick={onChangeDefenseState}
            >
              Defense
            </Button>
          </FlexItem>
        </FlexWrapperRow>
      </FlexItemForSection>
    );
  }
}

Options.propTypes = {
  onChangeTeamSize: PropTypes.func.isRequired,
  onChangeTypeEvent: PropTypes.func.isRequired,
  currentTypeEvent: PropTypes.number.isRequired,
  teamSize: PropTypes.number,
  onChangeDefenseState: PropTypes.func.isRequired,
  defensePressed: PropTypes.bool.isRequired,
};

const mapStateToProps = createStructuredSelector({
  currentTypeEvent: typeEventSelector(),
  teamSize: teamSizeSelector(),
  defensePressed: defensePressedSelector(),
});

const mapDispatchToProps = dispatch => ({
  onChangeTypeEvent: data => () => dispatch(changeTypeEvent(data)),
  onChangeTeamSize: data => dispatch(changeTeamSize(data)),
  onChangeExhangeLimit: data => dispatch(changeExchangeLimit(data)),
  onChangeDefenseState: () => dispatch(changeDefenseState()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Options);
