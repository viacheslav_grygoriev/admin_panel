import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { ExchangeButtonWithLimitWrapper } from './styledComponents/styledComponents';
import Button from './styledComponents/Button';
import { optionExchangesSelector, exchangeTypeInitialSelector } from './selectors';
import { addExchangeTypes, changeExchangeLimit, removeExchangeTypes } from './actions';
import LabeledInput from '../../components/LabeledInput';


class ExchangeButtonWithLimit extends Component {
  constructor() {
    super();
    this.state = {};
    this.labeledInputRef = React.createRef();
  }

  onClickButton = () => () => {
    const { addExchange, removeExchange, optionExchange, exchangeTypeInitial } = this.props;
    if (this.isInOptionExchanges() && !exchangeTypeInitial.includes(optionExchange)) {
      removeExchange({ optionExchange });
    } else {
      addExchange({ optionExchange, exchangeValue: '1000' });
    }
  };
  isInOptionExchanges = () => {
    const { optionExchanges, optionExchange } = this.props;
    return optionExchanges.hasOwnProperty(optionExchange); // eslint-disable-line no-prototype-builtins
  };
  onChangeExhangeLimit = () => {
    const { changeExhangeLimit, optionExchange } = this.props;
    changeExhangeLimit({ exchangeValue: this.labeledInputRef.current.getData(), optionExchange });
  };

  render() {
    const { buttonName, optionExchange, optionExchanges, ...props } = this.props;
    return (
      <ExchangeButtonWithLimitWrapper>
        <Button
          onClick={this.onClickButton(optionExchange)}
          pressed={this.isInOptionExchanges()}
        >{buttonName}</Button>
        {this.isInOptionExchanges() &&
        <LabeledInput
          type="number"
          validator="^([0-9]|[1-8][0-9]|9[0-9]|[1-8][0-9]{2}|9[0-8][0-9]|99[0-9]|[1-8][0-9]{3}|9[0-8][0-9]{2}|99[0-8][0-9]|999[0-9])$"
          min="0"
          max="9999"
          value={optionExchanges[optionExchange]}
          name={`exchangeLimit${optionExchange}`}
          label={`${buttonName} Limit`}
          onChange={this.onChangeExhangeLimit}
          ref={this.labeledInputRef}
          autoFocus
          {...props}
        />}
      </ExchangeButtonWithLimitWrapper>
    );
  }
}

ExchangeButtonWithLimit.propTypes = {
  buttonName: PropTypes.string.isRequired,
  optionExchange: PropTypes.number.isRequired,
  addExchange: PropTypes.func.isRequired,
  removeExchange: PropTypes.func.isRequired,
  changeExhangeLimit: PropTypes.func.isRequired,
  optionExchanges: PropTypes.object.isRequired,
  exchangeTypeInitial: PropTypes.array,
};

const mapStateToProps = createStructuredSelector({
  optionExchanges: optionExchangesSelector(),
  exchangeTypeInitial: exchangeTypeInitialSelector(),
});

const mapDispatchToProps = dispatch => ({
  addExchange: data => dispatch(addExchangeTypes(data)),
  removeExchange: data => dispatch(removeExchangeTypes(data)),
  changeExhangeLimit: data => dispatch(changeExchangeLimit(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ExchangeButtonWithLimit);
