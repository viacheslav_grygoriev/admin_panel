/* eslint-disable */
import React from 'react';
import PropTypes from 'prop-types';
import Table from './styledComponents/Table';
import Tr from './styledComponents/Tr';
import Th from './styledComponents/Th';

const TableHead = ({ arrayOfHeaders, children }) => (
  <Table>
    <thead>
      <Tr bigTableHeading>
        {arrayOfHeaders.map((headerName, key) => headerName ? <Th key={key}>{headerName}</Th> : <Th key={key}/>)}
      </Tr>
    </thead>
    <tbody>{children}</tbody>
  </Table>
);

TableHead.propTypes = {
  children: PropTypes.element.isRequired,
  arrayOfHeaders: PropTypes.array.isRequired,
};

export default TableHead;
