import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import FlexWrapper from '../../components/FlexWrapper';
import { exchangeSelector, currentPositionFilterSelector } from './selectors';
import { saveCurrentPositionFilter } from './actions';
import { namesComparator } from '../../utils';
import { AllButton, PositionButton } from './styledComponents/styledComponents';

const compareNames = (comparator, currentPosition) => propsPosition => comparator(currentPosition, propsPosition);
const onClickButton = clickHandler => positionName => () => clickHandler(positionName);

const PositionsOfCurrentSport = ({ exchange, clickPosition, currentPosition }) => {
  const { Positions } = exchange;
  const isItCurrentPosition = compareNames(namesComparator, currentPosition);
  const onClickPosition = onClickButton(clickPosition);
  return (
    <FlexWrapper>
      Positions:
      {Positions.map((position) => {
        const { Index, Name } = position;
        return <PositionButton key={Index} pressed={isItCurrentPosition(Name)} onClick={onClickPosition(Name)}>{Name}</PositionButton>;
      })}
      <AllButton pressed={isItCurrentPosition('All')} onClick={onClickPosition('All')}>All</AllButton>
    </FlexWrapper>
  );
};

PositionsOfCurrentSport.propTypes = {
  exchange: PropTypes.object,
  currentPosition: PropTypes.string,
  clickPosition: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  exchange: exchangeSelector(),
  currentPosition: currentPositionFilterSelector(),
});

const mapDispatchToProps = dispatch => ({
  clickPosition: data => dispatch(saveCurrentPositionFilter(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(PositionsOfCurrentSport);
