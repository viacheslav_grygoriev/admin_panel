import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import H3 from './styledComponents/H3';
import Button from './styledComponents/Button';
import {
  timeEventsSelector,
  typeEventSelector,
  loadingTimeEventsSelector,
  periodSelector,
  selectedTimeEventsSelector,
} from './selectors';
import Loading from '../../components/Loading';
import { timeEventsForSelect, namesComparator, Types } from '../../utils';
import { loadPlayers, loadTimeEvents } from './actions';
import { FlexItemForSection, FlexWrapperRow } from './styledComponents/styledComponents';

const { TypeEvent: { FantasySport } } = Types;

class SelectEvents extends Component {
  constructor() {
    super();
    this.state = {};
    this.filterValues = [1, 2, 4, 8, 12, 24, 48];
  }

  static getDerivedStateFromProps(props) {
    const { currentTypeEvent } = props;
    return { isFantasySport: namesComparator(currentTypeEvent, FantasySport) };
  }

  filterButtons = (onClickButtonFilter) => {
    const { currentPeriod, loadingTimeEvents } = this.props;
    return this.filterValues.map(buttonValue => (
      <Button
        key={buttonValue}
        disabled={loadingTimeEvents || this.state.isFantasySport}
        pressed={namesComparator(currentPeriod, buttonValue)}
        onClick={onClickButtonFilter(buttonValue)}
      >{buttonValue}h</Button>
    ));
  };
  onClickButtonFilter = e => () => {
    const { onLoadTimeEvents } = this.props;
    onLoadTimeEvents(e);
  };

  onChangeEventSelect = (selectValues) => {
    const { onLoadPlayers } = this.props;
    const selectValuesIds = selectValues.map(selectValue => selectValue.value);
    onLoadPlayers(selectValuesIds);
  };

  currentTimeEventFind = () => {
    const { selectedTimeEvents, timeEvents } = this.props;
    // эту херню надо пофиксить
    return selectedTimeEvents
      ? selectedTimeEvents.map(timeEvent => timeEvents.get(timeEvent))
      : [];
  };

  render() {
    const { timeEvents, loadingTimeEvents } = this.props;
    const { isFantasySport } = this.state;
    return (
      <FlexItemForSection>
        <H3>Select Events</H3>
        <FlexWrapperRow>
          Filter of Events: {this.filterButtons(this.onClickButtonFilter)}
        </FlexWrapperRow>
        {loadingTimeEvents ? <Loading/> : <Select
          options={timeEventsForSelect([...timeEvents.values()])}
          onChange={this.onChangeEventSelect}
          isMulti={!isFantasySport}
          isDisabled={isFantasySport}
          defaultValue={timeEventsForSelect(this.currentTimeEventFind())}
          isClearable={false}
          closeMenuOnSelect={false}
        />}
      </FlexItemForSection>
    );
  }
}

SelectEvents.propTypes = {
  selectedTimeEvents: PropTypes.array,
  timeEvents: PropTypes.instanceOf(Map),
  currentTypeEvent: PropTypes.number, // eslint-disable-line react/no-unused-prop-types
  onLoadPlayers: PropTypes.func.isRequired,
  onLoadTimeEvents: PropTypes.func.isRequired,
  loadingTimeEvents: PropTypes.bool,
  currentPeriod: PropTypes.number,
};

const mapStateToProps = createStructuredSelector({
  timeEvents: timeEventsSelector(),
  selectedTimeEvents: selectedTimeEventsSelector(),
  currentTypeEvent: typeEventSelector(),
  loadingTimeEvents: loadingTimeEventsSelector(),
  currentPeriod: periodSelector(),
});

const mapDispatchToProps = dispatch => ({
  onLoadPlayers: data => dispatch(loadPlayers(data)),
  onLoadTimeEvents: data => dispatch(loadTimeEvents(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SelectEvents);
