import styled, { css } from 'styled-components';
import CreatableSelect from 'react-select/lib/Creatable';
import Select from 'react-select';
import Button from './Button';
import FlexWrapper from '../../../components/FlexWrapper/index';
import FlexItem from '../../../components/FlexItem/index';
import Tr from './Tr';
import H4 from './H4';
import { SIDEBAR_BACKGROUND, BLUE, NOT_ACTIVE_BLUE } from '../../../utils/index';

export const StyledCreatableSelect = styled(CreatableSelect)`
  width: 200px;
  ${props => props.error && css`
    border: 1px solid red;
`}
`;

export const DefenseSelect = styled(Select)`
  width: 200px;
`;

export const ExchangeButtonWithLimitWrapper = styled.div`
  max-width: 130px;
`;

export const AllButton = styled(Button)`
  margin-left: 15px;
  position: relative;  
`;

export const FlexWrapperForSection = styled(FlexWrapper)`
  justify-content: space-between;
  margin-bottom: 20px;
  &:first-of-type{
    //margin-top: 0;
  }
`;

export const FlexWrapperRow = styled(FlexWrapper)`
  justify-content: flex-start;
  margin-bottom: 10px;
`;

export const FlexItemForSection = styled(FlexItem)`
  width: 50%;
`;

export const TeamsButtonsBlock = styled.div`
  margin-left: 5px;
`;

export const PositionButton = styled(Button)`
  margin-left: 5px;
`;

export const TablesBodyRow = styled(Tr)`
  border-top: 1px solid ${SIDEBAR_BACKGROUND};
  ${props => props.playerChoosed && css`
    background-color: ${NOT_ACTIVE_BLUE};
    color: white;
  `}
  ${props => props.playerIsInCurrentTeam && css`
    background-color: ${BLUE};
    color: white;
  `}
`;

export const SelectedPlayersTeamHeader = styled(H4)`
  color: white;
  background-color: ${BLUE};
  margin-bottom: 5px;
  padding: 5px;
  ${props => !props.active && css`
    background-color: ${NOT_ACTIVE_BLUE};
  `}
`;

export const SelectedPlayersTeamContent = styled.div`
  margin: 5px 0;
`;
