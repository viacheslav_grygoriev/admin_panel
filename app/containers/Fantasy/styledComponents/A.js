import { css } from 'styled-components';
import NormalA from '../../../components/A/index';

export default NormalA.extend`
  ${props => props.team && css`
    margin: 0 2px;
  `}
  ${props => props.position && css`
    margin-right: 5px;
  `}
  ${props => props.active && css`
    font-weight: bold;
  `}
  
`;
