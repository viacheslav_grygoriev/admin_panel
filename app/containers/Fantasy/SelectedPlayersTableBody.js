import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import Td from './styledComponents/Td';
import Button from './styledComponents/Button';
import { playersSelector, serverPlayersSelector, isEditSelector } from './selectors';
import { removePlayerFromSelected } from './actions';
import namesComparator from '../../utils/namesComparator';
import formatDate from '../../utils/formatDate';
import { TablesBodyRow } from './styledComponents/styledComponents';

class SelectedPlayersTableBody extends Component {
  render() {
    const { playersIds, players, deletePlayer, serverPlayers, isEdit } = this.props;
    return playersIds.map((playerId, key) => {
      const { Position, TeamAlias, FirstName, LastName, StartDate, Eppg, Status, IsBlockedPoints, IsBlockedPlayer, TeamName, Rank, CustomPosition } = serverPlayers && serverPlayers.has(playerId) ? serverPlayers.get(playerId) : players.get(playerId);
      const isEditPosition = isEdit ? CustomPosition : Position;
      return (
        <TablesBodyRow key={playerId}>
          <Td>{key + 1}</Td>
          <Td>{isEditPosition}</Td>
          <Td>{TeamAlias}</Td>
          <Td>{namesComparator('DEF', isEditPosition) ? TeamName : `${FirstName} ${LastName}`}</Td>
          <Td>{formatDate(StartDate)}</Td>
          <Td>{Rank || '-'}</Td>
          <Td>{Eppg}</Td>
          <Td>{Status}</Td>
          <Td>{IsBlockedPoints ? 'blocked points' : 'unblocked points'}</Td>
          <Td>{!namesComparator('DEF', isEditPosition) && (IsBlockedPlayer ? 'blocked player' : 'unblocked player')}</Td>
          <Td><Button onClick={deletePlayer(playerId)}>X</Button></Td>
        </TablesBodyRow>
      );
    });
  }
}

SelectedPlayersTableBody.propTypes = {
  playersIds: PropTypes.array,
  players: PropTypes.instanceOf(Map),
  deletePlayer: PropTypes.func,
  serverPlayers: PropTypes.instanceOf(Map),
  isEdit: PropTypes.bool,
};

const mapStateToProps = createStructuredSelector({
  players: playersSelector(),
  serverPlayers: serverPlayersSelector(),
  isEdit: isEditSelector(),
});

const mapDispatchToProps = dispatch => ({
  deletePlayer: data => () => dispatch(removePlayerFromSelected(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SelectedPlayersTableBody);

