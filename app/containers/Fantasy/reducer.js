import { Map, OrderedSet, Set } from 'immutable';

import {
  LOAD_FANTASY_PAGE_DATA, LOAD_FANTASY_PAGE_DATA_SUCCESS, LOAD_FANTASY_PAGE_DATA_ERROR,
  LOAD_TIME_EVENTS_SUCCESS, LOAD_TIME_EVENTS_ERROR,
  LOAD_PLAYERS_SUCCESS, LOAD_PLAYERS_ERROR, LOAD_PLAYERS, LOAD_TIME_EVENTS,
  SAVE_SELECTED_TIME_EVENTS,
  SAVE_CURRENT_TEAM_FILTER, SAVE_CURRENT_POSITION_FILTER,
  SAVE_FILTERED_PLAYERS, START_FILTERING_PLAYERS,
  SAVE_CHANGED_ACTIVE_TEAM,
  SAVE_ADD_PLAYER_TO_SELECTED, SAVE_REMOVE_PLAYER_FROM_SELECTED,
  SAVE_CHANGED_TYPE_EVENT,
  SAVE_CHANGE_TEAM_ALIAS,
  SAVE_CHANGED_TEAM_SIZE,
  SAVE_ADDED_EXCHANGE_TYPES, SAVE_REMOVED_EXCHANGE_TYPES,
  SAVE_CHANGED_EXCHANGE_LIMIT,
  SAVE_TOTAL_CALCULATE,
  LOAD_DEFENSE, LOAD_DEFENSE_SUCCESS, LOAD_DEFENSE_ERROR,
  SAVE_FANTASY_SUCCESS, SAVE_FANTASY_ERROR,
  SAVE_CANCEL_CLICKED,
  CLEAR_STATE,
  HIDE_NOTIFICATION, SHOW_NOTIFICATION,
  SAVE_CHANGED_DEFENSE_STATE,
  TEAMS_FOR_EDIT,
  EXCHANGE_TYPE_INITIAL,
  SAVE_SERVER_PLAYERS,
} from './constants';

import { Types, inTypesEntitiesNamesFinder } from '../../utils';

const { Aliases: { Away }, TypeEvent: { FantasySport } } = Types;

const initialState = Map({
  loading: true,
  loadingPlayers: true,
  loadingTimeEvents: true,
  loadingDefense: false,
  error: null,
  exchange: null,
  period: 1,
  timeEvents: null,
  currentTimeEvent: null,
  players: null,
  selectedTimeEvents: null,
  currentTeamFilter: null,
  currentPositionFilter: null,
  filteredPlayers: null,
  activeTeam: Away,
  teamHomePlayers: OrderedSet(),
  teamAwayPlayers: OrderedSet(),
  typeEvent: FantasySport,
  teamHomeName: {},
  teamAwayName: {},
  teamSize: null,
  optionExchanges: Map(),
  teamAwayTotal: '0.0',
  teamHomeTotal: '0.0',
  notification: {},
  defensePressed: true,
  isEdit: false,
  exchangeTypeInitial: Set(),
  serverPlayers: null,
});

export default function fantasyReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_FANTASY_PAGE_DATA:
      return state
        .set('loading', true);

    case LOAD_FANTASY_PAGE_DATA_SUCCESS: {
      const { currentTimeEvent, AwayName, HomeName, AwayAlias, HomeAlias, TypeEvent, isEdit, ...rest } = action.data;
      return state
        .set('currentTimeEvent', currentTimeEvent)
        .set('exchange', rest)
        .set('teamHomeName', { TeamName: HomeName, TeamAlias: HomeAlias })
        .set('teamAwayName', { TeamName: AwayName, TeamAlias: AwayAlias })
        .set('typeEvent', TypeEvent)
        .set('isEdit', isEdit)
        .set('loading', false);
    }

    case LOAD_FANTASY_PAGE_DATA_ERROR:
    case LOAD_TIME_EVENTS_ERROR:
    case LOAD_PLAYERS_ERROR:
    case LOAD_DEFENSE_ERROR:
    case SAVE_FANTASY_ERROR:
      return state
        .set('error', action.error)
        .set('loading', true);

    case LOAD_TIME_EVENTS:
      return state
        .set('loadingTimeEvents', true);

    case LOAD_TIME_EVENTS_SUCCESS: {
      const { timeEvents, period } = action.data;
      return state
        .set('timeEvents', timeEvents)
        .set('period', period)
        .set('loadingTimeEvents', false);
    }

    case LOAD_PLAYERS:
    case START_FILTERING_PLAYERS:
      return state
        .set('loadingPlayers', true);

    case LOAD_PLAYERS_SUCCESS:
      return state
        .set('players', action.data);

    case SAVE_SELECTED_TIME_EVENTS:
      return state
        .set('selectedTimeEvents', action.data);

    case SAVE_CURRENT_TEAM_FILTER:
      return state
        .set('currentTeamFilter', action.data);

    case SAVE_CURRENT_POSITION_FILTER:
      return state
        .set('currentPositionFilter', action.data);

    case SAVE_FILTERED_PLAYERS:
      return state
        .set('filteredPlayers', action.data)
        .set('loadingPlayers', false);

    case SAVE_CHANGED_ACTIVE_TEAM:
      return state
        .set('activeTeam', action.data);

    case SAVE_ADD_PLAYER_TO_SELECTED: {
      const { activeTeam, playerId } = action.data;
      return state
        .updateIn(
          [`team${inTypesEntitiesNamesFinder('Aliases', activeTeam)}Players`],
          orderedSet => orderedSet.add(playerId),
        );
    }

    case SAVE_REMOVE_PLAYER_FROM_SELECTED: {
      const { activeTeam, playerId } = action.data;
      return state
        .updateIn(
          [`team${inTypesEntitiesNamesFinder('Aliases', activeTeam)}Players`],
          orderedSet => orderedSet.delete(playerId),
        );
    }

    case SAVE_CHANGED_TYPE_EVENT:
      return state
        .set('typeEvent', action.data);

    case SAVE_CHANGE_TEAM_ALIAS: {
      const { TeamAlias, TeamName } = action.data;
      return state
        .set(
          `team${inTypesEntitiesNamesFinder('Aliases', action.data.activeTeam)}Name`,
          { TeamAlias, TeamName }
        );
    }

    case SAVE_CHANGED_TEAM_SIZE:
      return state
        .set('teamSize', action.data);

    case SAVE_ADDED_EXCHANGE_TYPES:
      return state
        .updateIn(
          ['optionExchanges'],
          exchange => exchange.set(action.data.optionExchange, action.data.exchangeValue)
        );

    case SAVE_REMOVED_EXCHANGE_TYPES:
      return state
        .updateIn(
          ['optionExchanges'],
          exchange => exchange.delete(action.data.optionExchange)
        );

    case SAVE_CHANGED_EXCHANGE_LIMIT: {
      const { optionExchange, exchangeValue } = action.data;
      return state
        .updateIn(
          ['optionExchanges'],
          exchange => exchange.set(optionExchange, exchangeValue)
        );
    }

    case SAVE_TOTAL_CALCULATE: {
      const { teamAwayTotal, teamHomeTotal } = action.data;
      return state
        .set('teamAwayTotal', teamAwayTotal.toFixed(1))
        .set('teamHomeTotal', teamHomeTotal.toFixed(1));
    }

    case LOAD_DEFENSE:
      return state
        .set('loadingDefense', true);

    case LOAD_DEFENSE_SUCCESS: {
      const { PlayerId, Defense } = action.data;
      return state
        .updateIn(
          ['players'],
          players => players.set(PlayerId, Defense)
        )
        .set('loadingDefense', false);
    }

    case SAVE_FANTASY_SUCCESS:
    case SAVE_CANCEL_CLICKED:
    case CLEAR_STATE:
      return initialState;

    case SHOW_NOTIFICATION:
      return state
        .set('notification', { message: action.data.message, type: action.data.type });

    case HIDE_NOTIFICATION:
      return state
        .set('notification', {});

    case SAVE_CHANGED_DEFENSE_STATE:
      return state
        .update('defensePressed', value => !value);

    case TEAMS_FOR_EDIT: {
      const { AwayPlayersIds, HomePlayersIds } = action.data;
      return state
        .update('teamAwayPlayers', orderedSet => orderedSet.concat(AwayPlayersIds))
        .update('teamHomePlayers', orderedSet => orderedSet.concat(HomePlayersIds));
    }

    case EXCHANGE_TYPE_INITIAL: {
      const { optionExchange } = action.data;
      return state
        .updateIn(['exchangeTypeInitial'], set => set.add(optionExchange));
    }

    case SAVE_SERVER_PLAYERS: {
      return state
        .set('serverPlayers', action.data);
    }

    default:
      return state;
  }
}
