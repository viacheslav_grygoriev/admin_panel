import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import FlexWrapper from '../../components/FlexWrapper';

import H3 from './styledComponents/H3';

import TeamsOfExchanges from './TeamsOfExchanges';
import PositionsOfCurrentSport from './PositionsOfCurrentSport';
import AvailablePlayersTable from './TableHead';
import AvailablePlayersTableBody from './AvailablePlayersTableBody';
import { loadingPlayersSelector } from './selectors';
import Loading from '../../components/Loading';
import { FlexItemForSection, FlexWrapperRow } from './styledComponents/styledComponents';

const AvailablePlayers = ({ loadingPlayers }) => (
  <FlexItemForSection>
    <H3>Available players</H3>
    {loadingPlayers
      ? <Loading/>
      : (
        <Fragment>
          <FlexWrapperRow flexDirection="column">
            <TeamsOfExchanges/>
            <PositionsOfCurrentSport/>
          </FlexWrapperRow>
          <FlexWrapper>
            <AvailablePlayersTable
              arrayOfHeaders={[null, 'Pos', 'Team', 'Player Name', 'Start Date', 'Rank', 'EPPG', 'Status', null]}
            >
              <AvailablePlayersTableBody/>
            </AvailablePlayersTable>
          </FlexWrapper>
        </Fragment>
      )}
  </FlexItemForSection>
);

AvailablePlayers.propTypes = {
  loadingPlayers: PropTypes.bool,
};

const mapStateToProps = createStructuredSelector({
  loadingPlayers: loadingPlayersSelector(),
});
export default connect(mapStateToProps)(AvailablePlayers);
