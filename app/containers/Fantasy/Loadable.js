/**
 * Asynchronously loads the component for Fantasy (create/edit) page
 */
import Loadable from 'react-loadable';
import Loading from 'components/Loading';

export default Loadable({
  loader: () => import(/* webpackChunkName: "Fantasy" */ './index'),
  loading: Loading,
});
