import React from 'react';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import SelectedPlayers from './SelectedPlayers';
import { Types } from '../../utils';
import { FlexItemForSection } from './styledComponents/styledComponents';
import { teamHomePlayersSelector, teamAwayPlayersSelector } from './selectors';

const { Aliases: { Away, Home } } = Types;

const SelectedPlayersWrapper = ({ teamHomePlayers, teamAwayPlayers }) => (
  <FlexItemForSection>
    <SelectedPlayers team={Away} amountOfPlayers={teamAwayPlayers.length}/>
    <SelectedPlayers team={Home} amountOfPlayers={teamHomePlayers.length}/>
  </FlexItemForSection>
);

SelectedPlayersWrapper.propTypes = {};

SelectedPlayersWrapper.propTypes = {
  teamHomePlayers: PropTypes.array,
  teamAwayPlayers: PropTypes.array,
};

const mapStateToProps = createStructuredSelector({
  teamHomePlayers: teamHomePlayersSelector(),
  teamAwayPlayers: teamAwayPlayersSelector(),
});
export default connect(mapStateToProps)(SelectedPlayersWrapper);
