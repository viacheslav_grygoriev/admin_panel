import { push } from 'react-router-redux';
import {
  take,
  put,
  all,
  call,
  select,
  takeLatest,
} from 'redux-saga/effects';
import { delay } from 'redux-saga';
import {
  request,
  showError,
  GET_NEWFEED_URL,
  GET_TIME_EVENTS,
  GET_PLAYERS,
  mapOfExchanges,
  namesComparator,
  Types,
  defaultTeamSizeCalculator,
  GET_DEFENSE, SAVE_FANTASY_URL, EDIT_FANTASY_URL, UPDATE_FANTASY_URL,
  notInList,
} from '../../utils';
import {
  LOAD_FANTASY_PAGE_DATA, LOAD_TIME_EVENTS, LOAD_PLAYERS, LOAD_TIME_EVENTS_SUCCESS,
  SAVE_CURRENT_POSITION_FILTER, SAVE_CURRENT_TEAM_FILTER, LOAD_PLAYERS_SUCCESS,
  CHANGE_ACTIVE_TEAM,
  ADD_PLAYER_TO_SELECTED, REMOVE_PLAYER_FROM_SELECTED,
  CHANGE_TYPE_EVENT,
  CHANGE_TEAM_SIZE,
  ADD_EXCHANGE_TYPES,
  CHANGE_EXCHANGE_LIMIT,
  CHANGE_TEAM_ALIAS, REMOVE_EXCHANGE_TYPES,
  LOAD_DEFENSE,
  SAVE_FANTASY,
  CANCEL_CLICKED,
  REQUEST_NOTIFICATION,
  CHANGE_DEFENSE_STATE, TOTAL_CALCULATE, LOAD_DEFENSE_SUCCESS,
} from './constants';
import {
  loadFantasyPageDataSuccess, loadFantasyPageDataError,
  loadTimeEvents, loadTimeEventsSuccess, loadTimeEventsError,
  loadPlayers, loadPlayersSuccess, loadPlayersError,
  saveSelectedTimeEvents,
  saveCurrentTeamFilter, saveCurrentPositionFilter,
  startFilteringPlayers, saveFilteredPlayers,
  saveChangedActiveTeam,
  saveAddPlayerToSelected, saveRemovePlayerFromSelected,
  saveChangedTypeEvent,
  saveChangeTeamAlias,
  saveChangedTeamSize,
  saveAddedExchangeTypes, saveRemovedExchangeTypes,
  changeExchangeLimit, saveChangedExchangeLimit,
  saveTotalCalculate, totalCalculate,
  loadDefenseSuccess, loadDefenseError,
  saveFantasySuccess, saveFantasyError,
  saveCancelClicked,
  requestNotification, showNotification, hideNotification,
  saveChangedDefenseState,
  teamsForEdit, loadDefense,
  exchangeTypeInitial,
  saveServerPlayers, changeDefenseState,
} from './actions';
import {
  activeTeamSelector,
  currentPositionFilterSelector,
  currentTeamFilterSelector,
  currentTimeEventSelector, exchangeSelector,
  loadingPlayersSelector,
  loadingTimeEventsSelector,
  optionExchangesSelector,
  playersSelector,
  teamAwayNameSelector,
  teamAwayPlayersSelector,
  teamHomeNameSelector, teamHomePlayersSelector,
  typeEventSelector,
  teamSizeSelector,
  validationObjectSelector, exchangeTypeInitialSelector,
} from './selectors';

import { requestingTokenSelector } from '../SiteApp/selectors';
import { refreshToken } from '../SiteApp/actions';
import { REFRESH_TOKEN_SUCCESS } from '../SiteApp/constants';


const { Aliases: { Home, Away }, TypeEvent: { FantasySport } } = Types;

export function* loadFantasyPageDataSaga(action) {
  try {
    const { id, isEdit } = action.data;
    let responseNewFeed = yield call(request, isEdit ? EDIT_FANTASY_URL : GET_NEWFEED_URL, {
      method: 'POST',
      body: JSON.stringify({
        [`${isEdit ? 'ExchangeName' : 'EventId'}`]: id,
      }),
    });
    // костыль начало
    if (responseNewFeed.status === 401) {
      const requesting = yield select(requestingTokenSelector());
      if (requesting) {
        yield take(REFRESH_TOKEN_SUCCESS);
      }
      yield put(refreshToken());
      yield take(REFRESH_TOKEN_SUCCESS);
      responseNewFeed = yield call(request, isEdit ? EDIT_FANTASY_URL : GET_NEWFEED_URL, {
        method: 'POST',
        body: JSON.stringify({
          [`${isEdit ? 'ExchangeName' : 'EventId'}`]: id,
        }),
      });
    }
    // костыль конец

    if (isEdit) {
      const {
        Exchanges: {
          Exchanges: {
            FullName, StartDateEvent, AwayName, HomeName, AwayAlias, HomeAlias, TypeEvent, Symbols, Name,
            Category: {
              CatId, CatName,
            },
          },
          AwayPlayers, HomePlayers,
        },
        Positions, EventId,
      } = responseNewFeed;

      const AwayPlayersIds = [];
      const HomePlayersIds = [];
      const arrayForDefenses = [];
      const arrayForLoadingPlayers = new Set([EventId]);

      const mapOfMarkets = yield call(mapOfExchanges, Symbols, {
        id: 'Name',
        path: 'Symbol',
        fields: [
          'ExchangeLimit',
          'OptionExchange',
        ],
      });

      for (const [id, market] of mapOfMarkets) {  // eslint-disable-line
        const { OptionExchange: optionExchange, ExchangeLimit: exchangeValue } = market;
        yield put(changeExchangeLimit({ optionExchange, exchangeValue }));
        yield put(exchangeTypeInitial({ optionExchange }));
      }

      for (const playerObj of AwayPlayers) { // eslint-disable-line no-restricted-syntax
        const { CustomPosition, TeamId, EventId: playersEventId } = playerObj;

        if (namesComparator(CustomPosition, 'DEF')) {
          arrayForDefenses.push({ TeamId, EventId: playersEventId });
        }

        if (EventId !== playersEventId) {
          arrayForLoadingPlayers.add(playersEventId);
        }
        AwayPlayersIds.push(playerObj.PlayerId);
      }

      for (const playerObj of HomePlayers) { // eslint-disable-line no-restricted-syntax
        const { CustomPosition, TeamId, EventId: playersEventId } = playerObj;

        if (namesComparator(CustomPosition, 'DEF')) {
          arrayForDefenses.push({ TeamId, EventId: playersEventId });
        }

        if (EventId !== playersEventId) {
          arrayForLoadingPlayers.add(playersEventId);
        }
        HomePlayersIds.push(playerObj.PlayerId);
      }

      const currentPositionFilter = Positions.find(position => position.Index === 1);
      yield put(saveCurrentPositionFilter(currentPositionFilter.Name));
      yield put(changeDefenseState());
      yield put(saveChangedTeamSize(AwayPlayers.length));
      yield put(loadFantasyPageDataSuccess({
        FullName,
        StartDateEvent,
        Positions,
        AwayName,
        AwayAlias,
        HomeName,
        HomeAlias,
        TypeEvent,
        League: CatName,
        CategoryId: CatId,
        currentTimeEvent: EventId,
        isEdit,
        Name,
      }));
      yield put(loadTimeEvents(1));

      yield put(loadPlayers(arrayForLoadingPlayers.toJSON()));
      yield take(LOAD_PLAYERS_SUCCESS);

      if (arrayForDefenses.length) {
        for (const defenseData of arrayForDefenses) { // eslint-disable-line no-restricted-syntax
          yield put(loadDefense(defenseData));
          yield take(LOAD_DEFENSE_SUCCESS);
        }
      }

      const concatedPlayers = AwayPlayers.concat(HomePlayers);
      const serverPlayersMap = yield call(mapOfExchanges, concatedPlayers, {
        id: 'PlayerId',
        fields: [
          // for backend:
          'PlayerId',
          'Eppg',
          'Status',
          'Index',
          'EventId',
          'IsNew',
          'IsBlockedPlayer',
          'IsBlockedPoints',
          // for view:
          'FirstName',
          'LastName',
          'Position',
          'CustomPosition',
          'StartDate',
          'TeamAlias',
          'Rank',
        ],
      });

      yield put(saveServerPlayers(serverPlayersMap));


      let thereIsDefenseInPlayers = false;
      for (const [id, player] of serverPlayersMap) { // eslint-disable-line
        const { CustomPosition } = player;
        if (namesComparator('DEF', CustomPosition)) {
          thereIsDefenseInPlayers = true;
          break;
        }
      }
      if (thereIsDefenseInPlayers) {
        yield put(changeDefenseState());
      }

      yield put(teamsForEdit({ AwayPlayersIds, HomePlayersIds }));
      yield put(totalCalculate());
    } else {
      const {
        Event: {
          FullName, StartDate, League,
        },
        Positions, Categories,
      } = responseNewFeed;

      const currentPositionFilter = Positions.find(position => position.Index === 1);
      const { CategoryId } = Categories.find(category => category.IsCurrent);

      yield put(loadFantasyPageDataSuccess({
        FullName,
        StartDate,
        Positions,
        League,
        CategoryId,
        TypeEvent: FantasySport,
        currentTimeEvent: id,
        isEdit,
      }));
      yield put(saveCurrentPositionFilter(currentPositionFilter.Name));
      yield put(loadTimeEvents(1));
      yield put(loadPlayers([id]));
      yield put(saveChangedTeamSize(defaultTeamSizeCalculator(Positions)));
    }
  } catch (error) {
    yield put(loadFantasyPageDataError(error.message));
    yield showError(error.message);
  }
}

export function* loadTimeEventsSaga({ data }) {
  try {
    const EventId = yield select(currentTimeEventSelector());
    let responseTimeEvents = yield call(request, GET_TIME_EVENTS, {
      method: 'POST',
      body: JSON.stringify({
        EventId,
        Period: data,
      }),
    });
    // костыль начало
    if (responseTimeEvents.status === 401) {
      const requesting = yield select(requestingTokenSelector());
      if (requesting) {
        yield take(REFRESH_TOKEN_SUCCESS);
      }
      yield put(refreshToken());
      yield take(REFRESH_TOKEN_SUCCESS);
      responseTimeEvents = yield call(request, GET_TIME_EVENTS, {
        method: 'POST',
        body: JSON.stringify({
          EventId,
          Period: data,
        }),
      });
    }
    // костыль конец
    const mapTimeEvents = yield call(mapOfExchanges, responseTimeEvents, {
      id: 'EventId',
      fields: [
        'AwayAlias',
        'AwayTeam',
        'EventId',
        'HomeAlias',
        'HomeTeam',
        'StartDate',
        'AwayId',
        'HomeId',
      ],
    });
    const currentTimeEvent = responseTimeEvents.find(timeEvent => timeEvent.EventId === EventId);
    yield put(loadTimeEventsSuccess({ timeEvents: mapTimeEvents, period: data }));
    yield put(saveCurrentTeamFilter(currentTimeEvent.AwayAlias));
  } catch (error) {
    yield put(loadTimeEventsError(error.message));
    yield showError(error.message);
  }
}

export function* loadPlayersSaga({ data }) {
  try {
    const loadingTimeEvents = yield select(loadingTimeEventsSelector());

    if (loadingTimeEvents) yield take(LOAD_TIME_EVENTS_SUCCESS);

    yield put(saveSelectedTimeEvents(data));

    let responsePlayers = yield call(request, GET_PLAYERS, {
      method: 'POST',
      body: JSON.stringify({
        EventId: data,
      }),
    });
    // костыль начало
    if (responsePlayers.status === 401) {
      const requesting = yield select(requestingTokenSelector());
      if (requesting) {
        yield take(REFRESH_TOKEN_SUCCESS);
      }
      yield put(refreshToken());
      yield take(REFRESH_TOKEN_SUCCESS);
      responsePlayers = yield call(request, GET_PLAYERS, {
        method: 'POST',
        body: JSON.stringify({
          EventId: data,
        }),
      });
    }
    // костыль конец
    const responsePlayersMap = yield call(mapOfExchanges, responsePlayers, {
      id: 'PlayerId',
      fields: [
        // for backend:
        'PlayerId',
        'Eppg',
        'Status',
        'Index',
        'EventId',
        'IsNew',
        'IsBlockedPlayer',
        'IsBlockedPoints',
        // for view:
        'FirstName',
        'LastName',
        'Position',
        'CustomPosition',
        'StartDate',
        'TeamAlias',
        'Rank',
      ],
    });
    yield put(loadPlayersSuccess(responsePlayersMap));
  } catch (error) {
    yield put(loadPlayersError(error));
    yield showError(error.message);
  }
}

export function* filterPlayersSaga() {
  const currentTeam = yield select(currentTeamFilterSelector());
  const currentPosition = yield select(currentPositionFilterSelector());
  const loadingTimeEvents = yield select(loadingTimeEventsSelector());
  const loadingPlayers = yield select(loadingPlayersSelector());

  if (loadingTimeEvents || loadingPlayers) yield take(LOAD_PLAYERS_SUCCESS);
  yield put(startFilteringPlayers());
  const players = yield select(playersSelector());
  const filteredPlayers = [...players.keys()].filter((playerId) => {
    const player = players.get(playerId);
    if (namesComparator(currentTeam, 'All') && namesComparator(currentPosition, 'All')) return player;
    if (namesComparator(currentTeam, 'All')) return player.Position === currentPosition;
    if (namesComparator(currentPosition, 'All')) return player.TeamAlias === currentTeam;
    return player.TeamAlias === currentTeam && player.Position === currentPosition;
  });
  yield put(saveFilteredPlayers(filteredPlayers));
}

export function* changeActiveTeamSaga() {
  const activeTeam = yield select(activeTeamSelector());
  const changedActiveTeam = namesComparator(Away, activeTeam) ? Home : Away;
  yield put(saveChangedActiveTeam(changedActiveTeam));
}

export function* addPlayerToSelectedSaga(action) {
  const activeTeam = yield select(activeTeamSelector());
  yield put(saveAddPlayerToSelected({ playerId: action.data, activeTeam }));
  yield put(totalCalculate());
}

export function* removePlayerFromSelectedSaga(action) {
  const activeTeam = yield select(activeTeamSelector());
  yield put(saveRemovePlayerFromSelected({ playerId: action.data, activeTeam }));
  yield put(totalCalculate());
}

export function* changeTypeEventSaga(action) {
  yield put(saveChangedTypeEvent(action.data));
}

export function* changeTeamAliasSaga(action) {
  const activeTeam = yield select(activeTeamSelector());
  const { value, label } = action.data;
  yield put(saveChangeTeamAlias({ TeamAlias: value, TeamName: label, activeTeam }));
}

export function* changeTeamSizeSaga(action) {
  const { value } = action.data;
  yield put(saveChangedTeamSize(value));
}

export function* addExchangeTypesSaga(action) {
  yield put(saveAddedExchangeTypes(action.data));
}

export function* removeExchangeTypesSaga(action) {
  yield put(saveRemovedExchangeTypes(action.data));
}

export function* changeExchangeLimitSaga(action) {
  yield put(saveChangedExchangeLimit(action.data));
}

export function* loadDefenseSaga(action) {
  try {
    const { TeamId, EventId, addToActive } = action.data;

    let responseDefense = yield call(request, GET_DEFENSE, {
      method: 'POST',
      body: JSON.stringify({
        EventId,
        TeamId,
      }),
    });
    // костыль начало
    if (responseDefense.status === 401) {
      const requesting = yield select(requestingTokenSelector());
      if (requesting) {
        yield take(REFRESH_TOKEN_SUCCESS);
      }
      yield put(refreshToken());
      yield take(REFRESH_TOKEN_SUCCESS);
      responseDefense = yield call(request, GET_DEFENSE, {
        method: 'POST',
        body: JSON.stringify({
          EventId,
          TeamId,
        }),
      });
    }
    // костыль конец

    yield put(loadDefenseSuccess({ PlayerId: responseDefense.PlayerId, Defense: responseDefense }));
    if (addToActive) {
      const activeTeam = yield select(activeTeamSelector());
      yield put(saveAddPlayerToSelected({ playerId: responseDefense.PlayerId, activeTeam }));
    }
    yield put(totalCalculate());
  } catch (error) {
    yield put(loadDefenseError(error));
    yield showError(error.message);
  }
}

function optionExchangesToExchangeLimit(optionExchangesObj) {
  const res = [];
  const entries = Object.entries(optionExchangesObj);
  entries.forEach(([optionExchange, Limit]) => {
    res.push({ OptionExchange: optionExchange, Limit });
  });
  return res;
}

function teamPlayersToTeam(playerIdsArr, playersMap) {
  const res = [];
  playerIdsArr.forEach((playerId) => {
    res.push(playersMap.get(playerId));
  });
  return res;
}

export function* requestNotificationSaga(action) {
  yield put(showNotification(action.data));
  yield delay(5000);
  yield put(hideNotification());
}

function* validationOfAddFantasySaga() {
  const { exchangeTypesAreValid, teamAwaySizeIsMore, teamAwaySizeIsLess, teamAwayNameExists, teamHomeSizeIsMore, teamHomeSizeIsLess, teamHomeNameExists } = yield select(validationObjectSelector());
  // __DEV__ && console.log('(validationObjectSelector)', yield select(validationObjectSelector()));
  if (!exchangeTypesAreValid) {
    yield put(requestNotification({ message: 'Please, choose exchange types' }));
    return false;
  }
  if (teamAwaySizeIsMore) {
    yield put(requestNotification({ message: 'Team away is more than team size' }));
    return false;
  }
  if (teamAwaySizeIsLess) {
    yield put(requestNotification({ message: 'Team away is less than team size' }));
    return false;
  }
  if (!teamAwayNameExists) {
    yield put(requestNotification({
      message: 'Please, generate or enter away team name',
    }));
    return false;
  }
  if (teamHomeSizeIsMore) {
    yield put(requestNotification({ message: 'Team home is more than team size' }));
    return false;
  }
  if (teamHomeSizeIsLess) {
    yield put(requestNotification({ message: 'Team home is less than team size' }));
    return false;
  }
  if (!teamHomeNameExists) {
    yield put(requestNotification({
      message: 'Please, generate or enter home team name',
    }));
    return false;
  }
  return true;
}

export function* saveFantasySaga(action) {
  try {
    const { path, isEdit } = action.data;

    const { TeamAlias: AwayAlias, TeamName: AwayName } = yield select(teamAwayNameSelector());
    const { TeamAlias: HomeAlias, TeamName: HomeName } = yield select(teamHomeNameSelector());
    const FullName = `${AwayName} @ ${HomeName}`;
    const TypeEvent = yield select(typeEventSelector());
    const EventId = yield select(currentTimeEventSelector());
    const { CategoryId, Name } = yield select(exchangeSelector());
    const optionExchanges = yield select(optionExchangesSelector());
    const ExchangeLimit = optionExchangesToExchangeLimit(optionExchanges);
    const players = yield select(playersSelector());
    const teamAwayPlayers = yield select(teamAwayPlayersSelector());
    const teamHomePlayers = yield select(teamHomePlayersSelector());
    const AwayTeam = teamPlayersToTeam(teamAwayPlayers, players);
    const HomeTeam = teamPlayersToTeam(teamHomePlayers, players);
    const teamSize = yield select(teamSizeSelector());
    const exchangeTypeInitialVar = yield select(exchangeTypeInitialSelector());

    const isValid = yield validationOfAddFantasySaga({
      ExchangeLimit,
      teamAwayPlayers,
      teamHomePlayers,
      teamSize,
    });
    if (isValid) {
      const responseAddFantasy = yield call(request, isEdit ? UPDATE_FANTASY_URL : SAVE_FANTASY_URL, {
        method: 'POST',
        body: JSON.stringify({
          [isEdit ? 'ExchangeName' : 'HomeAlias']: isEdit ? Name : HomeAlias,
          AwayAlias,
          [isEdit ? 'ExchangeTypes' : 'CategoryId']: isEdit ? notInList(Object.keys(optionExchanges), exchangeTypeInitialVar) : CategoryId,
          FullName,
          HomeName,
          AwayName,
          TypeEvent,
          EventId,
          ExchangeLimit,
          HomeTeam,
          AwayTeam,
        }),
      });
      // костыль начало
      if (responseAddFantasy.status === 401) {
        const requesting = yield select(requestingTokenSelector());
        if (requesting) {
          yield take(REFRESH_TOKEN_SUCCESS);
        }
        yield put(refreshToken());
        yield take(REFRESH_TOKEN_SUCCESS);
        yield call(request, isEdit ? UPDATE_FANTASY_URL : SAVE_FANTASY_URL, {
          method: 'POST',
          body: JSON.stringify({
            [isEdit ? 'ExchangeName' : 'HomeAlias']: isEdit ? Name : HomeAlias,
            AwayAlias,
            [isEdit ? 'ExchangeTypes' : 'CategoryId']: isEdit ? notInList(Object.keys(optionExchanges), exchangeTypeInitialVar) : CategoryId,
            FullName,
            HomeName,
            AwayName,
            TypeEvent,
            EventId,
            ExchangeLimit,
            HomeTeam,
            AwayTeam,
          }),
        });
      }
      // костыль конец
      yield put(saveFantasySuccess());
      yield put(push(path));
    }
  } catch (error) {
    yield put(saveFantasyError(error));
    yield showError(error.message);
  }
}

export function* cancelClickedsaga() {
  yield put(saveCancelClicked());
}

export function* changeDefenseStateSaga() {
  const players = yield select(playersSelector());
  const teamAwayPlayers = yield select(teamAwayPlayersSelector());
  const teamHomePlayers = yield select(teamHomePlayersSelector());

  for (let i = 0; i < teamAwayPlayers.length; i += 1) {
    const currentPlayer = players.get(teamAwayPlayers[i]);
    const { Position } = currentPlayer;
    if (namesComparator('DEF', Position)) {
      yield put(saveRemovePlayerFromSelected({ playerId: teamAwayPlayers[i], activeTeam: Away }));
    }
  }
  for (let i = 0; i < teamHomePlayers.length; i += 1) {
    const currentPlayer = players.get(teamHomePlayers[i]);
    const { Position } = currentPlayer;
    if (namesComparator('DEF', Position)) {
      yield put(saveRemovePlayerFromSelected({ playerId: teamHomePlayers[i], activeTeam: Home }));
    }
  }
  yield put(saveChangedDefenseState());
  yield put(totalCalculate());
}

function* totalCalculateSaga() {
  const players = yield select(playersSelector());
  const teamAwayPlayers = yield select(teamAwayPlayersSelector());
  const teamHomePlayers = yield select(teamHomePlayersSelector());
  const res = {
    teamAwayTotal: 0,
    teamHomeTotal: 0,
  };
  teamAwayPlayers.forEach((playerId) => {
    const { Eppg } = players.get(playerId);
    res.teamAwayTotal += Eppg;
  });
  teamHomePlayers.forEach((playerId) => {
    const { Eppg } = players.get(playerId);
    res.teamHomeTotal += Eppg;
  });
  yield put(saveTotalCalculate(res));
}

export default function* fantasySaga() {
  yield all([
    yield takeLatest(LOAD_FANTASY_PAGE_DATA, loadFantasyPageDataSaga),
    yield takeLatest(LOAD_TIME_EVENTS, loadTimeEventsSaga),
    yield takeLatest(LOAD_PLAYERS, loadPlayersSaga),
    yield takeLatest([SAVE_CURRENT_TEAM_FILTER, SAVE_CURRENT_POSITION_FILTER, LOAD_PLAYERS], filterPlayersSaga),
    yield takeLatest(CHANGE_ACTIVE_TEAM, changeActiveTeamSaga),
    yield takeLatest(ADD_PLAYER_TO_SELECTED, addPlayerToSelectedSaga),
    yield takeLatest(REMOVE_PLAYER_FROM_SELECTED, removePlayerFromSelectedSaga),
    yield takeLatest(CHANGE_TYPE_EVENT, changeTypeEventSaga),
    yield takeLatest(CHANGE_TEAM_SIZE, changeTeamSizeSaga),
    yield takeLatest(ADD_EXCHANGE_TYPES, addExchangeTypesSaga),
    yield takeLatest(REMOVE_EXCHANGE_TYPES, removeExchangeTypesSaga),
    yield takeLatest(CHANGE_EXCHANGE_LIMIT, changeExchangeLimitSaga),
    yield takeLatest(CHANGE_TEAM_ALIAS, changeTeamAliasSaga),
    yield takeLatest(LOAD_DEFENSE, loadDefenseSaga),
    yield takeLatest(SAVE_FANTASY, saveFantasySaga),
    yield takeLatest(CANCEL_CLICKED, cancelClickedsaga),
    yield takeLatest(REQUEST_NOTIFICATION, requestNotificationSaga),
    yield takeLatest(CHANGE_DEFENSE_STATE, changeDefenseStateSaga),
    yield takeLatest(TOTAL_CALCULATE, totalCalculateSaga),
  ]);
}
