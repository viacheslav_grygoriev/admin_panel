/* eslint-disable */

import { createSelector } from 'reselect';
import { FANTASY_PAGE } from './constants';
import { inTypesEntitiesNamesFinder, Types, namesComparator, isEmptyObject } from '../../utils';

const { Aliases: { Home, Away } } = Types;

const selectFantasy = state => state.get(FANTASY_PAGE).toJS();

const loadingSelector = () => createSelector(
  [selectFantasy],
  (selectFantasy) => selectFantasy.loading
);

const exchangeSelector = () => createSelector(
  [selectFantasy],
  (selectFantasy) => selectFantasy.exchange
);

const periodSelector = () => createSelector(
  [selectFantasy],
  (selectFantasy) => selectFantasy.period
);

const timeEventsSelector = () => createSelector(
  [selectFantasy],
  (selectFantasy) => selectFantasy.timeEvents
);

const currentTimeEventSelector = () => createSelector(
  [selectFantasy],
  (selectFantasy) => selectFantasy.currentTimeEvent
);

const playersSelector = () => createSelector(
  [selectFantasy],
  (selectFantasy) => selectFantasy.players
);

const loadingPlayersSelector = () => createSelector(
  [selectFantasy],
  (selectFantasy) => selectFantasy.loadingPlayers
);

const loadingTimeEventsSelector = () => createSelector(
  [selectFantasy],
  (selectFantasy) => selectFantasy.loadingTimeEvents
);

const selectedTimeEventsSelector = () => createSelector(
  [selectFantasy],
  (selectFantasy) => selectFantasy.selectedTimeEvents
);

const currentTeamFilterSelector = () => createSelector(
  [selectFantasy],
  (selectFantasy) => selectFantasy.currentTeamFilter
);

const currentPositionFilterSelector = () => createSelector(
  [selectFantasy],
  (selectFantasy) => selectFantasy.currentPositionFilter
);

const filteredPlayersSelector = () => createSelector(
  [selectFantasy],
  (selectFantasy) => selectFantasy.filteredPlayers
);

const activeTeamSelector = () => createSelector(
  [selectFantasy],
  (selectFantasy) => selectFantasy.activeTeam
);

const typeEventSelector = () => createSelector(
  [selectFantasy],
  (selectFantasy) => selectFantasy.typeEvent
);

const teamAwayPlayersSelector = () => createSelector(
  [selectFantasy],
  (selectFantasy) => selectFantasy.teamAwayPlayers
);

const teamHomePlayersSelector = () => createSelector(
  [selectFantasy],
  (selectFantasy) => selectFantasy.teamHomePlayers
);

const currentTeamPlayersSelector = createSelector(
  [selectFantasy],
  (selectFantasy) => {
    const { activeTeam, teamAwayPlayers, teamHomePlayers } = selectFantasy;
    return namesComparator(activeTeam, Home) ? teamHomePlayers : teamAwayPlayers;
  }
);

const concatedTeamPlayersSelector = () => createSelector(
  [selectFantasy],
  (selectFantasy) => {
    const teamHomePlayersSelected = selectFantasy.teamHomePlayers;
    const teamAwayPlayersSelected = selectFantasy.teamAwayPlayers;
    return teamAwayPlayersSelected.concat(teamHomePlayersSelected);
  }
);

const teamHomeNameSelector = () => createSelector(
  [selectFantasy],
  (selectFantasy) => selectFantasy.teamHomeName
);

const teamAwayNameSelector = () => createSelector(
  [selectFantasy],
  (selectFantasy) => selectFantasy.teamAwayName
);

const currentTeamNameSelector = createSelector(
  [selectFantasy],
  (selectFantasy) => {
    const { activeTeam, teamHomeName, teamAwayName } = selectFantasy;
    return namesComparator(activeTeam, Home) ? teamHomeName : teamAwayName;
  }
);

const teamSizeSelector = () => createSelector(
  [selectFantasy],
  (selectFantasy) => selectFantasy.teamSize
);

const optionExchangesSelector = () => createSelector(
  [selectFantasy],
  (selectFantasy) => selectFantasy.optionExchanges
);

const totalSelector = () => createSelector(
  [selectFantasy],
  (selectFantasy) => ({
    [`${Away}`]: selectFantasy.teamAwayTotal,
    [`${Home}`]: selectFantasy.teamHomeTotal,
  })
);

const validationObjectSelector = () => createSelector(
  [selectFantasy, currentTeamNameSelector, currentTeamPlayersSelector],
  (selectFantasy, currentTeamName, currentTeamPlayers) => {
    const { optionExchanges, teamSize, teamAwayPlayers, teamHomePlayers, teamAwayName, teamHomeName } = selectFantasy;
    return {
      exchangeTypesAreValid: !isEmptyObject(optionExchanges),
      currentTeamNameIsValid: !isEmptyObject(currentTeamName) && !!currentTeamName.TeamName,
      teamAwaySizeIsMore: teamAwayPlayers.length > teamSize,
      teamAwaySizeIsLess: teamAwayPlayers.length < teamSize,
      teamAwayNameExists: teamAwayName.TeamName,
      teamHomeSizeIsMore: teamHomePlayers.length > teamSize,
      teamHomeSizeIsLess: teamHomePlayers.length < teamSize,
      teamHomeNameExists: teamHomeName.TeamName,
      currentTeamPlayersAreMore: currentTeamPlayers.length >= teamSize,
    }
  }
);


const showDefenseSelector = () => createSelector(
  [selectFantasy],
  (selectFantasy) => {
    const {activeTeam, players, exchange, defensePressed} = selectFantasy;
    const choosenPlayers = selectFantasy[`team${inTypesEntitiesNamesFinder('Aliases', activeTeam)}Players`];
    const isDefenseChoosen = choosenPlayers.some(playerId => {
      const player = players.get(playerId);
      return player.Position === 'DEF';
    });
    const currentSportNotAmericanFootball = namesComparator('NFL', exchange.League);
    // __DEV__&&console.assert(currentSportNotAmericanFootball, "currentSportNotAmericanFootball");
    return currentSportNotAmericanFootball && !isDefenseChoosen && defensePressed
  }
);

const notificationSelector = () => createSelector(
  [selectFantasy],
  (selectFantasy) => selectFantasy.notification
);

const defensePressedSelector = () => createSelector(
  [selectFantasy],
  (selectFantasy) => selectFantasy.defensePressed
);

const loadingDefenseSelector = () => createSelector(
  [selectFantasy],
  (selectFantasy) => selectFantasy.loadingDefense
);

const isEditSelector = () => createSelector(
  [selectFantasy],
  (selectFantasy) => selectFantasy.isEdit
);

const exchangeTypeInitialSelector = () => createSelector(
  [selectFantasy],
  (selectFantasy) => selectFantasy.exchangeTypeInitial
);

const serverPlayersSelector = () => createSelector(
  [selectFantasy],
  (selectFantasy) => selectFantasy.serverPlayers
);

export {
  loadingSelector,
  exchangeSelector,
  periodSelector,
  timeEventsSelector,
  currentTimeEventSelector,
  playersSelector,
  loadingPlayersSelector,
  loadingTimeEventsSelector,
  selectedTimeEventsSelector,
  currentTeamFilterSelector,
  currentPositionFilterSelector,
  filteredPlayersSelector,
  activeTeamSelector,
  typeEventSelector,
  teamAwayPlayersSelector,
  teamHomePlayersSelector,
  concatedTeamPlayersSelector,
  teamHomeNameSelector,
  teamAwayNameSelector,
  teamSizeSelector,
  optionExchangesSelector,
  totalSelector,
  showDefenseSelector,
  currentTeamNameSelector,
  currentTeamPlayersSelector,
  validationObjectSelector,
  notificationSelector,
  defensePressedSelector,
  loadingDefenseSelector,
  isEditSelector,
  exchangeTypeInitialSelector,
  serverPlayersSelector,
}
