import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import Tr from './styledComponents/Tr';
import Td from './styledComponents/Td';
import Button from './styledComponents/Button';
import { loadingPlayersSelector, filteredPlayersSelector,
  concatedTeamPlayersSelector, playersSelector, validationObjectSelector,
  currentTeamPlayersSelector,
} from './selectors';
import { addPlayerToSelected, removePlayerFromSelected } from './actions';
import { formatDate } from '../../utils';
import { TablesBodyRow } from './styledComponents/styledComponents';

class AvailablePlayersTableBody extends Component {
  clickHandler = PlayerId => () => {
    const { add, remove, concatedTeamPlayers } = this.props;
    if (!concatedTeamPlayers.includes(PlayerId)) {
      add(PlayerId);
    } else {
      remove(PlayerId);
    }
  };

  render() {
    const { filteredPlayers, loadingPlayers, concatedTeamPlayers, players, validationObject, currentTeamPlayers } = this.props;
    if (loadingPlayers) return <Tr/>;
    const { currentTeamPlayersAreMore } = validationObject;
    return filteredPlayers.map((playerId, key) => {
      const { PlayerId, Position, TeamAlias, LastName, FirstName, StartDate, Eppg, Status, Rank } = players.get(playerId);
      const playerChoosed = concatedTeamPlayers.includes(PlayerId);
      const playerIsInCurrentTeam = currentTeamPlayers.includes(PlayerId);
      return (
        <TablesBodyRow playerChoosed={playerChoosed} playerIsInCurrentTeam={playerIsInCurrentTeam} key={PlayerId}>
          <Td>{key + 1}</Td>
          <Td>{Position}</Td>
          <Td>{TeamAlias}</Td>
          <Td>{`${FirstName} ${LastName}`}</Td>
          <Td>{formatDate(StartDate)}</Td>
          <Td>{Rank || '-'}</Td>
          <Td>{Eppg}</Td>
          <Td>{Status}</Td>
          <Td><Button disabled={currentTeamPlayersAreMore && !playerChoosed} onClick={this.clickHandler(PlayerId)}>{playerChoosed ? 'Delete' : 'Add'}</Button></Td>
        </TablesBodyRow>
      );
    });
  }
}

AvailablePlayersTableBody.propTypes = {
  filteredPlayers: PropTypes.array,
  loadingPlayers: PropTypes.bool,
  add: PropTypes.func,
  remove: PropTypes.func,
  concatedTeamPlayers: PropTypes.array,
  players: PropTypes.instanceOf(Map),
  validationObject: PropTypes.object,
  currentTeamPlayers: PropTypes.array,
};

const mapStateToProps = createStructuredSelector({
  filteredPlayers: filteredPlayersSelector(),
  loadingPlayers: loadingPlayersSelector(),
  concatedTeamPlayers: concatedTeamPlayersSelector(),
  players: playersSelector(),
  validationObject: validationObjectSelector(),
  currentTeamPlayers: currentTeamPlayersSelector,
});

const mapDispatchToProps = dispatch => ({
  add: data => dispatch(addPlayerToSelected(data)),
  remove: data => dispatch(removePlayerFromSelected(data)),
});
export default connect(mapStateToProps, mapDispatchToProps)(AvailablePlayersTableBody);
