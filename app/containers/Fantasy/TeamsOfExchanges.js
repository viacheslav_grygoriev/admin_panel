import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import Button from './styledComponents/Button';
import { currentTeamFilterSelector, selectedTimeEventsSelector, timeEventsSelector } from './selectors';
import { saveCurrentTeamFilter } from './actions';
import { namesComparator } from '../../utils';
import { AllButton, FlexWrapperRow, TeamsButtonsBlock } from './styledComponents/styledComponents';

const teamFilterHandler = clickOnTeamFilter => alias => () => clickOnTeamFilter(alias);
const choosenTeam = (comparator, selectedTeam) => propsTeam => comparator(selectedTeam, propsTeam);

const TeamsOfExchanges = ({ selectedTimeEvents, clickOnTeamFilter, currentTeamFilter, timeEvents }) => {
  const teamFilter = teamFilterHandler(clickOnTeamFilter);
  const isItCurrentTeam = choosenTeam(namesComparator, currentTeamFilter);
  return (
    <FlexWrapperRow>
      Teams:
      {selectedTimeEvents.map((timeEvent) => {
        const { EventId, AwayAlias, HomeAlias } = timeEvents.get(timeEvent);
        return (
          <TeamsButtonsBlock key={EventId}>
            <Button
              onClick={teamFilter(AwayAlias)}
              team
              pressed={isItCurrentTeam(AwayAlias)}
            >{AwayAlias}
            </Button>@<Button
              onClick={teamFilter(HomeAlias)}
              team
              pressed={isItCurrentTeam(HomeAlias)}
            >{HomeAlias}</Button>
          </TeamsButtonsBlock>
        );
      })}
      <AllButton onClick={teamFilter('All')} team pressed={isItCurrentTeam('All')}>All</AllButton>
    </FlexWrapperRow>
  );
};

TeamsOfExchanges.propTypes = {
  selectedTimeEvents: PropTypes.array,
  clickOnTeamFilter: PropTypes.func,
  currentTeamFilter: PropTypes.string,
  timeEvents: PropTypes.instanceOf(Map),
};

const mapStateToProps = createStructuredSelector({
  selectedTimeEvents: selectedTimeEventsSelector(),
  currentTeamFilter: currentTeamFilterSelector(),
  timeEvents: timeEventsSelector(),
});

const mapDispatchToProps = dispatch => ({
  clickOnTeamFilter: data => dispatch(saveCurrentTeamFilter(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(TeamsOfExchanges);
