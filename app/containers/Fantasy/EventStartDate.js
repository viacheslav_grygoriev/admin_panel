import React from 'react';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';

import { exchangeSelector } from './selectors';
import H3 from './styledComponents/H3';
import P from './styledComponents/P';
import { formatDate } from '../../utils';
import { FlexItemForSection } from './styledComponents/styledComponents';

const EventStartDate = ({ exchange }) => {
  const { StartDateEvent } = exchange;
  return (
    <FlexItemForSection>
      <H3>Event start date</H3>
      <P>{formatDate(StartDateEvent, { year: 'numeric' })}</P>
    </FlexItemForSection>
  );
};

EventStartDate.propTypes = {
  exchange: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  exchange: exchangeSelector(),
});
export default connect(mapStateToProps)(EventStartDate);
