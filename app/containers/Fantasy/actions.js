import {
  LOAD_FANTASY_PAGE_DATA, LOAD_FANTASY_PAGE_DATA_SUCCESS, LOAD_FANTASY_PAGE_DATA_ERROR,
  LOAD_TIME_EVENTS, LOAD_TIME_EVENTS_SUCCESS, LOAD_TIME_EVENTS_ERROR,
  LOAD_PLAYERS, LOAD_PLAYERS_SUCCESS, LOAD_PLAYERS_ERROR,
  SAVE_SELECTED_TIME_EVENTS,
  SAVE_CURRENT_TEAM_FILTER, SAVE_CURRENT_POSITION_FILTER,
  START_FILTERING_PLAYERS, SAVE_FILTERED_PLAYERS,
  CHANGE_ACTIVE_TEAM, SAVE_CHANGED_ACTIVE_TEAM,
  CHANGE_TYPE_EVENT, SAVE_CHANGED_TYPE_EVENT,
  CHANGE_TEAM_SIZE, SAVE_CHANGED_TEAM_SIZE,
  ADD_EXCHANGE_TYPES, SAVE_ADDED_EXCHANGE_TYPES,
  REMOVE_EXCHANGE_TYPES, SAVE_REMOVED_EXCHANGE_TYPES,
  CHANGE_EXCHANGE_LIMIT, SAVE_CHANGED_EXCHANGE_LIMIT,
  ADD_PLAYER_TO_SELECTED, SAVE_ADD_PLAYER_TO_SELECTED,
  REMOVE_PLAYER_FROM_SELECTED, SAVE_REMOVE_PLAYER_FROM_SELECTED,
  CHANGE_TEAM_ALIAS, SAVE_CHANGE_TEAM_ALIAS,
  TOTAL_CALCULATE, SAVE_TOTAL_CALCULATE,
  LOAD_DEFENSE, LOAD_DEFENSE_SUCCESS, LOAD_DEFENSE_ERROR,
  SAVE_FANTASY, SAVE_FANTASY_SUCCESS, SAVE_FANTASY_ERROR,
  CANCEL_CLICKED, SAVE_CANCEL_CLICKED,
  CLEAR_STATE,
  HIDE_NOTIFICATION, REQUEST_NOTIFICATION, SHOW_NOTIFICATION,
  CHANGE_DEFENSE_STATE, SAVE_CHANGED_DEFENSE_STATE,
  TEAMS_FOR_EDIT,
  EXCHANGE_TYPE_INITIAL,
  SAVE_SERVER_PLAYERS,
} from './constants';
import { NOTIFICATION_TYPE_ERROR } from '../../utils';

export const loadFantasyPageData = data => ({
  type: LOAD_FANTASY_PAGE_DATA,
  data,
});

export const loadFantasyPageDataSuccess = data => ({
  type: LOAD_FANTASY_PAGE_DATA_SUCCESS,
  data,
});

export const loadFantasyPageDataError = error => ({
  type: LOAD_FANTASY_PAGE_DATA_ERROR,
  error,
});

export const loadTimeEvents = data => ({
  type: LOAD_TIME_EVENTS,
  data,
});

export const loadTimeEventsSuccess = data => ({
  type: LOAD_TIME_EVENTS_SUCCESS,
  data,
});

export const loadTimeEventsError = error => ({
  type: LOAD_TIME_EVENTS_ERROR,
  error,
});

export const loadPlayers = data => ({
  type: LOAD_PLAYERS,
  data,
});

export const loadPlayersSuccess = data => ({
  type: LOAD_PLAYERS_SUCCESS,
  data,
});

export const loadPlayersError = error => ({
  type: LOAD_PLAYERS_ERROR,
  error,
});

export const saveSelectedTimeEvents = data => ({
  type: SAVE_SELECTED_TIME_EVENTS,
  data,
});

export const saveCurrentTeamFilter = data => ({
  type: SAVE_CURRENT_TEAM_FILTER,
  data,
});

export const saveCurrentPositionFilter = data => ({
  type: SAVE_CURRENT_POSITION_FILTER,
  data,
});

export const startFilteringPlayers = data => ({
  type: START_FILTERING_PLAYERS,
  data,
});

export const saveFilteredPlayers = data => ({
  type: SAVE_FILTERED_PLAYERS,
  data,
});

export const changeActiveTeam = () => ({
  type: CHANGE_ACTIVE_TEAM,
});

export const saveChangedActiveTeam = data => ({
  type: SAVE_CHANGED_ACTIVE_TEAM,
  data,
});

export const changeTypeEvent = data => ({
  type: CHANGE_TYPE_EVENT,
  data,
});

export const saveChangedTypeEvent = data => ({
  type: SAVE_CHANGED_TYPE_EVENT,
  data,
});

export const changeTeamSize = data => ({
  type: CHANGE_TEAM_SIZE,
  data,
});

export const saveChangedTeamSize = data => ({
  type: SAVE_CHANGED_TEAM_SIZE,
  data,
});

export const addExchangeTypes = data => ({
  type: ADD_EXCHANGE_TYPES,
  data,
});

export const saveAddedExchangeTypes = data => ({
  type: SAVE_ADDED_EXCHANGE_TYPES,
  data,
});

export const removeExchangeTypes = data => ({
  type: REMOVE_EXCHANGE_TYPES,
  data,
});

export const saveRemovedExchangeTypes = data => ({
  type: SAVE_REMOVED_EXCHANGE_TYPES,
  data,
});

export const changeExchangeLimit = data => ({
  type: CHANGE_EXCHANGE_LIMIT,
  data,
});

export const saveChangedExchangeLimit = data => ({
  type: SAVE_CHANGED_EXCHANGE_LIMIT,
  data,
});

export const addPlayerToSelected = data => ({
  type: ADD_PLAYER_TO_SELECTED,
  data,
});

export const removePlayerFromSelected = data => ({
  type: REMOVE_PLAYER_FROM_SELECTED,
  data,
});

export const saveAddPlayerToSelected = data => ({
  type: SAVE_ADD_PLAYER_TO_SELECTED,
  data,
});

export const saveRemovePlayerFromSelected = data => ({
  type: SAVE_REMOVE_PLAYER_FROM_SELECTED,
  data,
});

export const changeTeamAlias = data => ({
  type: CHANGE_TEAM_ALIAS,
  data,
});

export const saveChangeTeamAlias = data => ({
  type: SAVE_CHANGE_TEAM_ALIAS,
  data,
});

export const totalCalculate = () => ({
  type: TOTAL_CALCULATE,
});

export const saveTotalCalculate = data => ({
  type: SAVE_TOTAL_CALCULATE,
  data,
});

export const loadDefense = data => ({
  type: LOAD_DEFENSE,
  data,
});

export const loadDefenseSuccess = data => ({
  type: LOAD_DEFENSE_SUCCESS,
  data,
});

export const loadDefenseError = error => ({
  type: LOAD_DEFENSE_ERROR,
  error,
});

export const saveFantasy = data => ({
  type: SAVE_FANTASY,
  data,
});

export const saveFantasySuccess = () => ({
  type: SAVE_FANTASY_SUCCESS,
});

export const saveFantasyError = error => ({
  type: SAVE_FANTASY_ERROR,
  error,
});

export const cancelClicked = () => ({
  type: CANCEL_CLICKED,
});

export const saveCancelClicked = () => ({
  type: SAVE_CANCEL_CLICKED,
});

export const clearState = () => ({
  type: CLEAR_STATE,
});

export const requestNotification = ({ message, type = NOTIFICATION_TYPE_ERROR }) => ({
  type: REQUEST_NOTIFICATION,
  data: { message, type },
});

export const showNotification = data => ({
  type: SHOW_NOTIFICATION,
  data,
});

// goes straight to the reducer, without saga
export const hideNotification = () => ({
  type: HIDE_NOTIFICATION,
});

export const changeDefenseState = () => ({
  type: CHANGE_DEFENSE_STATE,
});

export const saveChangedDefenseState = () => ({
  type: SAVE_CHANGED_DEFENSE_STATE,
});

export const teamsForEdit = data => ({
  type: TEAMS_FOR_EDIT,
  data,
});

export const exchangeTypeInitial = data => ({
  type: EXCHANGE_TYPE_INITIAL,
  data,
});

export const saveServerPlayers = data => ({
  type: SAVE_SERVER_PLAYERS,
  data,
});
