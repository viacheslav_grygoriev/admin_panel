import React, { Fragment, Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { StyledCreatableSelect, DefenseSelect, FlexWrapperRow, SelectedPlayersTeamHeader, SelectedPlayersTeamContent } from './styledComponents/styledComponents';

import TableHead from './TableHead';
import SelectedPlayersTableBody from './SelectedPlayersTableBody';
import Button from './styledComponents/Button';
import P from './styledComponents/P';
import { namesComparator, inTypesEntitiesNamesFinder } from '../../utils';
import {
  activeTeamSelector,
  playersSelector,
  totalSelector,
  timeEventsSelector,
  selectedTimeEventsSelector,
  loadingPlayersSelector,
  showDefenseSelector,
  currentTeamNameSelector,
  currentTeamPlayersSelector,
  loadingDefenseSelector,
  validationObjectSelector,
} from './selectors';
import { changeActiveTeam, changeTeamAlias, loadDefense } from './actions';
import FlexWrapper from '../../components/FlexWrapper';
import Loading from '../../components/Loading';

class SelectedPlayers extends Component {
  state = {};

  static getDerivedStateFromProps(props) {
    const { activeTeam, team } = props;
    return { active: namesComparator(activeTeam, team) };
  }

  handleChange = (newValue, actionMeta) => {
    const { onChangeTeamName } = this.props;
    const { action } = actionMeta;
    if (namesComparator('create-option', action)) {
      const aliasRegExp = new RegExp('\\b[a-zA-Z]', 'ig');
      const { value } = newValue;
      newValue.value = value.trim().match(aliasRegExp).join('').slice(0, 5).toUpperCase(); // eslint-disable-line no-param-reassign
    }
    onChangeTeamName({ ...newValue });
  }

  isValidNewOption(inputValue, selectValue, selectOptions) {
    if (
      inputValue.trim().length === 0 ||
      selectOptions.find(option => option.name === inputValue)
    ) {
      return false;
    }
    return true;
  }

  valueLabelForSelectCreator() {
    const { players, currentTeam } = this.props;
    const res = [];
    currentTeam.forEach((playerId) => {
      const { LastName, TeamAlias, Position } = players.get(playerId);
      if (!namesComparator('DEF', Position)) {
        res.push({
          value: `${LastName.slice(0, 5).toUpperCase()}0${TeamAlias}`.toUpperCase(),
          label: `Team ${LastName}`,
        });
      }
    });
    return res;
  }

  clickGenerate = () => {
    const { players, onChangeTeamName, currentTeam } = this.props;
    const res = {};
    let bufEppg = 0;
    currentTeam.forEach((playerId) => {
      const { LastName, TeamAlias, Eppg, Position } = players.get(playerId);
      if (Position !== 'DEF' && (Eppg > bufEppg || !res.value)) {
        bufEppg = Eppg;
        res.value = `${LastName.slice(0, 5).toUpperCase()}0${TeamAlias}`;
        res.label = `Team ${LastName}`;
      }
    });
    onChangeTeamName({ ...res });
  };

  optionsForDefense = () => {
    const { timeEvents, selectedTimeEvents } = this.props;
    const res = [];
    selectedTimeEvents.forEach((timeEventId) => {
      const { AwayId, HomeId, AwayAlias, HomeAlias, AwayTeam, HomeTeam, EventId } = timeEvents.get(timeEventId); // eslint-disable-line no-unused-vars
      ['Away', 'Home'].forEach((team) => {
        res.push({
          value: { TeamId: eval(`${team}Id`), EventId }, // eslint-disable-line no-eval
          label: `${eval(`${team}Team`)} (${AwayAlias}@${HomeAlias})`, // eslint-disable-line no-eval
        });
      });
    });
    return res;
  };

  defenseChange = (data) => {
    const { onLoadDefense } = this.props;
    onLoadDefense({ ...data.value, addToActive: true });
  };

  render() {
    const { team, activeTeam, clickHeader, total, loadingPlayers, showDefense, currentName, currentTeam, loadingDefense, amountOfPlayers, validationObject } = this.props;
    if (loadingPlayers) return <Loading/>;
    const { active } = this.state;
    const { TeamAlias, TeamName } = currentName;
    const { currentTeamPlayersAreMore } = validationObject;
    return (
      <Fragment>
        <SelectedPlayersTeamHeader
          onClick={clickHeader}
          active={active}
        >
          Selected Players: Team {inTypesEntitiesNamesFinder('Aliases', team)} ({amountOfPlayers})
        </SelectedPlayersTeamHeader>
        {active && (
          <SelectedPlayersTeamContent>
            <FlexWrapperRow>
              <StyledCreatableSelect
                isClearable
                allowCreate
                value={{ value: TeamAlias, label: TeamName }}
                onChange={this.handleChange}
                isValidNewOption={this.isValidNewOption}
                options={this.valueLabelForSelectCreator()}
              />
              <Button onClick={this.clickGenerate}>Generate</Button>
            </FlexWrapperRow>
            <TableHead
              arrayOfHeaders={[null, 'Pos', 'Team', 'Player Name', 'Start Date', 'Rank', 'EPPG', 'Status', 'Block EPPG', 'Block Player', null]}
            >
              <SelectedPlayersTableBody
                playersIds={currentTeam}
              />
            </TableHead>
            <div>
              <P>Total: <span>{total[activeTeam]}</span></P>
            </div>
            {showDefense && !loadingDefense && <FlexWrapper>
              <P>Defense: </P>
              <DefenseSelect
                isSearchable={false}
                placeholder="Choose defense"
                options={this.optionsForDefense()}
                onChange={this.defenseChange}
                isDisabled={currentTeamPlayersAreMore}
              />
            </FlexWrapper>}
          </SelectedPlayersTeamContent>
        )}
      </Fragment>
    );
  }
}

SelectedPlayers.propTypes = {
  activeTeam: PropTypes.number,
  team: PropTypes.number,
  clickHeader: PropTypes.func,
  players: PropTypes.instanceOf(Map),
  onChangeTeamName: PropTypes.func,
  total: PropTypes.object,
  timeEvents: PropTypes.instanceOf(Map),
  selectedTimeEvents: PropTypes.array,
  loadingPlayers: PropTypes.bool,
  onLoadDefense: PropTypes.func,
  showDefense: PropTypes.bool,
  currentName: PropTypes.object,
  currentTeam: PropTypes.array,
  loadingDefense: PropTypes.bool,
  amountOfPlayers: PropTypes.number,
  validationObject: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  activeTeam: activeTeamSelector(),
  players: playersSelector(),
  total: totalSelector(),
  timeEvents: timeEventsSelector(),
  selectedTimeEvents: selectedTimeEventsSelector(),
  loadingPlayers: loadingPlayersSelector(),
  showDefense: showDefenseSelector(),
  currentName: currentTeamNameSelector,
  currentTeam: currentTeamPlayersSelector,
  loadingDefense: loadingDefenseSelector(),
  validationObject: validationObjectSelector(),
});

const mapDispatchToProps = dispatch => ({
  clickHeader: () => dispatch(changeActiveTeam()),
  onChangeTeamName: data => dispatch(changeTeamAlias(data)),
  onLoadDefense: data => dispatch(loadDefense(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SelectedPlayers);
