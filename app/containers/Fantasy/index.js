import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { push } from 'react-router-redux';
import { withLastLocation } from 'react-router-last-location';

import { injectReducer, injectSaga } from '../../utils';
import reducer from './reducer';
import saga from './saga';
import { loadFantasyPageData, saveFantasy, cancelClicked, clearState } from './actions';
import { FANTASY_PAGE } from './constants';
import { loadingSelector, exchangeSelector, notificationSelector } from './selectors';
import SelectEvents from './SelectEvents';
import Options from './Options';
import FlexWrapper from '../../components/FlexWrapper';
import AvailablePlayers from './AvailablePlayers';
import SelectedPlayerWrapper from './SelectedPlayersWrapper';
import H1 from './styledComponents/H1';
import H2 from './styledComponents/H2';
import Loading from '../../components/Loading';
import EventName from './EventName';
import EventStartDate from './EventStartDate';
import Button from './styledComponents/Button';
import ModalNotification from '../../components/ModalNotification';
import { FlexWrapperForSection } from './styledComponents/styledComponents';

class Fantasy extends Component {
  state = {};

  componentDidMount() {
    const { loadFantasy, match: { params: { id } }, isEdit } = this.props;
    loadFantasy({ id: isEdit ? id : Number(id), isEdit });
  }

  componentWillUnmount() {
    const { onClearState } = this.props;
    onClearState();
  }

  saveFantasyAndRedirectToNew = () => {
    const { onSaveFantasy, isEdit } = this.props;
    onSaveFantasy({ path: '/sport/New/1', isEdit });
  };

  ifLastLocationRedirect = (fn) => {
    const { lastLocation, isEdit } = this.props;
    if (lastLocation) {
      fn({ path: lastLocation.pathname, isEdit });
    } else {
      fn({ path: isEdit ? '/sport/Approved/1' : '/feed', isEdit });
    }
  };

  saveFantasyAndRedirectToFeed = () => {
    const { onSaveFantasy } = this.props;
    this.ifLastLocationRedirect(onSaveFantasy);
  };

  onClickCancel = () => {
    const { onRedirect, onCancelClick } = this.props;
    this.ifLastLocationRedirect(onRedirect);
    onCancelClick();
  };

  render() {
    const { loading, exchange, notification, isEdit } = this.props;
    if (loading) return <Loading/>;
    const { type, message } = notification;
    const {
      FullName,
    } = exchange;
    return (
      <Fragment>
        <H1>{isEdit ? 'Edit' : 'Create'} Fantasy Exchange</H1>
        <H2>{FullName}</H2>
        <FlexWrapperForSection>
          <SelectEvents/>
          <Options/>
        </FlexWrapperForSection>
        <FlexWrapperForSection>
          <AvailablePlayers/>
          <SelectedPlayerWrapper/>
        </FlexWrapperForSection>
        <FlexWrapperForSection>
          <EventName/>
          <EventStartDate/>
        </FlexWrapperForSection>
        <FlexWrapper>
          <Button onClick={this.saveFantasyAndRedirectToNew}>Save</Button>
          {!isEdit && <Button onClick={this.saveFantasyAndRedirectToFeed}>Save and create New</Button>}
          <Button onClick={this.onClickCancel}>Cancel</Button>
        </FlexWrapper>
        <ModalNotification type={type}>
          {message}
        </ModalNotification>
      </Fragment>
    );
  }
}

Fantasy.propTypes = {
  loadFantasy: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  match: PropTypes.object.isRequired,
  exchange: PropTypes.object,
  onRedirect: PropTypes.func.isRequired,
  lastLocation: PropTypes.oneOfType([
    () => null,
    PropTypes.object,
  ]),
  onSaveFantasy: PropTypes.func,
  onCancelClick: PropTypes.func,
  notification: PropTypes.object,
  onClearState: PropTypes.func,
  isEdit: PropTypes.bool,
};

const mapStateToProps = createStructuredSelector({
  loading: loadingSelector(),
  exchange: exchangeSelector(),
  notification: notificationSelector(),
});

const mapDispatchToProps = dispatch => (
  {
    loadFantasy: data => dispatch(loadFantasyPageData(data)),
    onRedirect: ({ path }) => dispatch(push(path)),
    onSaveFantasy: data => dispatch(saveFantasy(data)),
    onCancelClick: () => dispatch(cancelClicked()),
    onClearState: () => dispatch(clearState()),
  }
);

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: FANTASY_PAGE, reducer });
const withSaga = injectSaga({ key: FANTASY_PAGE, saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
  withLastLocation,
)(Fantasy);
