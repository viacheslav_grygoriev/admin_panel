/* eslint-disable */

import { createSelector } from 'reselect';
import {HOME} from './constants';

const selectHome = (state) => state.get(HOME).toJS();

const homeExchangesSelector = () => createSelector(
  [selectHome],
  (selectHome) => selectHome.homeExchanges
);

const sortAndPageSelector = () => createSelector(
  [selectHome],
  (selectHome) => {
    return {
      SortBy : selectHome.SortBy,
      OrderBy : selectHome.OrderBy,
      Status: selectHome.Status,
      PageInfo: selectHome.PageInfo,
      Path: selectHome.Path,
    }
  }
);

const loadingSelector = () => createSelector(
  [selectHome],
  (selectHome) => selectHome.loading
);

const checkedExchangesSelector = () => createSelector(
  [selectHome],
  (selectHome) => selectHome.checkedExchanges,
);

export {
  homeExchangesSelector,
  sortAndPageSelector,
  loadingSelector,
  checkedExchangesSelector,
}
