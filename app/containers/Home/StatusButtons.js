import React from 'react';
import { Types, namesComparator } from 'utils';
import PropTypes from 'prop-types';
import Button from './styledComponents/Button';
import FlexWrapper from '../../components/FlexWrapper';

const statusExchanges = Object.keys(Types.StatusExchange);
const StatusButtons = ({ statusButtonClick, Status }) => (
  <FlexWrapper>
    {statusExchanges.map(statusExchange => (
      <Button
        key={statusExchange}
        onClick={statusButtonClick(Types.StatusExchange[statusExchange])}
        active={namesComparator(Types.StatusExchange[statusExchange], Status)}
      >
        {statusExchange}
      </Button>
    ))}
  </FlexWrapper>
);

StatusButtons.propTypes = {
  statusButtonClick: PropTypes.func.isRequired,
  Status: PropTypes.number,
};

export default StatusButtons;
