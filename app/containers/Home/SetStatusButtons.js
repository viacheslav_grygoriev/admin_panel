import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Types, SET_STATUS_REMOVED, SET_STATUS_APPROVED, SET_STATUS_SUSPENDED } from '../../utils';
import Button from './styledComponents/Button';
import FlexWrapper from '../../components/FlexWrapper';

const { StatusExchange: { New, Approved, Suspended } } = Types;
const SetStatusButtons = ({ setStatusButtonClick, Status, isSomeExchangeChecked }) => (
  <FlexWrapper>
    {(() => {
      switch (Status) {
        case New:
          return (
            <Fragment>
              <Button disabled={!isSomeExchangeChecked} onClick={setStatusButtonClick({ setStatus: SET_STATUS_REMOVED, status: New })}>Delete</Button>
              <Button disabled={!isSomeExchangeChecked} onClick={setStatusButtonClick({ setStatus: SET_STATUS_APPROVED, status: Approved })}>Set Approved</Button>
            </Fragment>
          );

        case Approved:
          return (
            <Fragment>
              <Button disabled={!isSomeExchangeChecked} onClick={setStatusButtonClick({ setStatus: SET_STATUS_SUSPENDED, status: Suspended })}>Set Suspended</Button>
            </Fragment>
          );

        case Suspended:
          return (
            <Fragment>
              <Button disabled={!isSomeExchangeChecked} onClick={setStatusButtonClick({ setStatus: SET_STATUS_APPROVED, status: Approved })}>Resume (Set
                Approved)</Button>
            </Fragment>
          );

        default:
          return <Fragment/>;
      }
    })()}
  </FlexWrapper>
);

SetStatusButtons.propTypes = {
  setStatusButtonClick: PropTypes.func.isRequired,
  Status: PropTypes.number,
  isSomeExchangeChecked: PropTypes.bool.isRequired,
};

export default SetStatusButtons;
