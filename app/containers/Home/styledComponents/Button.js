import { css } from 'styled-components';
import NormalButton from '../../../components/Button/index';

export default NormalButton.extend`
  margin-right: 10px;
  &:last-child {
    margin-right: 0;
  }
  ${props =>
    props.active && css`
      box-shadow: 1px 1px 1px 1px;
    `}
`;
