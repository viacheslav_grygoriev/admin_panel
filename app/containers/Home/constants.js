export const HOME = 'HOME';

export const GET_HOME_SUCCESS = `altbet/${HOME}/GET_HOME_SUCCESS`;
export const GET_HOME_ERROR = `altbet/${HOME}/GET_HOME_ERROR`;

export const SORT = `altbet/${HOME}/SORT`;
export const PUT_SORT = `altbet/${HOME}/PUT_SORT`;

export const PAGE = `altbet/${HOME}/PAGE`;
export const PUT_PAGE = `altbet/${HOME}/PUT_PAGE`;

export const CHECK_EXCHANGE = `altbet/${HOME}/CHECK_EXCHANGE`;
export const ADD_EXCHANGE_TO_LIST = `altbet/${HOME}/ADD_EXCHANGE_TO_LIST`;
export const REMOVE_EXCHANGE_FROM_LIST = `altbet/${HOME}/REMOVE_EXCHANGE_FROM_LIST`;

export const SET_STATUS_OF_CHECKED_EXCHANGES = `altbet/${HOME}/SET_STATUS_OF_CHECKED_EXCHANGES`;
export const SET_STATUS_OF_CHECKED_EXCHANGES_SUCCESS = `altbet/${HOME}/SET_STATUS_OF_CHECKED_EXCHANGES_SUCCESS`;
export const SET_STATUS_OF_CHECKED_EXCHANGES_ERROR = `altbet/${HOME}/SET_STATUS_OF_CHECKED_EXCHANGES_ERROR`;

export const RESET_STATUS = `altbet/${HOME}/RESET_STATUS`;
