import { Map, Set } from 'immutable';

import {
  GET_HOME_SUCCESS, GET_HOME_ERROR,
  PUT_SORT,
  PUT_PAGE,
  ADD_EXCHANGE_TO_LIST, REMOVE_EXCHANGE_FROM_LIST,
  RESET_STATUS,
  SET_STATUS_OF_CHECKED_EXCHANGES, SET_STATUS_OF_CHECKED_EXCHANGES_SUCCESS, SET_STATUS_OF_CHECKED_EXCHANGES_ERROR,
} from './constants';

const initialState = Map({
  loading: true,
  error: '',
  homeExchanges: null,
  SortBy: 'StartDate',
  OrderBy: 'Asc',
  Status: 1,
  PageInfo: {},
  Path: '',
  checkedExchanges: Set(),
});

export default function homeReducer(state = initialState, action) {
  switch (action.type) {
    case GET_HOME_SUCCESS: {
      const {
        ExchangesMap, SortBy, OrderBy, PageInfo,
      } = action.data;
      return state
        .set('homeExchanges', ExchangesMap)
        .set('SortBy', SortBy)
        .set('OrderBy', OrderBy)
        .set('PageInfo', PageInfo)
        .set('loading', false);
    }

    case GET_HOME_ERROR:
    case SET_STATUS_OF_CHECKED_EXCHANGES_ERROR:
      return state
        .set('error', action.error.message)
        .set('loading', true);

    case PUT_SORT:
      return state
        .set('SortBy', action.data.SortBy)
        .set('OrderBy', action.data.OrderBy);

    case PUT_PAGE:
      return state
        .set('PageInfo', { CurrentPage: action.data.CurrentPage })
        .set('Status', action.data.Status)
        .set('Path', action.data.Path)
        .update('checkedExchanges', val => val.clear())
        .set('loading', true);

    case ADD_EXCHANGE_TO_LIST:
      return state
        .update('checkedExchanges', val => val.add(action.data));

    case REMOVE_EXCHANGE_FROM_LIST: {
      return state
        .update('checkedExchanges', val => val.filter(market => market !== action.data));
    }

    case RESET_STATUS:
      return state
        .set('Status', 1);

    case SET_STATUS_OF_CHECKED_EXCHANGES:
      return state
        .set('loading', true);
    case SET_STATUS_OF_CHECKED_EXCHANGES_SUCCESS:
      return state
        .set('loading', false);

    default:
      return state;
  }
}
