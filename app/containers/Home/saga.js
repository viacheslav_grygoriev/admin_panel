import { push } from 'react-router-redux';
import { select, call, put, all, takeLatest, take } from 'redux-saga/effects';
import {
  request,
  showError,
  ascDescSwitcher,
  EXCHANGE_CONTROLLER_URL,
  GET_EXCHANGES_URL,
  namesComparator,
  SET_STATUS_REMOVED,
} from '../../utils';
import { SORT, PAGE, CHECK_EXCHANGE, SET_STATUS_OF_CHECKED_EXCHANGES } from './constants';
import {
  getHomeExchangesSucces, getHomeExchangesError,
  putSort, putPage,
  addExchangeToList, removeExchangeFromList,
  setStatusOfCheckedExchangesSuccess, setStatusOfCheckedExchangesError,
} from './actions';
import { sortAndPageSelector, checkedExchangesSelector } from './selectors';
import { refreshToken } from '../SiteApp/actions';
import { REFRESH_TOKEN_SUCCESS } from '../SiteApp/constants';
import { requestingTokenSelector } from '../SiteApp/selectors';

export function* getHomeExchangesSaga() {
  const sortAndPage = yield select(sortAndPageSelector());
  const { PageInfo: { CurrentPage } } = sortAndPage;
  try {
    const response = yield call(request, GET_EXCHANGES_URL, {
      method: 'POST',
      body: JSON.stringify({
        SortBy: sortAndPage.SortBy,
        OrderBy: sortAndPage.OrderBy,
        Status: sortAndPage.Status,
        page: CurrentPage,
        Path: sortAndPage.Path,
      }),
    });
    // костыль начало
    if (response.status === 401) {
      const requesting = yield select(requestingTokenSelector());
      if (requesting) {
        yield take(REFRESH_TOKEN_SUCCESS);
      }
      yield put(refreshToken());
      yield take(REFRESH_TOKEN_SUCCESS);
      yield getHomeExchangesSaga();
      return;
    }
    // костыль конец
    const {
      Exchanges, SortBy, OrderBy, PageInfo,
    } = response;
    const ExchangesMap = new Map();
    Exchanges.forEach((exchange) => {
      ExchangesMap.set(exchange.Name, exchange);
    });
    yield put(getHomeExchangesSucces({
      ExchangesMap, SortBy, OrderBy, PageInfo,
    }));
  } catch (error) {
    yield put(getHomeExchangesError(error));
    // yield call(showError, error, getHomeExchangesSaga);
    yield call(showError, error.message);
  }
}

export function* sortSaga(action) {
  const sortAndPage = yield select(sortAndPageSelector());
  yield put(putSort({
    SortBy: action.data,
    OrderBy: ascDescSwitcher(sortAndPage.OrderBy),
  }));
  yield getHomeExchangesSaga();
}

export function* pageSaga(action) {
  const { CurrentPage, Status, Path = '' } = action.data;
  const regexpForPath = /\/sport\/(.+)\/.+\//i;
  const matchedRegexpForPath = Path.match(regexpForPath);
  // __DEV__&&console.warn("pageSaga");
  yield put(putPage({
    CurrentPage,
    Status,
    Path: matchedRegexpForPath ? matchedRegexpForPath[1] : '',
  }));
  yield getHomeExchangesSaga();
}

export function* checkExchangeSaga(action) {
  const { market, checked } = action.data;
  const stringifiedMarket = JSON.stringify(market);
  if (checked) {
    yield put(addExchangeToList(stringifiedMarket));
  } else {
    yield put(removeExchangeFromList(stringifiedMarket));
  }
}

export function* setStatusOfCheckedExchangesSaga(action) {
  // const {} = action.data;
  const checkedExchanges = yield select(checkedExchangesSelector());
  const Exchanges = {};
  checkedExchanges.forEach((exchange) => {
    const [exchangeId, marketId] = JSON.parse(exchange);
    if (!Exchanges[exchangeId]) Exchanges[exchangeId] = [];
    Exchanges[exchangeId].push(marketId);
  });
  const { setStatus, redirectionUrl } = action.data;
  const isStatusRemoved = namesComparator(setStatus, SET_STATUS_REMOVED);
  try {
    const response = yield call(request, `${EXCHANGE_CONTROLLER_URL}/${setStatus}`, {
      method: isStatusRemoved ? 'DELETE' : 'POST',
      body: JSON.stringify({
        Exchanges,
      }),
    });
    // костыль начало
    if (response.status === 401) {
      const requesting = yield select(requestingTokenSelector());
      if (requesting) {
        yield take(REFRESH_TOKEN_SUCCESS);
      }
      yield put(refreshToken());
      yield take(REFRESH_TOKEN_SUCCESS);
      yield call(request, `${EXCHANGE_CONTROLLER_URL}/${setStatus}`, {
        method: isStatusRemoved ? 'DELETE' : 'POST',
        body: JSON.stringify({
          Exchanges,
        }),
      });
    }
    // костыль конец
    yield put(setStatusOfCheckedExchangesSuccess());
    if (isStatusRemoved) {
      yield getHomeExchangesSaga();
    } else {
      yield put(push(redirectionUrl));
    }
  } catch (error) {
    yield put(setStatusOfCheckedExchangesError(error));
    yield call(showError, error.message);
  }
}

export default function* homeSaga() {
  yield all([
    yield takeLatest(SORT, sortSaga),
    yield takeLatest(PAGE, pageSaga),
    yield takeLatest(CHECK_EXCHANGE, checkExchangeSaga),
    yield takeLatest(SET_STATUS_OF_CHECKED_EXCHANGES, setStatusOfCheckedExchangesSaga),
  ]);
}
