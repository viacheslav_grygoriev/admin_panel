import React from 'react';
import PropTypes from 'prop-types';

import Table from './styledComponents/Table';
import Tr from './styledComponents/Tr';
import Td from './styledComponents/Td';
import Button from './styledComponents/Button';
import Input from './styledComponents/Input';
import { exchangeNameFinder, formatDate, inTypesEntitiesNamesFinder, namesComparator, Types } from '../../utils';

const { StatusExchange: { Ended, Suspended, Settlement }, TypeEvent: { Sport } } = Types;

const ExchangesTableBody = ({ mapOfExchanges, onCheckExchange, editClick, currentStatus, setSettlementClick }) => (
  [...mapOfExchanges.values()].map((event) => {
    const compareWithCurrentStatus = status => namesComparator(currentStatus, status);
    const {
      FullName, StartDateEvent, EndDateEvent, TypeEvent, StatusExchange, Symbols, Name,
      Category: {
        CatName,
      },
    } = event;
    const isSettlement = (namesComparator(StatusExchange, Settlement));
    return (
      <Tr bigTable key={Name}>
        <Td colSpan={10}>
          <Table>
            <tbody>
              <Tr underscored>
                <Td width="5%" rowSpan={2}>
                  <Input
                    type="checkbox"
                    disabled={isSettlement}
                    onChange={e => onCheckExchange({
                      exchangeId: Name,
                      markets: Symbols.map(symbol => symbol.Symbol.Name),
                      e,
                    })}
                  />
                </Td>
                <Td width="10%">{FullName}</Td>
                <Td width="10%">{formatDate(StartDateEvent, { year: 'numeric' })}</Td>
                <Td width="10%">{formatDate(EndDateEvent, { year: 'numeric' })}</Td>
                <Td width="10%">Sport</Td>
                <Td width="10%">{CatName}</Td>
                <Td width="10%">
                  {namesComparator(TypeEvent, Sport)
                    ? 'Sport'
                    : 'Fantasy'}
                </Td>
                {namesComparator(StatusExchange, Ended) &&
                <Td width="10%">{inTypesEntitiesNamesFinder('StatusEvent', StatusExchange)}</Td>}
                <Td width="10%" rowSpan={2}>
                  {(() => { // eslint-disable-line consistent-return
                    if (compareWithCurrentStatus(Suspended) || compareWithCurrentStatus(Ended)) {
                      return <Button onClick={setSettlementClick(Name)}>Set Settlement</Button>;
                    } else if (!isSettlement) {
                      return <Button onClick={editClick(Name)}>Edit</Button>;
                    }
                  })()
                  }
                </Td>

              </Tr>
              <Tr underscored>
                <Td colSpan={6}>
                  <Table
                    style={{ width: `${33.3333 * Symbols.filter(symbol => namesComparator(currentStatus, symbol.Symbol.Status)).length}%` }}
                  >
                    <tbody>
                      <Tr>
                        {Symbols.map((symbol) => {
                          const {
                            ID, Symbol: {
                              ExpectedValue, OptionExchange, Name: marketId, Status: marketStatus,
                            },
                          } = symbol;
                          if (namesComparator(currentStatus, marketStatus)) {
                            return (
                              <Td key={ID}>
                                <Table>
                                  <tbody>
                                    <Tr>
                                      <Td width="10%">
                                        <Input
                                          id={ID}
                                          type="checkbox"
                                          disabled={isSettlement}
                                          onChange={e => onCheckExchange({
                                            exchangeId: Name,
                                            markets: [marketId],
                                            e,
                                          })}
                                        />
                                      </Td>
                                      <Td width="60%"><label htmlFor={ID}>{exchangeNameFinder(OptionExchange)}</label></Td>
                                      <Td width="30%"><label htmlFor={ID}>{ExpectedValue}</label></Td>
                                    </Tr>
                                  </tbody>
                                </Table>
                              </Td>
                            );
                          }
                          return <Td key={ID}/>;
                        })}
                      </Tr>
                    </tbody>
                  </Table>
                </Td>
              </Tr>
            </tbody>
          </Table>
        </Td>
      </Tr>
    );
  })
);

ExchangesTableBody.propTypes = {
  mapOfExchanges: PropTypes.instanceOf(Map),
  onCheckExchange: PropTypes.func.isRequired,
  editClick: PropTypes.func.isRequired,
  currentStatus: PropTypes.number.isRequired,
};

export default ExchangesTableBody;

