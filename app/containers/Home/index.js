import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { push } from 'react-router-redux';

import Loading from 'components/Loading';
import Paginate from 'components/Paginate';
import {
  injectReducer,
  Types,
  injectSaga,
  inTypesEntitiesNamesFinder,
} from '../../utils';

import { HOME } from './constants';
import reducer from './reducer';
import saga from './saga';
import { sortRequest, changePage, checkExchange, setStatusOfCheckedExchanges } from './actions';
import {
  homeExchangesSelector,
  loadingSelector,
  checkedExchangesSelector,
  sortAndPageSelector,
} from './selectors';

import H1 from './styledComponents/H1';
import ExchangesTable from './ExchangesTable';
import StatusButtons from './StatusButtons';
import SetStatusButtons from './SetStatusButtons';
import ExchangesTableBody from './ExchangesTableBody';

class Home extends Component {
  state = {
    currentPage: null,
    currentStatusName: null,
  };

  componentDidMount() {
    const {
      onChangePage,
      sortAndPage,
      match: { params: { page, status }, url },
    } = this.props;
    const { CurrentPage, Status } = sortAndPage;
    const curPage = Number(CurrentPage || page);
    const curStatus = status || inTypesEntitiesNamesFinder('StatusExchange', Status);
    // __DEV__&&console.warn("did mount");
    // __DEV__&&console.log("this.props", this.props);
    // __DEV__&&console.log("Status -", inTypesEntitiesNamesFinder('StatusExchange', Status), '; status -', status);
    // __DEV__&&console.log("curStatus", curStatus);
    this.setState({ currentPage: curPage, currentStatusName: curStatus }); // eslint-disable-line react/no-did-mount-set-state

    onChangePage({ CurrentPage: curPage, Status: Types.StatusExchange[curStatus], Path: url });
  }

  getSnapshotBeforeUpdate(prevProps) {
    const { match: { params: { page, status }, url } } = this.props;
    const { match: { params: { page: prevPage, status: prevStatus }, url: prevUrl } } = prevProps;
    // __DEV__&&console.warn("beforeUpdate");
    // __DEV__&&console.log("beforeUpdate", 'status -', status, '; prevStatus -', prevStatus);
    // __DEV__&&console.log("beforeUpdate", 'page -', page, '; prevPage -', prevPage);
    // __DEV__&&console.log("beforeUpdate", 'url -', url, '; prevUrl -', prevUrl);
    // __DEV__&&console.log("prevPage !== page || prevStatus !== status || url !== prevUrl", prevPage !== page || prevStatus !== status || url !== prevUrl);
    if (prevPage !== page || prevStatus !== status || url !== prevUrl) {
      return {
        page: page || prevPage,
        status: status || prevStatus,
        url: url || prevUrl,
      };
    }
    return null;
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const {
      onChangePage,
    } = this.props;
    if (snapshot) {
      const { page, status, url } = snapshot;
      onChangePage({ CurrentPage: Number(page), Status: Types.StatusExchange[status], Path: url });
      this.setState({ currentPage: Number(page), currentStatusName: status }); // eslint-disable-line react/no-did-update-set-state
      // __DEV__&&console.warn("didUpdate");
      // __DEV__&&console.log("didUpdate status", status);
    }
  }

  editClick = exchangeId => () => {
    const { pushToEditPage } = this.props;
    pushToEditPage(exchangeId);
  };

  sortClick = columnName => () => {
    const { sortColumn } = this.props;
    sortColumn(columnName);
  };

  statusButtonClick = Status => () => {
    const { pushToPage } = this.props;
    // __DEV__&&console.warn("status button click");
    // __DEV__&&console.log("status click Status", Status);
    pushToPage(inTypesEntitiesNamesFinder('StatusExchange', Status), 1);
  };

  paginationClick = ({ selected }) => {
    const { pushToPage } = this.props;
    const { currentStatusName } = this.state;
    const currentPageNumber = selected + 1;
    // __DEV__&&console.warn("pagination click");
    // __DEV__&&console.log("currentStatusName", currentStatusName);
    pushToPage(currentStatusName, currentPageNumber);
  };

  setStatusButtonClick = ({ setStatus, status }) => () => {
    const { onSetStatusOfCheckedExchanges } = this.props;
    const redirectionUrl = urlForPushCreator(inTypesEntitiesNamesFinder('StatusExchange', status), 1);
    onSetStatusOfCheckedExchanges({ setStatus, redirectionUrl });
  };

  onCheckExchange = (data) => {
    const { onChangeCheckbox } = this.props;
    const { exchangeId, markets, e } = data;
    markets.forEach((market) => {
      onChangeCheckbox({ market: [exchangeId, market], checked: e.target.checked });
    });
  };

  setSettlement = eventId => () => {
    const { pushToSetSettlementPage } = this.props;
    pushToSetSettlementPage(eventId);
  };

  render() {
    const { mapOfExchanges, loading, sortAndPage, checkedExchanges } = this.props;

    const {
      SortBy,
      OrderBy,
      Status,
      PageInfo,
    } = sortAndPage;
    const { currentPage } = this.state;
    // __DEV__&&console.warn("render");
    // __DEV__ && console.log('Status', Status);
    return (
      <div>
        <H1>Created Exchanges</H1>
        <StatusButtons statusButtonClick={this.statusButtonClick} Status={Status}/>
        <SetStatusButtons
          setStatusButtonClick={this.setStatusButtonClick}
          Status={Status}
          isSomeExchangeChecked={!!checkedExchanges.length}
        />
        {loading ?
          <Loading/>
          :
          <Fragment>
            <ExchangesTable sortClick={this.sortClick} sortBy={SortBy} orderBy={OrderBy} status={Status}>
              <ExchangesTableBody
                mapOfExchanges={mapOfExchanges}
                onCheckExchange={this.onCheckExchange}
                editClick={this.editClick}
                setSettlementClick={this.setSettlement}
                currentStatus={Status}
              />
            </ExchangesTable>
            <Paginate pageInfo={PageInfo} page={currentPage} handlePageClick={this.paginationClick}/>
          </Fragment>
        }
      </div>
    );
  }
}

Home.propTypes = {
  mapOfExchanges: PropTypes.instanceOf(Map),
  pushToEditPage: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  sortColumn: PropTypes.func.isRequired,
  sortAndPage: PropTypes.object.isRequired,
  onChangePage: PropTypes.func.isRequired,
  pushToPage: PropTypes.func.isRequired,
  pushToSetSettlementPage: PropTypes.func.isRequired,
  match: PropTypes.object.isRequired,
  onChangeCheckbox: PropTypes.func.isRequired,
  onSetStatusOfCheckedExchanges: PropTypes.func.isRequired,
  checkedExchanges: PropTypes.array.isRequired,
};

const urlForPushCreator = (currentStatusName, pageNumber) => {
  const path = window.location.pathname.match(/\/sport\/.+/i)[0];
  const splittedPathname = path.split('/');

  let urlForPush = `/${currentStatusName}/${pageNumber}`;
  if (splittedPathname.length > 3) {
    const mainUrl = splittedPathname
      .slice(0, -2)
      .reduce((acc, curItem) => `${acc}/${curItem}`);
    urlForPush = `${mainUrl}/${currentStatusName}/${pageNumber}`;
  }
  return urlForPush;
};

const mapStateToProps = createStructuredSelector({
  mapOfExchanges: homeExchangesSelector(),
  loading: loadingSelector(),
  sortAndPage: sortAndPageSelector(),
  checkedExchanges: checkedExchangesSelector(),
});

function mapDispatchToProps(dispatch) {
  return {
    pushToEditPage: eventId => dispatch(push(`/edit/${eventId}`)),
    pushToSetSettlementPage: eventId => dispatch(push(`/setSettlement/${eventId}`)),
    sortColumn: columnName => dispatch(sortRequest(columnName)),
    onChangePage: data => dispatch(changePage(data)),
    pushToPage: (currentStatusName, pageNumber) => {
      dispatch(push(urlForPushCreator(currentStatusName, pageNumber)));
    },
    onChangeCheckbox: data => dispatch(checkExchange(data)),
    onSetStatusOfCheckedExchanges: data => dispatch(setStatusOfCheckedExchanges(data)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);
const withReducer = injectReducer({ key: HOME, reducer });
const withSaga = injectSaga({ key: HOME, saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Home);
