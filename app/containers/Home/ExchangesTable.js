import React from 'react';
import PropTypes from 'prop-types';
import { namesComparator, Types } from 'utils';

import Table from './styledComponents/Table';
import Tr from './styledComponents/Tr';
import Th from './styledComponents/Th';
import Span from './styledComponents/Span';
import Input from './styledComponents/Input';

export default function ExchangesTable(props) {
  const {
    children, sortClick, sortBy, orderBy, status,
  } = props;
  const names = {
    FullName: 'FullName',
    StartDate: 'StartDate',
    EndDate: 'EndDate',
    SportName: 'SportName',
    CategoryName: 'CategoryName',
    Type: 'Type',
  };
  return (
    <Table>
      <thead>
        <Tr bigTableHeading>
          {(status === Types.StatusExchange.New
            || status === Types.StatusExchange.Approved
            || status === Types.StatusExchange.Suspended) && (
            <Th width="5%"><Input type="checkbox" /></Th>
          )}
          <Th width="10%" onClick={sortClick(names.FullName)}>
            <Span sortable active={namesComparator(names.FullName, sortBy)} orderBy={orderBy}>Full name</Span>
          </Th>
          <Th width="10%" onClick={sortClick(names.StartDate)}>
            <Span sortable active={namesComparator(names.StartDate, sortBy)} orderBy={orderBy}>Start date</Span>
          </Th>
          <Th width="10%" onClick={sortClick(names.EndDate)}>
            <Span sortable active={namesComparator(names.EndDate, sortBy)} orderBy={orderBy}>End date</Span>
          </Th>
          <Th width="10%" onClick={sortClick(names.SportName)}>
            <Span sortable active={namesComparator(names.SportName, sortBy)} orderBy={orderBy}>Sport</Span>
          </Th>
          <Th width="10%" onClick={sortClick(names.CategoryName)}>
            <Span sortable active={namesComparator(names.CategoryName, sortBy)} orderBy={orderBy}>League</Span>
          </Th>
          <Th width="10%" onClick={sortClick(names.Type)}>
            <Span sortable active={namesComparator(names.Type, sortBy)} orderBy={orderBy}>Type</Span>
          </Th>


          {status === Types.StatusExchange.Ended && <Th>Status Event</Th>}
          {status === Types.StatusExchange.Settlement && <Th>Result</Th>}
          {status !== Types.StatusExchange.Settlement && <Th width="10%" />}
        </Tr>
      </thead>
      <tbody>{children}</tbody>
    </Table>
  );
}

ExchangesTable.propTypes = {
  children: PropTypes.object,
  sortClick: PropTypes.func,
  sortBy: PropTypes.string,
  orderBy: PropTypes.string,
  status: PropTypes.number,
};
