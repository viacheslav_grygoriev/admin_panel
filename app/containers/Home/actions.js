import {
  GET_HOME_SUCCESS, GET_HOME_ERROR,
  SORT, PUT_SORT,
  PAGE, PUT_PAGE,
  CHECK_EXCHANGE, ADD_EXCHANGE_TO_LIST, REMOVE_EXCHANGE_FROM_LIST,
  SET_STATUS_OF_CHECKED_EXCHANGES,
  RESET_STATUS, SET_STATUS_OF_CHECKED_EXCHANGES_SUCCESS, SET_STATUS_OF_CHECKED_EXCHANGES_ERROR,
} from './constants';

export const getHomeExchangesSucces = data => ({
  type: GET_HOME_SUCCESS,
  data,
});

export const getHomeExchangesError = error => ({
  type: GET_HOME_ERROR,
  error,
});

export const sortRequest = data => ({
  type: SORT,
  data,
});

export const putSort = data => ({
  type: PUT_SORT,
  data,
});

export const changePage = data => ({
  type: PAGE,
  data,
});

export const putPage = data => ({
  type: PUT_PAGE,
  data,
});

export const checkExchange = data => ({
  type: CHECK_EXCHANGE,
  data,
});

export const addExchangeToList = data => ({
  type: ADD_EXCHANGE_TO_LIST,
  data,
});

export const removeExchangeFromList = data => ({
  type: REMOVE_EXCHANGE_FROM_LIST,
  data,
});

export const setStatusOfCheckedExchanges = data => ({
  type: SET_STATUS_OF_CHECKED_EXCHANGES,
  data,
});
export const setStatusOfCheckedExchangesSuccess = () => ({
  type: SET_STATUS_OF_CHECKED_EXCHANGES_SUCCESS,
});
export const setStatusOfCheckedExchangesError = error => ({
  type: SET_STATUS_OF_CHECKED_EXCHANGES_ERROR,
  error,
});

// this action goes directly in reducer
export const resetStatus = () => ({
  type: RESET_STATUS,
});
