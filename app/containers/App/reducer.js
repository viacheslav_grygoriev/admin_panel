import { Map } from 'immutable';

const initialState = Map({
  error: null,
});

export default function appReducer(state = initialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}
