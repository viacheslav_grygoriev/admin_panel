/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import styled from 'styled-components';
import PropTypes from 'prop-types';
// import { connect } from 'react-redux';
import { compose } from 'redux';
import { LastLocationProvider } from 'react-router-last-location';

import saga from './saga';
import reducer from './reducer';

import { APP } from './constants';

import HomePage from '../../containers/Home/Loadable';
import NotFoundPage from '../../containers/NotFoundPage/Loadable';
import Header from '../../containers/Header';
import Sidebar from '../../containers/Sidebar/Loadable';
import AddNew from '../../containers/AddNew/Loadable';
import FeedPage from '../../containers/Feed/Loadable';
import Sport from '../../containers/Sport/Loadable';
import Fantasy from '../../containers/Fantasy/Loadable';
import Category from '../../containers/Category/Loadable';
import SetSettlementPage from '../../containers/SetSettlement/Loadable';
import { injectReducer, injectSaga } from '../../utils';

const AppWrapper = styled.div``;

const Main = styled.div`
  display: flex;
`;

const DynamicComponentsWrapper = styled.main`
  width: 100%;
  min-height: calc(100vh - 50px);
  padding: 20px;
`;

function App(props) {
  const { location: { pathname } } = props;
  const splittedPathname = pathname.split('/');
  return (
    <AppWrapper>
      <Header/>
      <Main>
        <Sidebar pathname={pathname}/>
        <DynamicComponentsWrapper>
          <LastLocationProvider>
            <Switch>
              <Route path="/user/:addUser" component={AddNew}/>
              <Route path="/feed/create/Sport/:id" component={Sport}/>
              <Route path="/feed/create/Fantasy/:id" component={Fantasy}/>
              <Route path="/category/:action/:categoryUrl" component={Category}/>

              <Redirect exact from="/" to="/sport/Approved/1"/>
              <Redirect exact from="/sport" to="/sport/Approved/1"/>

              {(() => { // eslint-disable-line consistent-return
                if (/^sport$/i.test(splittedPathname[1])) {
                  const splittedPathnameLength = splittedPathname.slice(2, -2).length;
                  let urlPattern = '/sport';
                  for (let i = 0; i < splittedPathnameLength; i += 1) {
                    urlPattern += `/:subcategory${i}`;
                  }
                  urlPattern += '/:status/:page';
                  return <Route path={urlPattern} component={HomePage}/>;
                }
              })()}

              <Redirect exact from="/feed" to="/feed/page/1"/>
              <Route path="/feed/page/:page" component={FeedPage}/>

              <Route path="/edit/:id" render={() => <Fantasy isEdit/>} />

              <Route path="/setSettlement/:exchangeName" component={SetSettlementPage}/>

              <Route component={NotFoundPage}/>
            </Switch>
          </LastLocationProvider>
        </DynamicComponentsWrapper>
      </Main>
    </AppWrapper>
  );
}

App.propTypes = {
  location: PropTypes.object.isRequired,
};

// const withConnect = connect(null, null);

const withReducer = injectReducer({ key: APP, reducer });
const withSaga = injectSaga({ key: APP, saga });

export default compose(
  withReducer,
  withSaga,
  // withConnect
)(App);

// export default App;
