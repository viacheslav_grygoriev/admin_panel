/* eslint-disable */

import { createSelector } from 'reselect';
import { FEED } from 'containers/Feed/constants';
import { HOME } from 'containers/Home/constants';

const selectFeed = state => state.get(FEED).toJS();
const selectHome = state => state.get(HOME).toJS();

const feedPageInfoSelector = () => createSelector(
  [selectFeed],
  (selectFeed) => selectFeed.PageInfo
);

const homePageInfoSelector = () => createSelector(
  [selectHome],
  (selectHome) => selectHome.PageInfo
);

export {
  feedPageInfoSelector,
  homePageInfoSelector,
};
