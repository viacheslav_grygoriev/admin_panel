// import request from '../../utils/request';
import { all, put, takeLatest /* call */ } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import {
  startSubmit,
  stopSubmit,
  reset, /* SubmissionError */
} from 'redux-form';
import { showError } from 'utils';
import { countries } from './staticData';

import { ADDNEW_FORM, SEND_COUNTRIES_REQUEST } from './constants';
import {
  sendNewFormError,
  hideNewFormError,
  sendCountriesRequestSuccess,
} from './actions';

export function* addNewForm(action) {
  const { formName, fields } = action.data;
  yield put(startSubmit(formName));
  try {
    const formData = new FormData();
    Object.entries(fields).forEach(([name, value]) => {
      formData.append(name, value);
    });

    // const response = yield call(fetch,
    //   'http://localhost/AltBet.Admin/user/addadmin', {
    //   method: 'POST',
    //   mode: 'no-cors',
    //   headers: {
    //     'Content-Type': 'application/json',
    //     "Access-Control-Allow-Origin" : "*",
    //     "Access-Control-Allow-Credentials" : true
    //   },
    //   body: formData,
    // });
    // let resp = yield response;
    // __DEV__&&console.log("resp", resp);

    // throw new SubmissionError({username: 'wrong user'});

    yield put(reset(formName));
    yield put(stopSubmit(formName));
  } catch (error) {
    yield put(stopSubmit(formName, error.errors));
    yield put(sendNewFormError(error.message));
    yield delay(5000);
    yield put(hideNewFormError());
    showError(error.message);
  }
}

export function* countriesRequest() {
  // const response = yield call(fetch,
  //   'http://localhost/AltBet.Admin/user/getcountries', {
  //   method: 'POST',
  //   mode: 'no-cors',
  //   headers: new Headers({
  //     'Content-Type': 'application/json',
  //     "Access-Control-Allow-Origin" : "*",
  //     "Access-Control-Allow-Credentials" : true
  //   }),
  // });
  // __DEV__&&console.log("resp", response.text().then(res => __DEV__&&console.log("res", res)  ) );
  yield put(sendCountriesRequestSuccess(JSON.parse(countries)));
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* addNew() {
  yield all([
    yield takeLatest(ADDNEW_FORM, addNewForm),
    yield takeLatest(SEND_COUNTRIES_REQUEST, countriesRequest),
  ]);
}
