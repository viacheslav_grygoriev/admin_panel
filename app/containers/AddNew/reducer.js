import { Map } from 'immutable';

import {
  SEND_ADDNEW_FORM,
  SEND_ADDNEW_FORM_ERROR,
  HIDE_ADDNEW_FORM_ERROR,
  SEND_COUNTRIES_REQUEST,
  SEND_COUNTRIES_REQUEST_SUCCESS,
  SEND_COUNTRIES_REQUEST_ERROR,
} from './constants';

// The initial state of the App
const initialState = Map({
  loading: false,
  error: '',
  countries: [],
});

function addNewReducer(state = initialState, action) {
  switch (action.type) {
    case SEND_ADDNEW_FORM:
      return state.set('loading', true);
    case SEND_ADDNEW_FORM_ERROR:
      return state.set('loading', false).set('error', action.data);
    case HIDE_ADDNEW_FORM_ERROR:
      return state.set('error', '');
    case SEND_COUNTRIES_REQUEST:
      return state.set('loading', true);
    case SEND_COUNTRIES_REQUEST_SUCCESS:
      return state.set('loading', false).set('countries', action.data);
    case SEND_COUNTRIES_REQUEST_ERROR:
      return state.set('loading', false);

    default:
      return state;
  }
}

export default addNewReducer;
