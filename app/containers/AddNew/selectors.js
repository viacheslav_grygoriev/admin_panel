/* eslint-disable */

/**
 * AddNew selectors
 */

import { createSelector, toJS } from 'reselect';
import {ADDNEW} from './constants';

const selectAddNew = (state) => state.get(ADDNEW).toJS();
const selectUserRegisterForm = (state) => state.get('form');

const loadingSelector = () => createSelector(
  [selectAddNew],
  (selectAddNew) => selectAddNew.loading
);

const errorSelector = () => createSelector(
  [selectAddNew],
  (selectAddNew) => selectAddNew.error
);

const countriesSelector = () => createSelector(
  [selectAddNew],
  (selectAddNew) => selectAddNew.countries
);

const statesSelector = () => createSelector(
  [selectAddNew, selectUserRegisterForm],
  (selectAddNew, selectUserRegisterForm) => {

    const currentCountry = selectUserRegisterForm
      && selectUserRegisterForm['UserRegisterForm']
      && selectUserRegisterForm['UserRegisterForm']['values']
      && selectUserRegisterForm['UserRegisterForm']['values']['country'];

    if (currentCountry){
      const currentCountryStates = selectAddNew.countries.find(country => {
        return country.Code === currentCountry;
      });
      return currentCountryStates.States;
    }
  }
);

export {
  errorSelector,
  loadingSelector,
  countriesSelector,
  statesSelector,
};
