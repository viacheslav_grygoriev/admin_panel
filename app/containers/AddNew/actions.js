import {
  ADDNEW_LOAD,
  ADDNEW_FORM,
  // SEND_ADDNEW_FORM_SUCCESS,
  SEND_ADDNEW_FORM_ERROR,
  HIDE_ADDNEW_FORM_ERROR,
  // SEND_ADDNEW_FORM,
  SEND_COUNTRIES_REQUEST,
  SEND_COUNTRIES_REQUEST_SUCCESS,
  // SEND_COUNTRIES_REQUEST_ERROR,
} from './constants';

export function addNewLoad() {
  return {
    type: ADDNEW_LOAD,
  };
}

export function addNewForm(formName, fields) {
  return {
    type: ADDNEW_FORM,
    data: {
      formName,
      fields,
    },
  };
}

export function sendNewFormError(data) {
  return {
    type: SEND_ADDNEW_FORM_ERROR,
    data,
  };
}

export function hideNewFormError() {
  return {
    type: HIDE_ADDNEW_FORM_ERROR,
  };
}

export function sendCountriesRequest() {
  return {
    type: SEND_COUNTRIES_REQUEST,
  };
}

export function sendCountriesRequestSuccess(data) {
  return {
    type: SEND_COUNTRIES_REQUEST_SUCCESS,
    data,
  };
}
