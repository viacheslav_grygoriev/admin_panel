import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { compose } from 'redux';
import { connect } from 'react-redux';

import AdminRegisterForm from 'components/AdminRegisterForm/index';
import UserRegisterForm from 'components/UserRegisterForm/index';
import { injectReducer, injectSaga } from 'utils';
import H1 from './styledComponents/H1';
import saga from './saga';
import reducer from './reducer';
import { addNewLoad } from './actions';
import { ADDNEW } from './constants';

const Wrapper = styled.div`
  width: 50%;
  @media (max-width: 1000px) {
    width: 100%;
  }
`;

class AddNew extends React.Component {
  // eslint-disable-line react/prefer-stateless-function

  componentDidMount() {
    const { onGetAddNew } = this.props;
    onGetAddNew();
  }
  render() {
    const {
      match: {
        params: { addUser },
      },
    } = this.props;
    return (
      <Wrapper>
        {(() => {
          switch (addUser) {
            case 'addadmin':
              return (
                <div key={addUser}>
                  <H1>Add New Admin Account</H1>
                  <AdminRegisterForm />
                </div>
              );
            case 'adduser':
              return (
                <div key={addUser}>
                  <H1>Add New User Account</H1>
                  <UserRegisterForm />
                </div>
              );

            default:
              return (
                <div key={addUser}>
                  <H1>Add New User Account</H1>
                </div>
              );
          }
        })()}
      </Wrapper>
    );
  }
}

AddNew.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      addUser: PropTypes.string.isRequired,
    }),
  }),
  onGetAddNew: PropTypes.func.isRequired,
};

export function mapDispatchToProps(dispatch) {
  return {
    onGetAddNew: () => dispatch(addNewLoad()),
  };
}

const withConnect = connect(
  null,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: ADDNEW, reducer });
const withSaga = injectSaga({ key: ADDNEW, saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(AddNew);
