import styled, { css } from 'styled-components';
import { ERROR_COLOR } from 'utils';

export const ItemsWrapper = styled.div`
  display: flex;
  width: 60%;
  margin: 15px 0;
`;

export const ItemWrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex-basis: 50%;
`;

export const OneLineWrapper = styled.div`
  display: flex;
`;

export const LabeledInputWrapper = styled.div`
  margin-right: 10px;
  flex-basis: calc(33.3% - 10px);
  &:last-child {
    margin-right: 0;
  }
`;

export const ErrorButtonsWrapper = styled.div`
  align-self: flex-start;
  ${props =>
    props.error &&
    css`
      outline: 1px solid ${ERROR_COLOR};
    `};
`;
