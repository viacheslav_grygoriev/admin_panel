import {
  LOAD_SPORT_PAGE_DATA,
  LOAD_SPORT_PAGE_DATA_SUCCESS,
  LOAD_SPORT_PAGE_DATA_ERROR,
  PRESS_FAVORITE_BUTTON,
  PRESS_EXCHANGE_TYPES_BUTTON,
  CHANGE_EXCHANGE_TYPE,
  VALIDATE_DATA,
  PUT_VALIDATE_DATA,
  SEND_FORM_DATA,
  SEND_FORM_DATA_START,
  SEND_FORM_DATA_SUCCESS,
  SEND_FORM_DATA_ERROR,
} from './constants';

export function sportPageLoadData() {
  return {
    type: LOAD_SPORT_PAGE_DATA,
  };
}

export function sportPageLoadDataSuccess(data) {
  return {
    type: LOAD_SPORT_PAGE_DATA_SUCCESS,
    data,
  };
}

export function sportPageLoadDataError(error) {
  return {
    type: LOAD_SPORT_PAGE_DATA_ERROR,
    error,
  };
}

export function pressFavoriteButton(data) {
  return {
    type: PRESS_FAVORITE_BUTTON,
    data,
  };
}

export function pressExchangeTypesButton(data) {
  return {
    type: PRESS_EXCHANGE_TYPES_BUTTON,
    data,
  };
}

export function changeExchangeType(data) {
  return {
    type: CHANGE_EXCHANGE_TYPE,
    data,
  };
}

export function validateData(data) {
  return {
    type: VALIDATE_DATA,
    data,
  };
}

export function putValidateData(data) {
  return {
    type: PUT_VALIDATE_DATA,
    data,
  };
}

export function sendFormData(data) {
  return {
    type: SEND_FORM_DATA,
    data,
  };
}

export function sendFormDataStart() {
  return {
    type: SEND_FORM_DATA_START,
  };
}

export function sendFormDataSuccess(data) {
  return {
    type: SEND_FORM_DATA_SUCCESS,
    data,
  };
}

export function sendFormDataError(error) {
  return {
    type: SEND_FORM_DATA_ERROR,
    error,
  };
}
