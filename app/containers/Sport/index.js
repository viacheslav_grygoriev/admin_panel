import React from 'react';

import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { push } from 'react-router-redux';
import RichTextEditor from 'react-rte';
import { withLastLocation } from 'react-router-last-location';

import { injectReducer, injectSaga, formatDate, generateInputDate, Types } from 'utils';

import LabeledInput from 'components/LabeledInput';
import ToggleButton from 'components/ToggleButton';
import Loading from 'components/Loading';

import { SPORT_PAGE } from './constants';
import saga from './saga';
import reducer from './reducer';
import {
  sportPageLoadData,
  pressFavoriteButton,
  pressExchangeTypesButton,
  validateData,
  sendFormData,
} from './actions';
import { makeSelectLoading, makeSelectValidationObject } from './selectors';

import Button from './styledComponents/Button';
import H3 from './styledComponents/H3';
import H1 from './styledComponents/H1';
import P from './styledComponents/P';
import {
  ItemsWrapper,
  ItemWrapper,
  OneLineWrapper,
  LabeledInputWrapper,
  ErrorButtonsWrapper,
} from './styledComponents/styledComponents';
import FlexWrapper from '../../components/FlexWrapper';

class Sport extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      textEditorValue: RichTextEditor.createEmptyValue(),
    };

    this.clickCancel = this.clickCancel.bind(this);
    this.onChangeTextEditor = this.onChangeTextEditor.bind(this);
    this.onCreateEvent = this.onCreateEvent.bind(this);
    this.onChangeFavorite = this.onChangeFavorite.bind(this);
    this.validateValue = this.validateValue.bind(this);
    this.nameFavoriteButton = this.nameFavoriteButton.bind(this);
    this.validateAllValues = this.validateAllValues.bind(this);
  }

  componentDidMount() {
    const { loadPage } = this.props;
    loadPage();
  }

  clickCancel() {
    const { pushCancel, lastLocation } = this.props;
    if (lastLocation) {
      pushCancel(lastLocation.pathname);
    } else {
      pushCancel('/feed');
    }
  }

  onChangeTextEditor(text) {
    const { onValidateData } = this.props;

    this.setState({ textEditorValue: text });
    onValidateData({
      Comment:
        this.state.textEditorValue.toString('html') !== '<p><br></p>'
          ? this.state.textEditorValue.toString('html')
          : '',
    });
  }

  validateValue(ref) {
    const { onValidateData } = this.props;
    if (ref) {
      onValidateData({ [ref.props.name]: ref.getData() });
    }
    return ref && ref.getData();
  }

  validateAllValues() {
    let valid = true;
    const {
      validationDataObject: {
        Spread,
        Moneyline,
        TotalPoints,
        FullName,
        StartDate,
        Favorite,
        AwayName,
        HomeName,
        SpreadButton,
        MoneylineButton,
        TotalPointsButton,
        EventId,
      },
    } = this.props;
    if (!AwayName) {
      valid = false;
    }
    if (!HomeName) {
      valid = false;
    }

    if (!Favorite) {
      valid = false;
    }
    if (!EventId) {
      valid = false;
    }
    if (!StartDate) {
      valid = false;
    }
    if (!FullName) {
      valid = false;
    }

    if (!(SpreadButton || MoneylineButton || TotalPointsButton)) {
      valid = false;
    }
    if (SpreadButton && !(SpreadButton && Spread)) {
      valid = false;
    }
    if (MoneylineButton && !(MoneylineButton && Moneyline)) {
      valid = false;
    }
    if (TotalPointsButton && !(TotalPointsButton && TotalPoints)) {
      valid = false;
    }

    return valid;
  }

  onCreateEvent() {
    const { onSubmitForm, validationDataObject } = this.props;
    onSubmitForm(validationDataObject);
  }

  onChangeFavorite(type) {
    const { ChangeFavorite } = this.props;
    return () => ChangeFavorite(type);
  }

  nameFavoriteButton(name) {
    const { Favorite } = this.props.validationDataObject;
    return Favorite === name ? 'Favorite' : 'Outsider';
  }

  render() {
    const { loading, onChangeExchangeType } = this.props;
    const {
      validationDataObject: {
        Spread,
        Moneyline,
        TotalPoints,
        FullName,
        StartDate,
        Favorite,
        AwayName,
        HomeName,
        SpreadButton,
        MoneylineButton,
        TotalPointsButton,
      },
    } = this.props;

    if (loading) return <Loading/>;
    return (
      <div>
        <H1>Create Sport Event</H1>
        <H3>
          {FullName} ({formatDate(StartDate, { year: 'numeric' })})
        </H3>
        <ItemsWrapper>
          <ItemWrapper>
            <LabeledInput
              name="AwayName"
              label="Away team"
              value={AwayName}
              ref={(val) => {
                this.AwayName = val;
              }}
              onChange={() => this.validateValue(this.AwayName)}
            />
            <div>
              <Button
                onClick={this.onChangeFavorite(Types.Aliases.AwayAlias)}
                pressed={Favorite === Types.Aliases.AwayAlias}
              >
                {this.nameFavoriteButton(Types.Aliases.AwayAlias)}
              </Button>
            </div>
          </ItemWrapper>
          <ItemWrapper>
            <LabeledInput
              name="HomeName"
              label="Home team"
              value={HomeName}
              ref={(val) => {
                this.HomeName = val;
              }}
              onChange={() => this.validateValue(this.HomeName)}
            />
            <div>
              <Button
                onClick={this.onChangeFavorite(Types.Aliases.HomeAlias)}
                pressed={Favorite === Types.Aliases.HomeAlias}
              >
                {this.nameFavoriteButton(Types.Aliases.HomeAlias)}
              </Button>
            </div>
          </ItemWrapper>
        </ItemsWrapper>
        <ItemsWrapper>
          <ItemWrapper>
            <P>Exchange types</P>
            <ErrorButtonsWrapper
              error={!(SpreadButton || MoneylineButton || TotalPointsButton)}
            >
              <ToggleButton
                onClick={() =>
                  onChangeExchangeType({
                    exchangeType: `${Types.ExchangeType.SPREAD}`,
                  })
                }
              >
                Spread
              </ToggleButton>
              <ToggleButton
                onClick={() =>
                  onChangeExchangeType({
                    exchangeType: `${Types.ExchangeType.MONEYLINE}`,
                  })
                }
              >
                Moneyline
              </ToggleButton>
              <ToggleButton
                onClick={() =>
                  onChangeExchangeType({
                    exchangeType: `${Types.ExchangeType.TOTALPOINTS}`,
                  })
                }
              >
                Total Points
              </ToggleButton>
            </ErrorButtonsWrapper>
          </ItemWrapper>
          <ItemWrapper>
            <P>Exchange values</P>
            <OneLineWrapper>
              {SpreadButton && (
                <LabeledInputWrapper>
                  <LabeledInput
                    name={Types.ExchangeType.SPREAD}
                    label={Types.ExchangeType.SPREAD}
                    value={Spread}
                    type="number"
                    min={-999.9}
                    max={0}
                    step={0.1}
                    validator={/^-\d{1,3}$|^-\d{1,3}\.\d$|^0$/}
                    ref={(val) => {
                      this.Spread = val;
                    }}
                    onChange={() => this.validateValue(this.Spread)}
                  />
                </LabeledInputWrapper>
              )}
              {MoneylineButton && (
                <LabeledInputWrapper>
                  <LabeledInput
                    name={Types.ExchangeType.MONEYLINE}
                    label={Types.ExchangeType.MONEYLINE}
                    value={Moneyline}
                    type="number"
                    min={0.01}
                    max={0.99}
                    step={0.01}
                    validator={/^0$|^0\.\d{1,2}$/}
                    ref={(val) => {
                      this.Moneyline = val;
                    }}
                    onChange={() => this.validateValue(this.Moneyline)}
                  />
                </LabeledInputWrapper>
              )}
              {TotalPointsButton && (
                <LabeledInputWrapper>
                  <LabeledInput
                    name={Types.ExchangeType.TOTALPOINTS}
                    label="Total Points"
                    value={TotalPoints}
                    type="number"
                    min={0}
                    max={999.9}
                    step={0.1}
                    validator={/^\d{1,3}$|^\d{1,3}\.\d$/}
                    ref={(val) => {
                      this.TotalPoints = val;
                    }}
                    onChange={() => this.validateValue(this.TotalPoints)}
                  />
                </LabeledInputWrapper>
              )}
            </OneLineWrapper>
          </ItemWrapper>
        </ItemsWrapper>
        <ItemsWrapper>
          <ItemWrapper>
            <LabeledInput
              type="date"
              name="StartDate"
              label="Start date"
              disabled
              value={generateInputDate(StartDate)}
              ref={(val) => {
                this.StartDate = val;
              }}
            />
          </ItemWrapper>
        </ItemsWrapper>
        <ItemsWrapper>
          <RichTextEditor
            value={this.state.textEditorValue}
            onChange={this.onChangeTextEditor}
          />
        </ItemsWrapper>
        <FlexWrapper>
          <Button
            blue
            onClick={this.onCreateEvent}
            disabled={!this.validateAllValues()}
          >
            Create
          </Button>
          <Button onClick={this.clickCancel}>Cancel</Button>
        </FlexWrapper>
      </div>
    );
  }
}

Sport.propTypes = {
  pushCancel: PropTypes.func.isRequired,
  loadPage: PropTypes.func.isRequired,
  ChangeFavorite: PropTypes.func.isRequired,
  onChangeExchangeType: PropTypes.func.isRequired,
  onValidateData: PropTypes.func.isRequired,
  onSubmitForm: PropTypes.func.isRequired,

  loading: PropTypes.bool.isRequired,

  validationDataObject: PropTypes.shape({
    AwayAlias: PropTypes.string,
    AwayName: PropTypes.string,
    Comment: PropTypes.oneOfType([() => null, PropTypes.string]),
    EventId: PropTypes.number,
    Favorite: PropTypes.number,
    FullName: PropTypes.string,
    HomeAlias: PropTypes.string,
    HomeName: PropTypes.string,
    [Types.ExchangeType.MONEYLINE]: PropTypes.oneOfType([
      () => null,
      PropTypes.string,
    ]),
    [`${Types.ExchangeType.MONEYLINE}Button`]: PropTypes.bool.isRequired,
    [Types.ExchangeType.SPREAD]: PropTypes.oneOfType([
      () => null,
      PropTypes.string,
    ]),
    [`${Types.ExchangeType.SPREAD}Button`]: PropTypes.bool.isRequired,
    [Types.ExchangeType.TOTALPOINTS]: PropTypes.oneOfType([
      () => null,
      PropTypes.string,
    ]),
    [`${Types.ExchangeType.TOTALPOINTS}Button`]: PropTypes.bool.isRequired,
    StartDate: PropTypes.string,
  }),
  lastLocation: PropTypes.oneOfType([
    () => null,
    PropTypes.object,
  ]),
};

export function mapDispatchToProps(dispatch) {
  return {
    pushCancel: route => dispatch(push(route)),
    loadPage: () => dispatch(sportPageLoadData()),
    ChangeFavorite: data => dispatch(pressFavoriteButton(data)),
    onChangeExchangeType: data => dispatch(pressExchangeTypesButton(data)),
    onValidateData: data => dispatch(validateData(data)),
    onSubmitForm: data => dispatch(sendFormData(data)),
  };
}

const mapStateToProps = createStructuredSelector({
  // pageData: makeSelectSportPage(),
  loading: makeSelectLoading(),
  // favorite: makeSelectFavorite(),
  // exchangeObject: makeSelectExchangeButtons(),
  validationDataObject: makeSelectValidationObject(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: SPORT_PAGE, reducer });
const withSaga = injectSaga({ key: SPORT_PAGE, saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
  withLastLocation,
)(Sport);
