import { Map } from 'immutable';

import {
  LOAD_SPORT_PAGE_DATA,
  LOAD_SPORT_PAGE_DATA_SUCCESS,
  LOAD_SPORT_PAGE_DATA_ERROR,
  CHANGE_EXCHANGE_TYPE,
  PUT_VALIDATE_DATA,
} from './constants';
import Types from '../../utils/Types';

// The initial state of the App
const initialState = Map({
  loading: true,
  error: '',
  validateData: Map({
    AwayName: null,
    HomeName: null,
    Favorite: null,
    [`${Types.ExchangeType.SPREAD}Button`]: false,
    [`${Types.ExchangeType.MONEYLINE}Button`]: false,
    [`${Types.ExchangeType.TOTALPOINTS}Button`]: false,
    [Types.ExchangeType.SPREAD]: null,
    [Types.ExchangeType.MONEYLINE]: '',
    [Types.ExchangeType.TOTALPOINTS]: null,
    StartDate: null,
    Comment: null,
    FullName: null,
    EventId: null,
  }),
});

export default function sportPageReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_SPORT_PAGE_DATA:
      return state.set('loading', true);

    case LOAD_SPORT_PAGE_DATA_SUCCESS:
      return state.set('loading', false);

    case LOAD_SPORT_PAGE_DATA_ERROR:
      return state.set('loading', false).set('error', action.error);

    case CHANGE_EXCHANGE_TYPE: {
      const { exchangeType } = action.data;
      return state.updateIn(
        ['validateData', `${exchangeType}Button`],
        value => !value
      );
    }

    case PUT_VALIDATE_DATA: {
      const { key, value } = action.data;
      return state.updateIn(['validateData', key], () => value);
    }

    default:
      return state;
  }
}
