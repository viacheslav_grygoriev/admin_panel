// container's name
export const SPORT_PAGE = 'SPORT_PAGE';

export const LOAD_SPORT_PAGE_DATA = `altbet/${SPORT_PAGE}/LOAD_SPORT_PAGE_DATA`;
export const LOAD_SPORT_PAGE_DATA_SUCCESS = `altbet/${SPORT_PAGE}/LOAD_SPORT_PAGE_DATA_SUCCESS`;
export const LOAD_SPORT_PAGE_DATA_ERROR = `altbet/${SPORT_PAGE}/LOAD_SPORT_PAGE_DATA_ERROR`;

export const PRESS_FAVORITE_BUTTON = `altbet/${SPORT_PAGE}/PRESS_FAVORITE_BUTTON`;

export const PRESS_EXCHANGE_TYPES_BUTTON = `altbet/${SPORT_PAGE}/PRESS_EXCHANGE_TYPES_BUTTON`;
export const CHANGE_EXCHANGE_TYPE = `altbet/${SPORT_PAGE}/CHANGE_EXCHANGE_TYPE`;

export const VALIDATE_DATA = `altbet/${SPORT_PAGE}/VALIDATE_DATA`;
export const PUT_VALIDATE_DATA = `altbet/${SPORT_PAGE}/PUT_VALIDATE_DATA`;

export const SEND_FORM_DATA = `altbet/${SPORT_PAGE}/SEND_FORM_DATA`;
export const SEND_FORM_DATA_START = `altbet/${SPORT_PAGE}/SEND_FORM_DATA_START`;
export const SEND_FORM_DATA_SUCCESS = `altbet/${SPORT_PAGE}/SEND_FORM_DATA_SUCCESS`;
export const SEND_FORM_DATA_ERROR = `altbet/${SPORT_PAGE}/SEND_FORM_DATA_ERROR`;
