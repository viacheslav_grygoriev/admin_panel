/* eslint-disable */
/**
 * Sidebar selectors
 */

import { createSelector, toJS } from 'reselect';
import {SPORT_PAGE} from './constants';

const selectSportPage = (state) => state.get(SPORT_PAGE).toJS();

const makeSelectLoading = () => createSelector(
  [selectSportPage],
  (selectSportPage) => selectSportPage.loading
);

const makeSelectValidationObject = () => createSelector(
  [selectSportPage],
  (selectSportPage) => selectSportPage.validateData
);

export {
  makeSelectLoading,
  makeSelectValidationObject,
};
