export const sportPageData = {
  AwayAlias: 'GS',
  EventId: 12476,
  FullName: 'Warriors @ Cavaliers',
  HomeAlias: 'CLE',
  League: 'NBA',
  Sport: 'Basketball',
  StartDate: '2018-06-07T01:00:00.000Z',
  Status: 'scheduled',
};
