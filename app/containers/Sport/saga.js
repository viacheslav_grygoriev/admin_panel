import {
  // call,
  put,
  all,
  // takeEvery,
  // select,
  takeLatest,
} from 'redux-saga/effects';
// import request from '../../utils/request';
import showError from 'utils/showError';
import {
  LOAD_SPORT_PAGE_DATA,
  PRESS_FAVORITE_BUTTON,
  PRESS_EXCHANGE_TYPES_BUTTON,
  VALIDATE_DATA,
  SEND_FORM_DATA,
} from './constants';
import {
  sportPageLoadDataSuccess,
  sportPageLoadDataError,
  changeExchangeType,
  putValidateData,
  sendFormDataStart,
  sendFormDataSuccess,
  sendFormDataError,
} from './actions';
import { sportPageData } from './staticData';
import Types from '../../utils/Types';

// import request from 'utils/request';
// import { makeSelectSidebar } from './selectors';

/**
 * Github repos request/response handler
 */
export function* getSportPage() {
  // Select username from store
  // const sidebarData = yield select(makeSelectSidebar());

  try {
    // Call our request helper (see 'utils/request')

    // const repos = yield call(fetch, 'http://localhost/AltBet.Admin/Menu/GetAdminMenu');
    // const response = yield call(request,
    //   'http://localhost/AltBet.Admin/Menu/GetAdminMenu', {
    //   method: 'POST',
    //   mode: 'no-cors',
    //   headers: {
    //     'Content-Type': 'application/json',
    //     "Access-Control-Allow-Origin" : "*",
    //     "Access-Control-Allow-Credentials" : true
    //   },
    // });
    // response.json()
    // console.log("response", response);
    // __DEV__&&console.log("after");
    // __DEV__&&console.log("sportPageData", sportPageData);
    // __DEV__&&console.log("sportPageData", sportPageData);
    const {
      EventId,
      FullName,
      StartDate,
      AwayAlias,
      HomeAlias,
    } = sportPageData;
    const teamsNames = FullName.split('@');
    const dataObj = {
      AwayName: teamsNames[0].trim(),
      HomeName: teamsNames[1].trim(),
      Favorite: Types.Aliases.HomeAlias,
      StartDate,
      FullName,
      EventId,
      AwayAlias,
      HomeAlias,
    };
    yield putValidateDataSaga(dataObj);
    yield put(sportPageLoadDataSuccess());
  } catch (error) {
    yield put(sportPageLoadDataError(error));
    showError(error.message);
  }
}

export function* favoriteButtonSaga(action) {
  const favoriteAlias = action.data;
  yield putValidateDataSaga({ Favorite: favoriteAlias });
}

export function* exchangeTypeSaga(action) {
  yield put(changeExchangeType(action.data));
}

export function* validateDataSaga(action) {
  const validationDataObject = action.data;
  yield putValidateDataSaga(validationDataObject);
}
export function* putValidateDataSaga(dataObj) {
  for (const field in dataObj) { // eslint-disable-line no-restricted-syntax
    if ({}.hasOwnProperty.call(dataObj, field)) {
      yield put(putValidateData({ key: field, value: dataObj[field] }));
    }
  }
}

export function* sendFormDataSaga() {
  yield sendFormDataStart();
  try {
    yield sendFormDataSuccess();
  } catch (error) {
    yield sendFormDataError(error);
    showError(error.message);
  }
}
/**
 * Root saga manages watcher lifecycle
 */
export default function* sportPageSaga() {
  yield all([
    yield takeLatest(LOAD_SPORT_PAGE_DATA, getSportPage),
    yield takeLatest(PRESS_FAVORITE_BUTTON, favoriteButtonSaga),
    yield takeLatest(PRESS_EXCHANGE_TYPES_BUTTON, exchangeTypeSaga),
    yield takeLatest(VALIDATE_DATA, validateDataSaga),
    yield takeLatest(SEND_FORM_DATA, sendFormDataSaga),
  ]);
}
