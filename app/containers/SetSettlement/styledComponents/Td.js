import NormalTd from '../../../components/Td/index';
import { INPUT_BORDER } from '../../../utils/index';

export default NormalTd.extend`
  border: 1px solid ${INPUT_BORDER};
  text-align: center;
`;
