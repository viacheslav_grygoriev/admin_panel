import NormalTable from '../../../components/Table/index';
import { INPUT_BORDER } from '../../../utils/index';

export default NormalTable.extend`
  min-width: 240px;
  border-collapse: collapse;
  border: 1px solid ${INPUT_BORDER};
`;
