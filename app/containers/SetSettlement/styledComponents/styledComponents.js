import styled, { css } from 'styled-components';
import Select from 'react-select';

import Button from './Button';
import FlexWrapper from '../../../components/FlexWrapper/index';

export const StyledSelect = styled(Select)`
  width: 200px;
`;

export const StyledButton = styled(Button)`
  height: 37px;
  width: 37px;
  ${props => props.top && css`
    border-radius: 4px 4px 0 0;
    ${props.pressed && css`
      background-color: #31b0d5;
      &:hover {
        background-color: #269abc;
      }
    `}
  `}
  ${props => props.bottom && css`
    border-radius: 0 0 4px 4px;
    ${props.pressed && css`
      background-color: #c9302c;
      &:hover {
        background-color: #ac2925;
      }
    `}
  `}
`;

export const BlockWithTypeOfCancellationContainer = styled(FlexWrapper)`
  margin: 20px 0;
`;
