import NormalH4 from '../../../components/H4/index';

export default NormalH4.extend`
  margin-bottom: 10px;
`;
