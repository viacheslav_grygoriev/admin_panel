import NormalTh from '../../../components/Th/index';
import { INPUT_BORDER } from '../../../utils/index';

export default NormalTh.extend`
  text-align: center;
  border: 1px solid ${INPUT_BORDER};
`;
