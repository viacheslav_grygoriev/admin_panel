import { css } from 'styled-components';
import NormalTr from '../../../components/Tr/index';
import { SIDEBAR_BACKGROUND } from '../../../utils/index';

export default NormalTr.extend`
  ${props => props.underscored && css`
    border-bottom: 1px solid ${SIDEBAR_BACKGROUND};
  `}
`;
