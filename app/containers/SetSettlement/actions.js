import { GET_SETTLEMENT, GET_SETTLEMENT_SUCCESS, GET_SETTLEMENT_ERROR,
  CHANGE_SETTLEMENT_FOR_MARKET, SAVE_CHANGED_SETTLEMENT_FOR_MARKET,
  CLEAR_STATE,
  SEND_SETTLEMENT, SEND_SETTLEMENT_SUCCESS, SEND_SETTLEMENT_ERROR,
  SET_CANCEL,
} from './constants';

export const getSettlement = data => ({
  type: GET_SETTLEMENT,
  data,
});

export const getSettlementSuccess = data => ({
  type: GET_SETTLEMENT_SUCCESS,
  data,
});

export const getSettlementError = error => ({
  type: GET_SETTLEMENT_ERROR,
  error,
});

export const changeSettlementForMarket = data => ({
  type: CHANGE_SETTLEMENT_FOR_MARKET,
  data,
});

export const saveChangedSettlementForMarket = data => ({
  type: SAVE_CHANGED_SETTLEMENT_FOR_MARKET,
  data,
});

export const clearState = () => ({
  type: CLEAR_STATE,
});

export const sendSettlement = data => ({
  type: SEND_SETTLEMENT,
  data,
});

export const sendSettlementSuccess = () => ({
  type: SEND_SETTLEMENT_SUCCESS,
});

export const sendSettlementError = error => ({
  type: SEND_SETTLEMENT_ERROR,
  error,
});

export const setCancel = data => ({
  type: SET_CANCEL,
  data,
});
