import { takeLatest, all, put, select, call, take } from 'redux-saga/effects';
import { push } from 'react-router-redux';

import { GET_SETTLEMENT, CHANGE_SETTLEMENT_FOR_MARKET, SEND_SETTLEMENT, SET_CANCEL } from './constants';
import { EXCHANGE_CONTROLLER_URL, SET_STATUS_SETTLEMENT, GET_EXCHANGE_URL, request, showError } from '../../utils';


import {
  getSettlementSuccess,
  getSettlementError,
  saveChangedSettlementForMarket,
  sendSettlementSuccess,
  sendSettlementError,
} from './actions';
import { requestingTokenSelector } from '../SiteApp/selectors';
import { REFRESH_TOKEN_SUCCESS } from '../SiteApp/constants';
import { refreshToken } from '../SiteApp/actions';

import { settlementDataSelector, exchangeSelector } from './selectors';

function* getSettlementSaga(action) {
  try {
    let response = yield call(request, GET_EXCHANGE_URL, {
      method: 'POST',
      body: JSON.stringify({
        ExchangeName: action.data,
      }),
    });
    // костыль начало
    if (response.status === 401) {
      const requesting = yield select(requestingTokenSelector());
      if (requesting) {
        yield take(REFRESH_TOKEN_SUCCESS);
      }
      yield put(refreshToken());
      yield take(REFRESH_TOKEN_SUCCESS);
      response = yield call(request, GET_EXCHANGE_URL, {
        method: 'POST',
        body: JSON.stringify({
          ExchangeName: action.data,
        }),
      });
    }
    // костыль конец
    yield put(getSettlementSuccess(response.Exchanges));
  } catch (error) {
    yield put(getSettlementError(error));
    yield call(showError, error.message);
  }
}

function* changeSettlementForMarketSaga(action) {
  yield put(saveChangedSettlementForMarket(action.data));
}

function* sendSettlementSaga(action) {
  try {
    const settlementData = yield select(settlementDataSelector());
    const exchange = yield select(exchangeSelector());
    const SymbolResult = [];
    Object.entries(settlementData).forEach(([key, value]) => {
      SymbolResult.push({ SymbolName: key, PositivePercent: value });
    });
    const response = yield call(request, `${EXCHANGE_CONTROLLER_URL}/${SET_STATUS_SETTLEMENT}`, {
      method: 'POST',
      body: JSON.stringify({
        ExchangeName: exchange.Name,
        SymbolResult,
      }),
    });
    // костыль начало
    if (response.status === 401) {
      const requesting = yield select(requestingTokenSelector());
      if (requesting) {
        yield take(REFRESH_TOKEN_SUCCESS);
      }
      yield put(refreshToken());
      yield take(REFRESH_TOKEN_SUCCESS);
      yield call(request, `${EXCHANGE_CONTROLLER_URL}/${SET_STATUS_SETTLEMENT}`, {
        method: 'POST',
        body: JSON.stringify({
          ExchangeName: exchange.Name,
          SymbolResult,
        }),
      });
    }
    // костыль конец

    yield put(sendSettlementSuccess());
    yield put(push(action.data));
  } catch (error) {
    yield put(sendSettlementError(error));
    yield call(showError, error.message);
  }
}

function* setCancelSaga(action) {
  yield put(push(action.data));
}

export default function* setSettlementSaga() {
  yield all([
    yield takeLatest(GET_SETTLEMENT, getSettlementSaga),
    yield takeLatest(CHANGE_SETTLEMENT_FOR_MARKET, changeSettlementForMarketSaga),
    yield takeLatest(SEND_SETTLEMENT, sendSettlementSaga),
    yield takeLatest(SET_CANCEL, setCancelSaga),
  ]);
}
