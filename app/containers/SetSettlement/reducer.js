import { Map } from 'immutable';

import { GET_SETTLEMENT, GET_SETTLEMENT_SUCCESS, GET_SETTLEMENT_ERROR, SAVE_CHANGED_SETTLEMENT_FOR_MARKET,
  CLEAR_STATE,
} from './constants';

const initialState = Map({
  loading: true,
  exchange: null,
  error: null,
  settlementData: Map(),
});

export default function setSettlementReducer(state = initialState, action) {
  switch (action.type) {
    case GET_SETTLEMENT: {
      return state
        .set('loading', true);
    }

    case GET_SETTLEMENT_SUCCESS: {
      return state
        .set('exchange', action.data)
        .set('loading', false);
    }

    case GET_SETTLEMENT_ERROR: {
      return state
        .set('error', action.error.message)
        .set('loading', true);
    }

    case SAVE_CHANGED_SETTLEMENT_FOR_MARKET: {
      const { SymbolName, PositivePercent } = action.data;
      return state
        .setIn(['settlementData', SymbolName], PositivePercent);
    }

    case CLEAR_STATE: {
      return initialState;
    }

    default: {
      return state;
    }
  }
}
