/* eslint-disable */

import { createSelector } from 'reselect';
import { SETTLEMENT } from './constants';

const selectSetSettlement = state => state.get(SETTLEMENT).toJS();

const exchangeSelector = () => createSelector(
  [selectSetSettlement],
  (selectSetSettlement) => selectSetSettlement.exchange
);
const loadingSelector = () => createSelector(
  [selectSetSettlement],
  (selectSetSettlement) => selectSetSettlement.loading
);
const settlementDataSelector = () => createSelector(
  [selectSetSettlement],
  (selectSetSettlement) => selectSetSettlement.settlementData
);

export {
  exchangeSelector,
  loadingSelector,
  settlementDataSelector,
}
