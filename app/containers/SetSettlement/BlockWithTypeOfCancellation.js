import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { namesComparator } from '../../utils';
import FlexWrapper from '../../components/FlexWrapper';
import P from './styledComponents/P';
import { StyledSelect, StyledButton, BlockWithTypeOfCancellationContainer } from './styledComponents/styledComponents';

class BlockWithTypeOfCancellation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pressed: 'top',
      name: props.name, // eslint-disable-line react/no-unused-state
      value: this.defaultValue(),
    };
  }

  defaultValue = () => { // eslint-disable-line consistent-return
    const { expectedValue, actualValue, options } = this.props;
    if (Number(expectedValue) > Number(actualValue)) return options[2];
    if (Number(expectedValue) < Number(actualValue)) return options[0];
    if (Number(expectedValue) === Number(actualValue)) return options[1];
  };

  buttonClickHandler = value => () => {
    this.setState((state) => {
      if (namesComparator(value, 'bottom')) {
        return { pressed: value, value: { ...state.value, value: -1 } };
      }
      return { pressed: value };
    });
  };

  selectChangeHandler = (obj) => {
    this.setState({ value: obj, pressed: 'top' });
  };

  onSave = () => {
    const { name } = this.props;
    const { value: { value } } = this.state;
    return { SymbolName: name, PositivePercent: value };
  };

  render() {
    const { options } = this.props;
    const { pressed, value } = this.state;
    return (
      <BlockWithTypeOfCancellationContainer>
        <FlexWrapper flexDirection="column">
          <FlexWrapper alignItems="center">
            <StyledButton
              top
              pressed={namesComparator(pressed, 'top')}
              onClick={this.buttonClickHandler('top')}
            />
            <FlexWrapper alignItems="center">
              <P>Settlement</P>
              <StyledSelect
                options={options}
                value={value}
                onChange={this.selectChangeHandler}
              />
            </FlexWrapper>
          </FlexWrapper>
          <FlexWrapper alignItems="center">
            <StyledButton
              bottom
              pressed={namesComparator(pressed, 'bottom')}
              onClick={this.buttonClickHandler('bottom')}
            />
            <FlexWrapper flexDirection="column">
              <P>Emergency finish</P>
            </FlexWrapper>
          </FlexWrapper>
        </FlexWrapper>
      </BlockWithTypeOfCancellationContainer>
    );
  }
}

BlockWithTypeOfCancellation.propTypes = {
  expectedValue: PropTypes.oneOfType([() => null, PropTypes.number]),
  actualValue: PropTypes.number,
  options: PropTypes.array.isRequired,
  name: PropTypes.string.isRequired,
};

export default BlockWithTypeOfCancellation;
