import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { withLastLocation } from 'react-router-last-location';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import Loading from 'components/Loading';
import FlexWrapper from 'components/FlexWrapper';
import Toggle from 'components/Toggle';
import Modal from 'components/ModalConfirmation';
import {
  injectReducer,
  injectSaga,
  exchangeNameFinder,
  inTypesEntitiesNamesFinder,
  lastLocationUtil,
  Types,
} from '../../utils';

import { SETTLEMENT } from './constants';

import reducer from './reducer';
import saga from './saga';
import { getSettlement, saveChangedSettlementForMarket, clearState, sendSettlement, setCancel } from './actions';

import { exchangeSelector, loadingSelector } from './selectors';

import H1 from './styledComponents/H1';
import H4 from './styledComponents/H4';
import P from './styledComponents/P';
import Table from './styledComponents/Table';
import Th from './styledComponents/Th';
import Tr from './styledComponents/Tr';
import Td from './styledComponents/Td';
import Button from './styledComponents/Button';
import BlockWithTypeOfCancellation from './BlockWithTypeOfCancellation';
import namesComparator from '../../utils/namesComparator';

const { OptionExchange: { TotalPoints, Moneyline, Spread } } = Types;

class SetSettlement extends Component {
  refsArray = [];

  componentDidMount() {
    const { match: { params: { exchangeName } }, onGetSettlement } = this.props;
    onGetSettlement(exchangeName);
  }

  componentWillUnmount() {
    const { clearComponentsState } = this.props;
    clearComponentsState();
  }

  statusEventName = statusEventCode => inTypesEntitiesNamesFinder('StatusEvent', statusEventCode);
  statusExchangeName = statusExchangeCode => inTypesEntitiesNamesFinder('StatusExchange', statusExchangeCode);

  options = (market) => {
    if (namesComparator(TotalPoints, market)) {
      return [
        { value: 0, label: 'Over' },
        { value: 50, label: 'Draft' },
        { value: 100, label: 'Under' },
      ];
    }
    return [
      { value: 0, label: 'Lost' },
      { value: 50, label: 'Draft' },
      { value: 100, label: 'Win' },
    ];
  };

  clickOnSave = () => {
    const { saveChangedSettlement } = this.props;
    this.refsArray.forEach((ref) => {
      saveChangedSettlement(ref.onSave());
    });
  };

  callbackForLastLocation = lastLocationUtil(this.props, '/sport/Suspended/1');

  clickCancel = () => {
    const { onCancel } = this.props;
    this.callbackForLastLocation(onCancel);
  };

  favorite = (expectedValue, homeName, awayName) => {
    if (expectedValue > 0) return homeName;
    if (expectedValue < 0) return awayName;
    return 'PK';
  };

  render() {
    const { exchange, loading, approveSettlement } = this.props;
    if (loading) return <Loading/>;
    const { FullName, StatusEvent, Symbols, AwayName, HomeName } = exchange;
    return (
      <Fragment>
        <H1>Settlement for exchange {FullName}</H1>
        <H4>Status of Exchange: {this.statusEventName(StatusEvent)}</H4>
        {Symbols.map((market, key) => {
          const { Name, OptionExchange, Status, ExpectedValue, ActualValue } = market.Symbol;
          const isOptionExchange = OptionExchangeValue => namesComparator(OptionExchange, OptionExchangeValue);
          const exchangeName = exchangeNameFinder(OptionExchange);
          const statusExchange = this.statusExchangeName(Status);
          return (
            <FlexWrapper key={Name} flexDirection="column" alignItems="center">
              <H4>{exchangeName}</H4>
              <P>Status of {exchangeName} Market: <b>{statusExchange}</b></P>
              {isOptionExchange(Spread) && <P>Favorite: <b>{this.favorite(ExpectedValue, HomeName, AwayName)}</b></P>}
              {isOptionExchange(Moneyline) ?
                <P>Away Team: <b>{AwayName}</b></P>
                :
                <Table>
                  <thead>
                    <Tr>
                      <Th>Expected</Th>
                      <Th>Actual</Th>
                    </Tr>
                  </thead>
                  <tbody>
                    <Tr>
                      <Td>{isOptionExchange(Spread) && ExpectedValue === 0 ? 'PK' : ExpectedValue}</Td>
                      <Td>{isOptionExchange(Spread) && ActualValue === 0 ? 'PK' : ActualValue}</Td>
                    </Tr>
                  </tbody>
                </Table>

              }
              <BlockWithTypeOfCancellation
                options={this.options(OptionExchange)}
                expectedValue={ExpectedValue}
                actualValue={ActualValue}
                name={Name}
                ref={(node) => {
                  this.refsArray[key] = node;
                }}
              />
            </FlexWrapper>
          );
        })}
        <FlexWrapper>
          <Toggle>
            {({ on, toggle }) => (
              <Fragment>
                <Button
                  onClick={() => {
                    this.clickOnSave();
                    toggle();
                  }}
                >Save</Button>
                <Modal
                  on={on}
                  onCancel={toggle}
                  header={`Set exchange "${FullName}" to Settlement?`}
                  onOk={() => {
                    this.callbackForLastLocation(approveSettlement);
                  }}
                >
                  All the markets will be settlemented
                </Modal>
              </Fragment>
            )}
          </Toggle>
          <Button onClick={this.clickCancel}>Cancel</Button>
        </FlexWrapper>
      </Fragment>
    );
  }
}

SetSettlement.propTypes = {
  onGetSettlement: PropTypes.func,
  exchange: PropTypes.object,
  match: PropTypes.object,
  loading: PropTypes.bool,
  saveChangedSettlement: PropTypes.func,
  clearComponentsState: PropTypes.func,
  approveSettlement: PropTypes.func,
  onCancel: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  exchange: exchangeSelector(),
  loading: loadingSelector(),
});

const mapDispatchToProps = dispatch => ({
  onGetSettlement: data => dispatch(getSettlement(data)),
  saveChangedSettlement: data => dispatch(saveChangedSettlementForMarket(data)),
  clearComponentsState: () => dispatch(clearState()),
  approveSettlement: data => dispatch(sendSettlement(data)),
  onCancel: data => dispatch(setCancel(data)),
});

const withReducer = injectReducer({ key: SETTLEMENT, reducer });
const withSaga = injectSaga({ key: SETTLEMENT, saga });
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(withReducer, withSaga, withConnect, withLastLocation)(SetSettlement);
