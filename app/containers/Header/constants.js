export const HEADER = 'HEADER';

export const SHOW_USERPOPUP = `altbet/${HEADER}/SHOW_USERPOPUP`;
export const HIDE_USERPOPUP = `altbet/${HEADER}/HIDE_USERPOPUP`;

export const LOGOUT = `altbet/${HEADER}/LOGOUT`;
