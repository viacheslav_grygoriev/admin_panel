import { Map } from 'immutable';

import { SHOW_USERPOPUP, HIDE_USERPOPUP } from './constants';

// The initial state of the App
const initialState = Map({
  showPopup: false,
});

function headerReducer(state = initialState, action) {
  switch (action.type) {
    case SHOW_USERPOPUP:
      return state.set('showPopup', true);
    case HIDE_USERPOPUP:
      return state.set('showPopup', false);

    default:
      return state;
  }
}

export default headerReducer;
