import React from 'react';
import { shallow } from 'enzyme';

import Header from '../index';
import Img from '../styledComponents/Img';
import StyledNavBar from '../NavBar';
import NavLink from '../NavLink';
import HeaderLogo from '../HeaderLogo';

describe('<Header />', () => {
  let headerComponent;
  beforeEach(() => {
    headerComponent = shallow(<Header />);
  });

  it('should render a <header> tag with the "header" class', () => {
    expect(headerComponent).toMatchSnapshot();
  });

  it('should render HeaderLogo component', () => {
    expect(headerComponent.find(HeaderLogo).length).toEqual(1);
  });

  it('should render Img component', () => {
    expect(headerComponent.find(Img).length).toEqual(1);
  });

  it('should render StyledNavBar component', () => {
    expect(headerComponent.find(StyledNavBar).length).toEqual(1);
  });

  it('should render NavLink component(s)', () => {
    expect(headerComponent.find(NavLink).exists()).toBeTruthy();
  });
});
