/* eslint-disable */

import { createSelector } from 'reselect';
// import {toJS} from 'immutable';
import {HEADER} from './constants';

const selectHeader = (state) => state.get(HEADER).toJS();

const userPopupSelector = () => createSelector(
  [selectHeader],
  (selectHeader) => selectHeader.showPopup
);

export {
  userPopupSelector,
}
