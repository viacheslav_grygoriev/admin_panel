import styled from 'styled-components';

import { HEADER_BLACK, WHITE } from 'utils';

export const PopupWrapper = styled.div`
  position: absolute;
  top: 100%;
  right: 1px;
  display: flex;
  flex-direction: column;
  background-color: ${HEADER_BLACK};
  height: 250px;
  width: 300px;
  color: ${WHITE};
  border: 1px solid ${HEADER_BLACK};
  border-top-color: ${WHITE};
  z-index: 1;
`;

export const PopupBody = styled.div`
  flex-basis: 75%;
  background-color: ${HEADER_BLACK};
  padding: 5px;
`;

export const PopupFooter = styled.div`
  flex-basis: 25%;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  padding: 0 10px;
  background-color: white;
`;

export const StyledHeader = styled.header`
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 50px;
  background-color: ${HEADER_BLACK};
`;

export const UserButtonWrapper = styled.div`
  position: relative;
  height: 100%;
`;
