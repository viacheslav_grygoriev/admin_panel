import styled from 'styled-components';
import NormalImg from '../../../components/Img/index';

export default styled(NormalImg)`
  width: auto;
  height: 100%;
`;
