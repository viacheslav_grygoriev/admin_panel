import { WHITE_LINK, WHITE_LINK_HOVER, BLACK_LINK, BLACK_LINK_HOVER } from 'utils';
import NormalA from '../../../components/A/index';

export default NormalA.extend`
  margin: 0 10px;
  height: 100%;
  display: flex;
  align-items: center;
  color: ${props =>
    props.to.state.black ? WHITE_LINK : BLACK_LINK};
  &:hover {
    color: ${props =>
    props.to.state.black
      ? WHITE_LINK_HOVER
      : BLACK_LINK_HOVER};
  }
  &:last-child {
    margin-right: 0;
  }
`;
