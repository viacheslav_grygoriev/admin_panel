import { all, takeLatest } from 'redux-saga/effects';
import { LOGOUT } from './constants';

export function* logoutSaga() {
  window.localStorage.removeItem('adminToken');
}

export default function* header() {
  yield all([
    yield takeLatest(LOGOUT, logoutSaga),
  ]);
}
