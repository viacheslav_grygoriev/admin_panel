import styled from 'styled-components';

export default styled.nav`
  height: 100%;
  display: flex;
  align-items: center;
`;
