import {
  SHOW_USERPOPUP, HIDE_USERPOPUP,
  LOGOUT,
} from './constants';

export const showUserPopup = () => (
  {
    type: SHOW_USERPOPUP,
  }
);

export const hideUserPopup = () => (
  {
    type: HIDE_USERPOPUP,
  }
);

export const logout = () => (
  {
    type: LOGOUT,
  }
);

