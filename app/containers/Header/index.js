import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { injectReducer, injectSaga, adminTokenData } from '../../utils';

import NavLink from './NavLink';
import Img from './styledComponents/Img';
import StyledNavBar from './NavBar';
import HeaderLogo from './HeaderLogo';
import image from '../../images/icon-128x128.png';
import Button from './styledComponents/Button';
import UserPopup from './UserPopup';
import reducer from './reducer';
import saga from './saga';
import { HEADER } from './constants';
import { showUserPopup } from './actions';
import { userPopupSelector } from './selectors';
import { StyledHeader, UserButtonWrapper } from './styledComponents/styledComponents';

const Header = ({ showPopup, userPopup }) => (
  <StyledHeader className="header">
    <HeaderLogo to={{ pathname: '/', state: { theme: { white: true } } }}>
      <Img
        className="header-logo"
        src={image}
        alt="react-boilerplate - Logo"
      />
    </HeaderLogo>
    <StyledNavBar>
      <NavLink to={{ pathname: '/user/addadmin', state: { black: true } }}>
        Register New Admin
      </NavLink>
      <NavLink to={{ pathname: '/user/adduser', state: { black: true } }}>
        Register New User
      </NavLink>
      <UserButtonWrapper>
        <Button black link onClick={showPopup}>
          {adminTokenData().userName}
        </Button>
        {userPopup && <UserPopup/>}
      </UserButtonWrapper>
    </StyledNavBar>
  </StyledHeader>
);

Header.propTypes = {
  showPopup: PropTypes.func.isRequired,
  userPopup: PropTypes.bool.isRequired,
};

const mapStateToProps = createStructuredSelector({
  userPopup: userPopupSelector(),
});

const mapDispatchToProps = dispatch => ({
  showPopup: () => dispatch(showUserPopup()),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: HEADER, reducer });
const withSaga = injectSaga({ key: HEADER, saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(Header);
