import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { PopupBody, PopupFooter, PopupWrapper } from './styledComponents/styledComponents';
import Button from './styledComponents/Button';
import { hideUserPopup } from './actions';
import { adminTokenData } from '../../utils';
import { removeToken } from '../../containers/SiteApp/actions';
import P from './styledComponents/P';

class UserPopup extends React.Component {
  constructor(props) {
    super(props);

    this.handleClickOutside = this.handleClickOutside.bind(this);
    this.onLogoutHandler = this.onLogoutHandler.bind(this);
  }

  componentDidMount() {
    document.addEventListener('click', this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleClickOutside);
  }

  onLogoutHandler() {
    const { onLogout, hidePopup } = this.props;
    hidePopup();
    onLogout();
  }

  handleClickOutside(event) {
    const { hidePopup } = this.props;
    const watchNode = document.getElementsByClassName('user-popup')[0];
    if (this.wrapperRef && !watchNode.contains(event.target)) {
      hidePopup();
    }
  }

  render() {
    const { userName, userRole } = adminTokenData();
    return (
      <PopupWrapper
        className="user-popup"
        ref={(element) => {
          this.wrapperRef = element;
        }}
      >
        <PopupBody>
          <P>{userName}</P>
          <P>{userRole}</P>
        </PopupBody>
        <PopupFooter>
          <Button onClick={this.onLogoutHandler}>Logout</Button>
        </PopupFooter>
      </PopupWrapper>
    );
  }
}

UserPopup.propTypes = {
  hidePopup: PropTypes.func.isRequired,
  onLogout: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({

});

const mapDispatchToProps = dispatch => ({
  hidePopup: () => dispatch(hideUserPopup()),
  onLogout: () => dispatch(removeToken()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserPopup);
