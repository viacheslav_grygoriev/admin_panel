import { GET_FEED_SUCCESS, GET_FEED_ERROR, SORT, PUT_SORT, PAGE, PUT_PAGE } from './constants';

export function getFeedSucces(data) {
  return {
    type: GET_FEED_SUCCESS,
    data,
  };
}

export function getFeedError(error) {
  return {
    type: GET_FEED_ERROR,
    error,
  };
}

export const sortRequest = data => ({
  type: SORT,
  data,
});

export const putSort = data => ({
  type: PUT_SORT,
  data,
});

export const changePage = data => ({
  type: PAGE,
  data,
});

export const putPage = data => ({
  type: PUT_PAGE,
  data,
});
