import { Map } from 'immutable';

import { GET_FEED, GET_FEED_SUCCESS, GET_FEED_ERROR, PUT_SORT, PUT_PAGE } from './constants';

const initialState = Map({
  loading: true,
  error: '',
  FeedExchanges: null,
  SortBy: 'StartDate',
  OrderBy: 'Asc',
  PageInfo: {},
});

export default function feedReducer(state = initialState, action) {
  switch (action.type) {
    case GET_FEED:
      return state.set('loading', true);

    case GET_FEED_SUCCESS: {
      const {
        FeedExchanges, SortBy, OrderBy, PageInfo,
      } = action.data;
      return state
        .set('FeedExchanges', FeedExchanges)
        .set('SortBy', SortBy)
        .set('OrderBy', OrderBy)
        .set('PageInfo', PageInfo)
        .set('loading', false);
    }

    case GET_FEED_ERROR:
      return state.set('loading', true).set('error', action.error.message);

    case PUT_SORT:
      return state
        .set('SortBy', action.data.SortBy)
        .set('OrderBy', action.data.OrderBy);

    // case PAGE:
    //   return state
    //     .set('loading', true)

    case PUT_PAGE:
      return state
        .set('PageInfo', action.data)
        .set('loading', true);

    default:
      return state;
  }
}
