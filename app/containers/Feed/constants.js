export const FEED = 'FEED';

export const GET_FEED = `altbet/${FEED}/GET_FEED`;
export const GET_FEED_SUCCESS = `altbet/${FEED}/GET_FEED_SUCCESS`;
export const GET_FEED_ERROR = `altbet/${FEED}/GET_FEED_ERROR`;

export const SORT = `altbet/${FEED}/SORT`;
export const PUT_SORT = `altbet/${FEED}/PUT_SORT`;

export const PAGE = `altbet/${FEED}/PAGE`;
export const PUT_PAGE = `altbet/${FEED}/PUT_PAGE`;

