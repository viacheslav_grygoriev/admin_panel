import { call, put, all, takeLatest, select, take } from 'redux-saga/effects';
import { request, showError, mapOfExchanges, GET_FEED_EVENTS_URL, ascDescSwitcher } from '../../utils';
import { SORT, PAGE } from './constants';
import { getFeedSucces, getFeedError, putSort, putPage } from './actions';
import { sortAndPageSelector } from './selectors';
import { refreshToken } from '../SiteApp/actions';
import { REFRESH_TOKEN_SUCCESS } from '../SiteApp/constants';
import { requestingTokenSelector } from '../SiteApp/selectors';

export function* getFeed() {
  const sortAndPage = yield select(sortAndPageSelector());
  const { PageInfo: { CurrentPage } } = sortAndPage;
  try {
    const response = yield call(request, GET_FEED_EVENTS_URL, {
      method: 'POST',
      body: JSON.stringify({
        SortBy: sortAndPage.SortBy,
        OrderBy: sortAndPage.OrderBy,
        page: CurrentPage,
      }),
    });
    // костыль начало
    if (response.status === 401) {
      const requesting = yield select(requestingTokenSelector());
      if (requesting) {
        yield take(REFRESH_TOKEN_SUCCESS);
      }
      yield put(refreshToken());
      yield take(REFRESH_TOKEN_SUCCESS);
      yield getFeed();
      return;
    }
    // костыль конец
    const {
      FeedEvents, SortBy, OrderBy, PageInfo,
    } = response;
    const FeedEventsMap = yield call(mapOfExchanges, FeedEvents, {
      id: 'EventId',
      fields: [
        'EventId',
        'FullName',
        'StartDate',
        'League',
        'Sport',
        'Status',
      ],
    });
    yield put(getFeedSucces({
      FeedExchanges: FeedEventsMap, SortBy, OrderBy, PageInfo,
    }));
  } catch (error) {
    yield put(getFeedError(error));
    showError(error.message);
  }
}

export function* sortSaga(action) {
  const sortAndPage = yield select(sortAndPageSelector());
  yield put(putSort({
    SortBy: action.data,
    OrderBy: ascDescSwitcher(sortAndPage.OrderBy),
  }));
  yield getFeed();
}

export function* pageSaga(action) {
  yield put(putPage({
    CurrentPage: action.data,
  }));
  yield getFeed();
}

export default function* feedSaga() {
  yield all([
    // yield takeLatest(GET_FEED, getFeed),
    yield takeLatest(SORT, sortSaga),
    yield takeLatest(PAGE, pageSaga),
  ]);
}
