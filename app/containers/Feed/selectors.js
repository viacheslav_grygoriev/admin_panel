/* eslint-disable */

import { createSelector } from 'reselect';
import {FEED} from './constants';

const selectFeed = (state) => state.get(FEED).toJS();

const feedSelector = () => createSelector(
  [selectFeed],
  (selectFeed) => selectFeed.FeedExchanges
);

const loadingSelector = () => createSelector(
  [selectFeed],
  (selectFeed) => selectFeed.loading
);

const sortAndPageSelector = () => createSelector(
  [selectFeed],
  (selectFeed) => {
    return {
      SortBy : selectFeed.SortBy,
      OrderBy : selectFeed.OrderBy,
      PageInfo: selectFeed.PageInfo,
    }
  }
);

export {
  feedSelector,
  loadingSelector,
  sortAndPageSelector,
}
