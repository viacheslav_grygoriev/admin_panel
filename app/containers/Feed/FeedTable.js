import React from 'react';
import PropTypes from 'prop-types';
import { namesComparator } from 'utils';

import Table from './styledComponents/Table';
import Tr from './styledComponents/Tr';
import Th from './styledComponents/Th';
import Span from './styledComponents/Span';

export default function FeedTable(props) {
  const {
    children,
    orderBy,
    sortBy,
    sortClick,
  } = props;
  const names = {
    FullName: 'FullName',
    StartDate: 'StartDate',
    Sport: 'Sport',
    League: 'League',
  };
  return (
    <Table>
      <thead>
        <Tr bigTableHeading>
          <Th onClick={sortClick(names.FullName)}>
            <Span sortable orderBy={orderBy} active={namesComparator(names.FullName, sortBy)} >
              Full name
            </Span>
          </Th>
          <Th onClick={sortClick(names.Sport)}>
            <Span sortable orderBy={orderBy} active={namesComparator(names.Sport, sortBy)} >
              Sport
            </Span>
          </Th>
          <Th onClick={sortClick(names.League)}>
            <Span sortable orderBy={orderBy} active={namesComparator(names.League, sortBy)} >
              League
            </Span>
          </Th>
          <Th>Status</Th>
          <Th onClick={sortClick(names.StartDate)}>
            <Span sortable orderBy={orderBy} active={namesComparator(names.StartDate, sortBy)} >
              Start date
            </Span>
          </Th>
          <Th>Create exchange</Th>
        </Tr>
      </thead>
      <tbody>{children}</tbody>
    </Table>
  );
}

FeedTable.propTypes = {
  children: PropTypes.array,
  orderBy: PropTypes.string.isRequired,
  sortBy: PropTypes.string.isRequired,
  sortClick: PropTypes.func,
};
