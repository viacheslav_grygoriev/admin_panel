import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { push } from 'react-router-redux';

import { injectReducer, injectSaga, formatDate } from 'utils';

import Loading from 'components/Loading';
import Paginate from 'components/Paginate';
import { FEED } from './constants';
import reducer from './reducer';
import saga from './saga';
import { sortRequest, changePage } from './actions';
import { feedSelector, loadingSelector, sortAndPageSelector } from './selectors';
import { Wrapper } from './styledComponents/styledComponents';
import H1 from './styledComponents/H1';
import FeedTable from './FeedTable';
import Tr from './styledComponents/Tr';
import Td from './styledComponents/Td';
import Button from './styledComponents/Button';

class FeedPage extends React.Component {
  state = {
    currentPage: null,
  };

  componentDidMount() {
    const {
      onChangePage,
      sortAndPage,
      match: { params: { page } },
    } = this.props;
    const { CurrentPage } = sortAndPage;
    const curPage = Number(CurrentPage || page);
    this.setState({ currentPage: curPage }); // eslint-disable-line react/no-did-mount-set-state

    onChangePage(curPage);
  }

  getSnapshotBeforeUpdate(prevProps) {
    if (prevProps.match.params.page !== this.props.match.params.page) {
      return this.props.match.params.page;
    }
    return null;
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const {
      onChangePage,
    } = this.props;

    if (snapshot) {
      onChangePage(Number(snapshot));
      this.setState({ currentPage: Number(snapshot) }); // eslint-disable-line react/no-did-update-set-state
    }
  }

  onCreateMarketClick = data => () => {
    const { pushToCreateMarketPage } = this.props;
    pushToCreateMarketPage(data);
  };
  sortClick = columnName => () => {
    const { sortColumn } = this.props;
    sortColumn(columnName);
  };
  paginationClick = ({ selected }) => {
    const { pushToPage } = this.props;
    const currentPageNumber = selected + 1;
    pushToPage(currentPageNumber);
  };

  render() {
    const { loading } = this.props;
    if (loading) return <Loading/>;
    const { mapOfExchanges, sortAndPage } = this.props;
    const { OrderBy, SortBy, PageInfo } = sortAndPage;
    const { currentPage } = this.state;
    return (
      <Wrapper>
        <H1>List of events</H1>
        <FeedTable orderBy={OrderBy} sortBy={SortBy} sortClick={this.sortClick}>
          {[...mapOfExchanges.values()].map((event) => {
            const { FullName, Sport, League, Status, StartDate, EventId } = event;
            return (
              <Tr bigTable key={EventId}>
                <Td>{FullName}</Td>
                <Td>{Sport}</Td>
                <Td>{League}</Td>
                <Td>{Status}</Td>
                <Td>{formatDate(StartDate, { year: 'numeric' })}</Td>
                <Td>
                  <Button onClick={this.onCreateMarketClick({ EventId, typeEvent: 'Fantasy' })}>Fantasy</Button>
                  <Button onClick={this.onCreateMarketClick({ EventId, typeEvent: 'Sport' })}>Sport</Button>
                </Td>
              </Tr>
            );
          })}
        </FeedTable>
        <Paginate pageInfo={PageInfo} page={currentPage} handlePageClick={this.paginationClick}/>
      </Wrapper>
    );
  }
}

FeedPage.propTypes = {
  mapOfExchanges: PropTypes.instanceOf(Map),
  pushToCreateMarketPage: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  sortAndPage: PropTypes.shape({
    SortBy: PropTypes.string.isRequired,
    OrderBy: PropTypes.string.isRequired,
    PageInfo: PropTypes.object.isRequired,
  }).isRequired,
  match: PropTypes.object.isRequired,
  onChangePage: PropTypes.func.isRequired,
  sortColumn: PropTypes.func.isRequired,
  pushToPage: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  mapOfExchanges: feedSelector(),
  loading: loadingSelector(),
  sortAndPage: sortAndPageSelector(),
});

function mapDispatchToProps(dispatch) {
  return {
    pushToCreateMarketPage: ({ EventId, typeEvent }) => {
      dispatch(push(`/feed/create/${typeEvent}/${EventId}`));
    },
    sortColumn: columnName => dispatch(sortRequest(columnName)),
    onChangePage: pageNumber => dispatch(changePage(pageNumber)),
    pushToPage: pageNumber => dispatch(push(`/feed/page/${pageNumber}`)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
const withReducer = injectReducer({ key: FEED, reducer });
const withSaga = injectSaga({ key: FEED, saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(FeedPage);
