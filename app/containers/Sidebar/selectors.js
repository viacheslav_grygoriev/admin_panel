/* eslint-disable */
/**
 * Sidebar selectors
 */

import { createSelector, toJS } from 'reselect';
import {SIDEBAR} from './constants';

const selectSidebar = (state) => state.get(SIDEBAR).toJS();

const makeSelectSidebar = () => createSelector(
  [selectSidebar],
  (selectSidebar) => selectSidebar.Menu
);

const activeMenuSelector = () => createSelector(
  [selectSidebar],
  (selectSidebar) => selectSidebar.active
);

const loadingSelector = () => createSelector(
  [selectSidebar],
  (selectSidebar) => selectSidebar.loading
);

export {
  makeSelectSidebar,
  activeMenuSelector,
  loadingSelector,
};
