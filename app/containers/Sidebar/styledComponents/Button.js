import NormalButton from '../../../components/Button/index';

export default NormalButton.extend`
  border: none;

  &.active {
    background-color: aqua;
  }
  &:not(.show) ~ * {
    display: none;
  }
  &.active ~ * {
    display: table;
  }
`;
