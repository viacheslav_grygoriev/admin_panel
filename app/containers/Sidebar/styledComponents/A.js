import NormalA from '../../../components/A/index';

export default NormalA.extend`
  margin: 0 0 0 10px;
  &.active {
    background-color: aqua;
  }
  &:not(.show) ~ * {
    display: none;
  }
  &.active ~ * {
    display: table;
  }
`;
