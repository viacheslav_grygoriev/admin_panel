import NormalLi from '../../../components/ListItem/index';

export default NormalLi.extend`
  margin: 0 0 0 10px;
`;
