import React, { Component, Fragment } from 'react';
import styled from 'styled-components';

import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { push } from 'react-router-redux';
import { injectReducer, injectSaga, clicksHandler, SIDEBAR_BACKGROUND } from 'utils';

import Link from './styledComponents/NavLink';
import Ul from './styledComponents/Ul';
import Li from './styledComponents/Li';
import Button from './styledComponents/Button';
import Loading from '../../components/Loading';
import ModalConfirmation from '../../components/ModalConfirmation';
import Toggle from '../../components/Toggle';


import reducer from './reducer';
import saga from './saga';

import { sidebarLoad, setActiveMenu, resetActiveMenu, categorySendDelete } from './actions';
import { activeMenuSelector, makeSelectSidebar, loadingSelector } from './selectors';
import { SIDEBAR } from './constants';

const StyledSidebar = styled.aside`
  display: flex;
  flex-direction: column;
  width: 250px;
  background-color: ${SIDEBAR_BACKGROUND};
  padding: 20px 0;
  min-height: calc(100vh - 50px);
`;

class Sidebar extends Component {
  state = {};
  componentDidMount() {
    const { onGetSidebar, pathname, onSetActiveMenu } = this.props;
    onGetSidebar();
    const itemsForActiveMenu = pathname.split('/').slice(1, -2);
    itemsForActiveMenu.forEach(item => onSetActiveMenu(item));
  }
  // getSnapshotBeforeUpdate(prevProps){
  //   const { pathname } = prevProps;
  //   const asdf = pathname.match(/\/(.+)\/.+\//i);
  //   __DEV__&&console.log("asdf", asdf[1]);
  //   if (this.props.pathname !== prevProps.pathname) {
  //     return {
  //       pathname: prevProps.pathname,
  //     };
  //   }
  //   return null;
  // }
  // componentDidUpdate(prevProps, prevState, snapshot) {
  //   const { redirectLink, pathname } = this.props;
  //   if (snapshot) {
  //     // __DEV__&&console.log("snapshot", snapshot);
  //     // redirectLink(pathname + snapshot.pathname)
  //   }
  // }
  setActive = (activeMenu) => {
    const { onSetActiveMenu } = this.props;
    onSetActiveMenu(activeMenu);
  };
  resetActive = (activeMenu) => {
    const { onResetActiveMenu } = this.props;
    onResetActiveMenu(activeMenu);
  };
  nesting = (item) => {
    const { activeMenu, redirectLink, onSendDeleteId } = this.props;
    const {
      Name,
      UrlChain,
      SubMenu,
      Id,
    } = item;
    const splittedUrlChain = UrlChain.split('/');
    const Url = splittedUrlChain[splittedUrlChain.length - 1];
    if (SubMenu.length) {
      return (
        <Ul>
          <Button
            className={activeMenu.includes(Url) && 'active'}
            onClick={clicksHandler(
              () => this.setActive(Url),
              () => { redirectLink(`${UrlChain}/Approved/1`); this.setActive(Url); }
            )}
          >
            {Name}
          </Button>
          <div>
            <Link className="show" to={{ pathname: `/category/add/${Url}`, state: { Name, Id, UrlChain } }}>Add</Link> {/* eslint-disable-line jsx-a11y/anchor-is-valid */}
            <Link className="show" to={{ pathname: `/category/edit/${Url}`, state: { Name, Id, UrlChain } }} >Edit</Link> {/* eslint-disable-line jsx-a11y/anchor-is-valid */}
          </div>
          { activeMenu.includes(Url) &&
            SubMenu.map(subitem => (
              <Li key={subitem.Id}>{this.nesting(subitem)}</Li>
            ))}
        </Ul>
      );
    }
    return (
      <React.Fragment>
        <Button
          onClick={clicksHandler(
            () => this.setActive(Url),
            () => { redirectLink(`${UrlChain}/Approved/1`); this.setActive(Url); }
          )}
          key={Id}
          className={activeMenu.includes(Url) && 'active'}
        >
          {Name}
        </Button>
        <div>
          <Link className="show" to={{ pathname: `/category/add/${Url}`, state: { Name, Id, UrlChain } }}>Add</Link> {/* eslint-disable-line jsx-a11y/anchor-is-valid */}
          <Link className="show" to={{ pathname: `/category/edit/${Url}`, state: { Name, Id, UrlChain } }}>Edit</Link> {/* eslint-disable-line jsx-a11y/anchor-is-valid */}
          <Toggle>
            {({ on, toggle }) => (
              <Fragment>
                <Button onClick={toggle} className="show">Delete</Button>
                <ModalConfirmation
                  on={on}
                  header={`Delete submenu "${Name}" ?`}
                  onCancel={toggle}
                  onOk={() => { onSendDeleteId({ Id }); }}
                >
                  {`Submenu ${Name} will be removed`}
                </ModalConfirmation>
              </Fragment>
            )}
          </Toggle>
        </div>
      </React.Fragment>
    );
  };
  render() {
    const { sidebarCategories, activeMenu, loading } = this.props;
    return (
      <StyledSidebar className="sidebar">
        {loading ?
          <Loading />
          :
          <Ul>
            <Li>
              <Button
                className={activeMenu.includes('categories') && 'active'}
                onClick={() => this.resetActive('categories')}
              >
                Categories
              </Button>
              <Link // eslint-disable-line jsx-a11y/anchor-is-valid
                to="/newcategory"
                className="add-category"
                onClick={() => this.setActive('newcategory')}
              >
                Add root category
              </Link>
              {this.nesting(sidebarCategories[0])}
            </Li>
            <Li>
              <Button
                className={activeMenu.includes('exchange') && 'active'}
                onClick={() => this.resetActive('exchange')}
              >
                Create new exchange
              </Button>
              <Ul>
                <Li>
                  <Link // eslint-disable-line jsx-a11y/anchor-is-valid
                    to="/feed"
                    className={`${activeMenu.includes('exchange_list') &&
                    'active'} feed`}
                    onClick={() => this.setActive('exchange_list')}
                  >
                    List of events
                  </Link>
                </Li>
              </Ul>
            </Li>
          </Ul>
        }
      </StyledSidebar>
    );
  }
}

Sidebar.propTypes = {
  sidebarCategories: PropTypes.array,
  activeMenu: PropTypes.array.isRequired,
  onGetSidebar: PropTypes.func.isRequired,
  onSetActiveMenu: PropTypes.func.isRequired,
  onResetActiveMenu: PropTypes.func.isRequired,
  redirectLink: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  pathname: PropTypes.string.isRequired,
  onSendDeleteId: PropTypes.func.isRequired,
};

export function mapDispatchToProps(dispatch) {
  return {
    onGetSidebar: () => dispatch(sidebarLoad()),
    onSetActiveMenu: activeMenu => dispatch(setActiveMenu(activeMenu)),
    onResetActiveMenu: activeMenu => dispatch(resetActiveMenu(activeMenu)),
    redirectLink: link => dispatch(push(`/${link}`)),
    onSendDeleteId: data => dispatch(categorySendDelete(data)),
  };
}

const mapStateToProps = createStructuredSelector({
  sidebarCategories: makeSelectSidebar(),
  activeMenu: activeMenuSelector(),
  loading: loadingSelector(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: SIDEBAR, reducer });
const withSaga = injectSaga({ key: SIDEBAR, saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(Sidebar);
