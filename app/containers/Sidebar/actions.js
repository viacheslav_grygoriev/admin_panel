import {
  LOAD_SIDEBAR_DATA,
  LOAD_SIDEBAR_DATA_SUCCESS,
  LOAD_SIDEBAR_DATA_ERROR,
  SET_ACTIVE_MENU,
  PUT_SET_ACTIVE_MENU,
  RESET_ACTIVE_MENU,
  PUT_RESET_ACTIVE_MENU,
  CATEGORY_SEND_DELETE,
} from './constants';

export function sidebarLoad() {
  return {
    type: LOAD_SIDEBAR_DATA,
  };
}

export function sidebarLoaded(data) {
  return {
    type: LOAD_SIDEBAR_DATA_SUCCESS,
    data,
  };
}

export function sidebarLoadedError(error) {
  return {
    type: LOAD_SIDEBAR_DATA_ERROR,
    error,
  };
}

export function setActiveMenu(data) {
  return {
    type: SET_ACTIVE_MENU,
    data,
  };
}

export function putSetActiveMenu(data) {
  return {
    type: PUT_SET_ACTIVE_MENU,
    data,
  };
}

export function resetActiveMenu(data) {
  return {
    type: RESET_ACTIVE_MENU,
    data,
  };
}

export function putResetActiveMenu(data) {
  return {
    type: PUT_RESET_ACTIVE_MENU,
    data,
  };
}

export const categorySendDelete = data => (
  {
    type: CATEGORY_SEND_DELETE,
    data,
  }
);
