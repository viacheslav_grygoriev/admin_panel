import { call, put, takeLatest, all, take, select } from 'redux-saga/effects';
import { request, showError, GET_MENU_URL, REMOVE_CATEGORY_URL } from '../../utils';
import { LOAD_SIDEBAR_DATA, SET_ACTIVE_MENU, RESET_ACTIVE_MENU, CATEGORY_SEND_DELETE } from './constants';
import { sidebarLoaded, sidebarLoadedError, putSetActiveMenu, putResetActiveMenu } from './actions';
import { refreshToken } from '../../containers/SiteApp/actions';
import { REFRESH_TOKEN_SUCCESS } from '../../containers/SiteApp/constants';
import { requestingTokenSelector } from '../SiteApp/selectors';

/**
 * Github repos request/response handler
 */
export function* getSidebarSaga() {
  try {
    // Call our request helper (see 'utils/request')
    const response = yield call(request, GET_MENU_URL);
    // костыль начало
    if (response.status === 401) {
      const requesting = yield select(requestingTokenSelector());
      if (requesting) {
        yield take(REFRESH_TOKEN_SUCCESS);
      }
      yield put(refreshToken());
      yield take(REFRESH_TOKEN_SUCCESS);
      yield getSidebarSaga();
      return;
    }
    // костыль конец

    yield put(sidebarLoaded(response));
  } catch (error) {
    yield put(sidebarLoadedError(error));
    showError(error.message);
  }
}

export function* setActiveMenuSaga(action) {
  yield put(putSetActiveMenu(action.data));
}

export function* resetActiveMenuSaga(action) {
  yield put(putResetActiveMenu(action.data));
}

export function* categorySendDeleteSaga(action) {
  try {
    const { data: { Id } } = action;
    const response = yield call(request, REMOVE_CATEGORY_URL, {
      method: 'DELETE',
      body: JSON.stringify({
        CategoryId: Id,
      }),
    });
    // костыль начало
    if (response.status === 401) {
      const requesting = yield select(requestingTokenSelector());
      if (requesting) {
        yield take(REFRESH_TOKEN_SUCCESS);
      }
      yield put(refreshToken());
      yield take(REFRESH_TOKEN_SUCCESS);
      yield call(request, REMOVE_CATEGORY_URL, {
        method: 'DELETE',
        body: JSON.stringify({
          CategoryId: Id,
        }),
      });
      return;
    }
    // костыль конец
    yield getSidebarSaga();
  } catch (error) {
    showError(error.message);
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* sidebarData() {
  yield all([
    yield takeLatest(LOAD_SIDEBAR_DATA, getSidebarSaga),
    yield takeLatest(SET_ACTIVE_MENU, setActiveMenuSaga),
    yield takeLatest(RESET_ACTIVE_MENU, resetActiveMenuSaga),
    yield takeLatest(CATEGORY_SEND_DELETE, categorySendDeleteSaga),
  ]);
}
