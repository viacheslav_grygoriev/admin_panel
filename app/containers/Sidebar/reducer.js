import { Map, List } from 'immutable';

import {
  LOAD_SIDEBAR_DATA,
  LOAD_SIDEBAR_DATA_SUCCESS,
  LOAD_SIDEBAR_DATA_ERROR,
  PUT_SET_ACTIVE_MENU,
  PUT_RESET_ACTIVE_MENU,
} from './constants';

// The initial state of the App
const initialState = Map({
  Menu: null,
  loading: true,
  active: List(['categories']),
});

function sidebarReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_SIDEBAR_DATA:
      return state.set('loading', true);
    case LOAD_SIDEBAR_DATA_SUCCESS:
      return state.set('Menu', action.data).set('loading', false);
    case LOAD_SIDEBAR_DATA_ERROR:
      return state.set('loading', true);

    case PUT_SET_ACTIVE_MENU:
      return state.updateIn(['active'], (list) => {
        if (list.includes(action.data)) {
          return list.splice(list.indexOf(action.data), 1);
        }
        return list.push(action.data);
      });

    case PUT_RESET_ACTIVE_MENU:
      return state.updateIn(['active'], (list) => {
        if (list.includes(action.data)) {
          return list.clear();
        }
        return List([action.data]);
      });

    default:
      return state;
  }
}

export default sidebarReducer;
