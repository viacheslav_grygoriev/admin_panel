import { Map } from 'immutable';

import {
  CATEGORY_SEND,
  CATEGORY_SEND_ERROR,
  CATEGORY_SEND_SUCCESS,
} from './constants';

// The initial state of the App
const initialState = Map({
  success: '',
  loading: true,
  error: '',
});

function categoryReducer(state = initialState, action) {
  switch (action.type) {
    case CATEGORY_SEND:
      return state.set('loading', true);
    case CATEGORY_SEND_SUCCESS:
      return state
        .set('success', action.data)
        .set('loading', false);
    case CATEGORY_SEND_ERROR:
      return state
        .set('error', action.data)
        .set('loading', true);
    default:
      return state;
  }
}

export default categoryReducer;
