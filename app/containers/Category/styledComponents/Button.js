import NormalButton from 'components/Button/index';

export default NormalButton.extend`
  margin-right: 10px;
  &:last-child {
    margin-right: 0;
  }
`;
