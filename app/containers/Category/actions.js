import { CATEGORY_SEND, CATEGORY_SEND_SUCCESS, CATEGORY_SEND_ERROR } from './constants';

export const categorySend = (formName, fields, props) => (
  {
    type: CATEGORY_SEND,
    data: { formName, fields, props },
  }
);

export const categorySendSuccess = data => (
  {
    type: CATEGORY_SEND_SUCCESS,
    data,
  }
);

export const categorySendError = error => (
  {
    type: CATEGORY_SEND_ERROR,
    error,
  }
);

