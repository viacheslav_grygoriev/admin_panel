import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { compose } from 'redux';
import { reduxForm, Field } from 'redux-form/immutable';
import { propTypes } from 'redux-form';
import { createStructuredSelector } from 'reselect';

import RenderField from 'components/RenderField';

import Loading from 'components/Loading';
import { formSubmit, injectReducer, injectSaga } from 'utils';
import SelectField from 'components/SelectField';

import Button from './styledComponents/Button';
import P from './styledComponents/P';
import H1 from './styledComponents/H1';
import { categorySend } from './actions';
import { CATEGORY } from './constants';
import reducer from './reducer';
import saga from './saga';

const FlexWrapper = styled.div`
  margin-top: 20px;
  text-align: right;
`;

const CategoryForm = ({
  handleSubmit,
  pristine,
  reset,
  submitting,
  valid,
  anyTouched,
  errorSendData,
  loading,
  match,
  location,
}) => {
  if (loading) return <Loading />;
  const { params: { action } } = match;
  const { state: { Name: categoryName } } = location;
  return (
    <React.Fragment>
      {(() => {
        switch (action) {
          case 'add':
            return <H1>{`Add submenu in "${categoryName}"`}</H1>;

          case 'edit':
            return <H1>{`Edit menu "${categoryName}"`}</H1>;

          default:
            throw Error('Wrong action in "Category" component');
        }
      })()}
      <form onSubmit={handleSubmit}>
        <Field
          name="Name"
          type="text"
          component={RenderField}
          label="Name"
          anyTouched={anyTouched}
        />
        <Field
          name="Url"
          type="text"
          component={RenderField}
          label="Url"
          anyTouched={anyTouched}
        />
        <Field
          name="Icon"
          component={SelectField}
          label="Icon"
          firstOptionName="- noicon -"
          anyTouched={anyTouched}
        />
        <FlexWrapper>
          {errorSendData && <P>{errorSendData}</P>}
          <Button blue type="submit" disabled={!valid || submitting}>
            Submit
          </Button>
          <Button type="button" disabled={pristine || submitting} onClick={reset}>
            Clear Values
          </Button>
        </FlexWrapper>
      </form>
    </React.Fragment>
  );
};

CategoryForm.propTypes = {
  ...propTypes,
  loading: PropTypes.bool,
  params: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({

});

// export function mapDispatchToProps(dispatch) {
//   return {
//
//   };
// }

const withConnect = connect(mapStateToProps);

const withReducer = injectReducer({ key: CATEGORY, reducer });
const withSaga = injectSaga({ key: CATEGORY, saga });

const withForm = reduxForm({
  form: 'CategoryForm',
  validate,
  onSubmit: formSubmit(categorySend),
});

export default compose(
  withReducer,
  withSaga,
  withConnect,
  withForm,
)(CategoryForm);

function validate({ Name, Url }) {
  const errors = {};

  // Name validation
  if (!Name) errors.Name = 'Username is required';

  // Url validation
  if (!Url) errors.Url = 'Url is required';

  return errors;
}

