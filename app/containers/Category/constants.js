// container's name
export const CATEGORY = 'CATEGORY';

export const CATEGORY_SEND = `altbet/${CATEGORY}/CATEGORY_SEND`;
export const CATEGORY_SEND_SUCCESS = `altbet/${CATEGORY}/CATEGORY_SEND_SUCCESS`;
export const CATEGORY_SEND_ERROR = `altbet/${CATEGORY}/CATEGORY_SEND_ERROR`;
