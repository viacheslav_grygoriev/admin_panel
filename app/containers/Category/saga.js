import { call, put, all, takeLatest, select, take } from 'redux-saga/effects';
import {
  startSubmit,
  stopSubmit,
  reset,
  SubmissionError,
} from 'redux-form';

import { request, showError, DEFAULT_SITENAME, namesComparator } from '../../utils';
import { CATEGORY_SEND } from './constants';
import { categorySendError } from './actions';
import { getSidebarSaga } from '../../containers/Sidebar/saga';
import { refreshToken } from '../SiteApp/actions';
import { REFRESH_TOKEN_SUCCESS } from '../SiteApp/constants';
import { requestingTokenSelector } from '../SiteApp/selectors';

export function* categorySendSaga(action) {
  const isAddMethod = () => namesComparator(categoryType, 'add');
  const { data: { fields, formName, props } } = action;
  const { Name, Url } = fields;
  const { match: { params: { action: categoryType } }, location: { state: { Id, UrlChain } } } = props;
  const modifiedUrl = UrlChain.split('/').slice(0, -1).concat(Url).join('/');
  const idFieldName = isAddMethod() ? 'ParentId' : 'Id';
  const url = `${DEFAULT_SITENAME}/category/${categoryType}category`;
  try {
    yield put(startSubmit(formName));
    const response = yield call(request, url, {
      method: isAddMethod() ? 'PUT' : 'POST',
      body: JSON.stringify({
        [idFieldName]: Id,
        Name,
        UrlChain: isAddMethod() ? `${UrlChain}/${Url}` : modifiedUrl,
        Icon: 'blabla',
      }),
    });
    // костыль начало
    if (response.status === 401) {
      const requesting = yield select(requestingTokenSelector());
      if (requesting) {
        yield take(REFRESH_TOKEN_SUCCESS);
      }
      yield put(refreshToken());
      yield take(REFRESH_TOKEN_SUCCESS);
      yield categorySendSaga(action);
      return;
    }
    // костыль конец
    yield put(stopSubmit(formName));
    yield put(reset(formName));
    yield getSidebarSaga();
  } catch (error) {
    yield put(stopSubmit(formName));
    yield put(categorySendError(error.message));
    showError(error.message);
    throw new SubmissionError(error.message);
  }
}

export default function* categorySaga() {
  yield all([
    yield takeLatest(CATEGORY_SEND, categorySendSaga),
  ]);
}
