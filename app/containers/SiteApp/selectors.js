/* eslint-disable */
import { createSelector } from 'reselect';
import { SITE_APP } from './constants';

const selectSiteApp = (state) => state.get(SITE_APP).toJS();

const requestingTokenSelector = () => createSelector(
  [selectSiteApp],
  (selectSiteApp) => selectSiteApp.requestingToken
);

// const notificationSelector = () => createSelector(
//   [selectSiteApp],
//   (selectSiteApp) => selectSiteApp.notification
// );

export {
  requestingTokenSelector,
  // notificationSelector,
}
