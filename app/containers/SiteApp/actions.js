import { REFRESH_TOKEN, REFRESH_TOKEN_SUCCESS, REFRESH_TOKEN_ERROR,
  REMOVE_TOKEN,
  SETUP_TOKEN, SETUP_TOKEN_SUCCESS, SETUP_TOKEN_ERROR,
  // SHOW_NOTIFICATION, HIDE_NOTIFICATION, REQUEST_NOTIFICATION,
} from './constants';

export const refreshToken = () => ({
  type: REFRESH_TOKEN,
});

export const refreshTokenSuccess = () => ({
  type: REFRESH_TOKEN_SUCCESS,
});

export const refreshTokenError = error => ({
  type: REFRESH_TOKEN_ERROR,
  error,
});

export const setupToken = data => ({
  type: SETUP_TOKEN,
  data,
});

export const setupTokenSuccess = () => ({
  type: SETUP_TOKEN_SUCCESS,
});

export const setupTokenError = error => ({
  type: SETUP_TOKEN_ERROR,
  error,
});

export const removeToken = () => ({
  type: REMOVE_TOKEN,
});
//
// export const requestNotification = data => ({
//   type: REQUEST_NOTIFICATION,
//   data,
// });
//
// export const showNotification = data => ({
//   type: SHOW_NOTIFICATION,
//   data,
// });
//
// export const hideNotification = () => ({
//   type: HIDE_NOTIFICATION,
// });
