import { Map } from 'immutable';

import {
  REFRESH_TOKEN, REFRESH_TOKEN_SUCCESS, REFRESH_TOKEN_ERROR,
  SETUP_TOKEN, SETUP_TOKEN_SUCCESS, SETUP_TOKEN_ERROR,
  // SHOW_NOTIFICATION, HIDE_NOTIFICATION,
  REMOVE_TOKEN,
} from './constants';

const initialState = Map({
  requestingToken: false,
  tokenIsInStorage: false,
  error: null,
  // notification: {},
});

export default function siteAppReducer(state = initialState, action) {
  switch (action.type) {
    case SETUP_TOKEN:
    case REFRESH_TOKEN:
      return state
        .set('requestingToken', true);
    case SETUP_TOKEN_SUCCESS:
    case REFRESH_TOKEN_SUCCESS:
      return state
        .set('tokenIsInStorage', true)
        .set('requestingToken', false);

    case SETUP_TOKEN_ERROR:
    case REFRESH_TOKEN_ERROR:
      return state
        .set('error', action.data.message);

    case REMOVE_TOKEN:
      return state
        .set('tokenIsInStorage', false)
        .set('requestingToken', false);

    // case SHOW_NOTIFICATION:
    //   return state
    //     .set('notification', { message: action.data.message, type: action.data.type });
    //
    // case HIDE_NOTIFICATION:
    //   return state
    //     .set('notificationType', null)
    //     .set('notificationMessage', null);

    default:
      return state;
  }
}
