import { all, takeLatest, put, call } from 'redux-saga/effects';
// import { delay } from 'redux-saga';
import { push } from 'react-router-redux';
import {
  REFRESH_TOKEN, SETUP_TOKEN, REMOVE_TOKEN,
  // REQUEST_NOTIFICATION,
} from './constants';
import {
  setupTokenSuccess, setupTokenError, refreshTokenSuccess, refreshTokenError,
  // showNotification, hideNotification,
} from './actions';
import { TOKEN_URL, adminTokenData, request } from '../../utils';
import { resetStatus } from '../../containers/Home/actions';

export function* refreshTokenSaga() {
  try {
    const { refresh_token } = adminTokenData(); // eslint-disable-line camelcase
    const response = yield call(request, TOKEN_URL, {
      method: 'POST',
      body: `refresh_token=${refresh_token}&grant_type=refresh_token&client_id=DesktopApp&client_Secret=in-play`, // eslint-disable-line camelcase
    });
    window.localStorage.setItem('adminToken', JSON.stringify(response));
    yield put(refreshTokenSuccess());
  } catch (error) {
    yield put(refreshTokenError(error));
  }
}

export function* setupTokenSaga(action) {
  try {
    const { login, password } = action.data;
    const response = yield call(request, TOKEN_URL, {
      method: 'POST',
      body: `UserName=${login}&Password=${password}&grant_type=password&client_id=DesktopApp&client_Secret=in-play`,
    });
    window.localStorage.setItem('adminToken', JSON.stringify(response));
    yield put(push('/'));
    yield put(setupTokenSuccess());
  } catch (error) {
    yield put(setupTokenError(error));
  }
}

export function* removeTokenSaga() {
  window.localStorage.removeItem('adminToken');
  yield put(push('/login'));
  yield put(resetStatus());
}

// export function* requestNotificationSaga(action) {
//   yield put(showNotification(action.data));
//   yield delay(5000);
//   yield put(hideNotification());
// }

export default function* siteAppSaga() {
  yield all([
    yield takeLatest(SETUP_TOKEN, setupTokenSaga),
    yield takeLatest(REFRESH_TOKEN, refreshTokenSaga),
    yield takeLatest(REMOVE_TOKEN, removeTokenSaga),
    // yield takeLatest(REQUEST_NOTIFICATION, requestNotificationSaga),
  ]);
}
