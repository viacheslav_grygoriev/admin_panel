import React, { Fragment } from 'react';
// import PropTypes from 'prop-types';
import { Switch, Route } from 'react-router-dom';
// import { connect } from 'react-redux';
import { compose } from 'redux';
// import { createStructuredSelector } from 'reselect';

import App from 'containers/App';
import Login from 'components/Login';
import NotFoundPage from 'containers/NotFoundPage/Loadable';

import saga from './saga';
import reducer from './reducer';

import { SITE_APP } from './constants';
import { injectSaga, injectReducer } from '../../utils';

import ProtectedPathChecker from '../../components/ProtectedPath/protectedPathChecker';
// import { notificationSelector } from './selectors';

const SiteApp = () => (
  <Fragment>
    <Switch>
      <Route path="/login" component={Login}/>
      <ProtectedPathChecker path="/" component={App} redirectTo="/login"/>
      <Route component={NotFoundPage}/>
    </Switch>
  </Fragment>
);
SiteApp.propTypes = {
  // notification: PropTypes.object.isRequired,
};
// const mapStateToProps = createStructuredSelector({
//   notification: notificationSelector(),
// });
// const withConnect = connect(mapStateToProps, null);

const withReducer = injectReducer({ key: SITE_APP, reducer });
const withSaga = injectSaga({ key: SITE_APP, saga });

export default compose(
  withReducer,
  withSaga,
  // withConnect,
)(SiteApp);

// export default SiteApp
