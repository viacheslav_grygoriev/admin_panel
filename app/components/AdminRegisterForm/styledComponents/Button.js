import NormalButton from '../../Button/index';

export default NormalButton.extend`
  margin-right: 10px;
  &:last-child {
    margin-right: 0;
  }
`;
