import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form/immutable';
import { propTypes } from 'redux-form';
import { createStructuredSelector } from 'reselect';
import { addNewForm } from 'containers/AddNew/actions';
import { formSubmit } from 'utils';

import RenderField from '../RenderField/index';
import Button from './styledComponents/Button';
import P from './styledComponents/P';
import {
  errorSelector,
  loadingSelector,
} from '../../containers/AddNew/selectors';
import Loading from '../../components/Loading';

const FlexWrapper = styled.div`
  margin-top: 20px;
  text-align: right;
`;

const AdminRegisterForm = ({
  handleSubmit,
  pristine,
  reset,
  submitting,
  valid,
  anyTouched,
  errorSendData,
  loading,
}) => {
  if (loading) return <Loading />;
  return (
    <form onSubmit={handleSubmit}>
      <Field
        name="username"
        type="text"
        component={RenderField}
        label="Username"
        anyTouched={anyTouched}
      />
      <Field
        name="password"
        type="password"
        component={RenderField}
        label="Password"
        anyTouched={anyTouched}
      />
      <Field
        name="confirmPassword"
        type="password"
        component={RenderField}
        label="Confirm password"
        anyTouched={anyTouched}
      />
      <FlexWrapper>
        {errorSendData && <P>{errorSendData}</P>}
        <Button blue type="submit" disabled={!valid || submitting}>
          Submit
        </Button>
        <Button type="button" disabled={pristine || submitting} onClick={reset}>
          Clear Values
        </Button>
      </FlexWrapper>
    </form>
  );
};

AdminRegisterForm.propTypes = {
  ...propTypes,
  errorSendData: PropTypes.string.isRequired,
  loading: PropTypes.bool.isRequired,
};

const mapStateToProps = createStructuredSelector({
  errorSendData: errorSelector(),
  loading: loadingSelector(),
});

export default connect(mapStateToProps)(reduxForm({
  form: 'AdminRegisterForm',
  validate,
  onSubmit: formSubmit(addNewForm),
})(AdminRegisterForm));

function validate({ username, password = '', confirmPassword }) {
  const errors = {};
  const matchPattern = password.match(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{6,}$/);

  // username validation
  if (!username) errors.username = 'Username is required';

  // password validation
  if (!password) errors.password = 'Password is required';
  if (!matchPattern) errors.password = 'Password should consist of minimum 6 characters, at least one letter, one number and one special character';

  // password confirmation validation
  if (password && !confirmPassword && matchPattern) errors.confirmPassword = 'Password confirmation is required';

  if (
    password &&
    confirmPassword &&
    matchPattern &&
    password !== confirmPassword
  ) {
    errors.confirmPassword = 'Passwords should be equal';
  }
  return errors;
}
