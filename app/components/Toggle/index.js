import React from 'react';
import PropTypes from 'prop-types';

export default class Toggle extends React.Component {
  state = {
    on: false,
  };

  toggle = () => {
    this.setState({
      on: !this.state.on,
    });
  };

  render() {
    const { children } = this.props;
    const { on } = this.state;
    return children({ on, toggle: this.toggle });
  }
}

Toggle.propTypes = {
  children: PropTypes.func,
};
