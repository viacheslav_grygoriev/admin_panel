/* eslint-disable */

import { connect } from 'react-redux';
// import { isUndefined, isNull } from 'lodash';
import { adminTokenData } from '../../utils';
import ProtectedPath from './index';

const mapStateToProps = (state) => {
  // In this example the auth reducer has a key
  // called authenticated which determines if the
  // user is authenticated or not

  const { token_type, access_token } = adminTokenData();
  // __DEV__&&console.log("token_type", token_type);
  // __DEV__&&console.log("access_token", access_token);
  const authenticated = !!(token_type && access_token);
  return {
    authenticated,
  };
};

export default connect(mapStateToProps)(ProtectedPath);
