import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';

const ProtectedPath = ({
  component: Component,
  authenticated,
  redirectTo,
  ...rest
}) => (
  <Route
    {...rest}
    render={props => (
      authenticated ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: redirectTo,
            state: { from: props.location },
          }}
        />
      )
    )}
  />
);


ProtectedPath.propTypes = {
  authenticated: PropTypes.bool,
  redirectTo: PropTypes.string.isRequired,
  component: PropTypes.func.isRequired,
  location: PropTypes.object.isRequired,
};

ProtectedPath.defaultProps = {
  authenticated: false,
};

export default ProtectedPath;
