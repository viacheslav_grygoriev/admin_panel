import NormalLabel from 'components/Label';

export default NormalLabel.extend`
  margin-top: 10px;
  display: block;
`;
