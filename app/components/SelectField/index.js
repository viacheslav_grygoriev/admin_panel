import React from 'react';
import PropTypes from 'prop-types';

import Select from './styledComponents/Select';
import Label from './styledComponents/Label';
import P from './styledComponents/P';

function SelectField({
  input,
  label,
  meta: { error },
  items,
  firstOptionName,
  anyTouched,
}) {
  const errorText = anyTouched && error && <P>{error}</P>;
  return (
    <div>
      <Label htmlFor={input.name}>{label}</Label>
      <Select {...input} id={input.name}>
        <option value="">{firstOptionName}</option>
        {items && items.map(item => (
          <option key={item.Code} value={item.Code}>
            {item.Name}
          </option>
        ))}
      </Select>
      {errorText}
    </div>
  );
}

SelectField.propTypes = {
  input: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired,
  anyTouched: PropTypes.bool.isRequired,
  items: PropTypes.array,
  firstOptionName: PropTypes.string,
};

export default SelectField;
