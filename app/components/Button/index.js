import styled, { css } from 'styled-components';
import { WHITE_LINK, WHITE_LINK_HOVER, BLACK_LINK, BLACK_LINK_HOVER } from 'utils';

export default styled.button`
  display: inline-block;
  padding: 6px 12px;
  font-size: 14px;
  font-weight: normal;
  line-height: 1.4;
  text-align: center;
  white-space: nowrap;
  vertical-align: middle;
  touch-action: manipulation;
  cursor: pointer;  
  user-select: none;
  background: #f4f4f4;
  border: 1px solid #ddd;
  border-radius: 4px;
  color: #333;
  outline: none;

  &:active {
    background-color: #e7e7e7;
  }
  &:focus {
    background-color: #e6e6e6;
    border-color: #8c8c8c;    
  }
  &:hover {
    background-color: #e7e7e7;
    border-color: #adadad;
  }
  &:active:hover, &:active:focus {
    background-color: #d4d4d4;
    border-color: #8c8c8c;
  }
  &[disabled] {
    cursor: not-allowed;    
    box-shadow: none;
    opacity: .65;
  }
  &[disabled]:hover {
    background-color: #fff;
    border-color: #ccc;
  }
  
  ${props =>
    props.pressed &&
    css`
      color: #fff;
      background-color: #286090;
      border-color: #204d74;

      &:active,
      &:focus,
      &:hover,
      &:active:hover,
      &:active:focus {
        color: #fff;
        background-color: #286090;
        border-color: #204d74;
      }
    `}
  
  ${props =>
    props.blue &&
    css`
      background-color: #00c0ef;
      border-color: #00acd6;
      color: white;
      &:active {
        background-color: #00acd6;
        box-shadow: inset 0 3px 5px rgba(0, 0, 0, 0.125);
        border-color: #269abc;
      }
      &:focus {
        background-color: #31b0d5;
        border-color: #1b6d85;
      }
      &:hover {
        background-color: #00acd6;
        border-color: #269abc;
      }
      &:active:hover,
      &:active:focus {
        background-color: #269abc;
        border-color: #1b6d85;
      }
      &[disabled]:hover {
        background-color: #5bc0de;
        border-color: #46b8da;
      }
    `}  
  
  ${props =>
    props.link &&
    css`
      background-color: transparent;
      color: ${props.black ? WHITE_LINK : BLACK_LINK};
      border-radius: 0;
      height: 100%;
      border: none;
      &:active {
        background-color: transparent;
      }
      &:focus {
        background-color: transparent;
      }
      &:hover {
        background-color: transparent;
        color: ${props.black
    ? WHITE_LINK_HOVER
    : BLACK_LINK_HOVER};
      }
      &:active:hover,
      &:active:focus {
        background-color: transparent;
      }
      &[disabled]:hover {
        background-color: transparent;
      }
    `}
`;
