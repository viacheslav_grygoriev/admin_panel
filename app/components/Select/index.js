import styled from 'styled-components';
import { INPUT_BORDER, WHITE } from 'utils';

export default styled.select`
  width: 100%;
  height: 34px;
  padding: 6px 12px;
  font-size: 14px;
  line-height: 1.4;
  background-color: ${WHITE};
  border: 1px solid ${INPUT_BORDER};
  border-radius: 4px;
  box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
  transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
`;
