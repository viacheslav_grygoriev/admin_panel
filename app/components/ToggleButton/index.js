import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Button from './styledComponents/Button';

export default class ToggleButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false,
    };
    this.click = this.click.bind(this);
  }

  click() {
    const { onClick } = this.props;
    this.setState({
      active: !this.state.active,
    });
    onClick();
  }

  render() {
    const { children } = this.props;
    const { active } = this.state;

    return (
      <Button {...this.props} onClick={this.click} pressed={active}>
        {children}
      </Button>
    );
  }
}

ToggleButton.propTypes = {
  onClick: PropTypes.func,
  children: PropTypes.string.isRequired,
};
