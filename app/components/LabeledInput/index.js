import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Input from './styledComponents/Input';
import Label from './styledComponents/Label';

class LabeledInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      error: false,
    };

    this.changeInput = this.changeInput.bind(this);
    this.validate = this.validate.bind(this);
    this.isError = this.isError.bind(this);
  }

  componentDidMount() {
    const { value } = this.props;
    if (value) {
      this.setState({ value }); // eslint-disable-line  react/no-did-mount-set-state
    }
    this.isError(value);
  }

  getData() {
    const { value } = this.state;
    return this.validate(value) ? value : '';
  }

  isError(str) {
    this.setState({ error: !this.validate(str) });
  }

  validate(str) {
    const { validator = /.+/ } = this.props;
    const val = new RegExp(validator);
    return val.test(str);
  }

  changeInput(e) {
    this.state.value = e.target.value;
    this.isError(e.target.value);
  }

  render() {
    const {
      name, label = 'label', type = 'text', onChange,
    } = this.props;
    const { value, error } = this.state;
    return (
      <div>
        <Label htmlFor={name}>{label}</Label>
        <Input
          {...this.props}
          id={name}
          type={type}
          value={value}
          onChange={(e) => {
            this.changeInput(e);
            if (onChange) onChange(e);
          }}
          error={error}
        />
      </div>
    );
  }
}

LabeledInput.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  type: PropTypes.string,
  validator: PropTypes.any,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onChange: PropTypes.func,
};

export default LabeledInput;
