import { css } from 'styled-components';
import { ERROR_COLOR } from 'utils';
import NormalInput from '../../Input/index';

export default NormalInput.extend`
  ${props =>
    props.error &&
    css`
      border-color: ${ERROR_COLOR};
      outline: none;
    `};
`;
