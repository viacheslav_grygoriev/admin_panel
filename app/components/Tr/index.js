import styled, { css } from 'styled-components';
import { BIG_TABLE_HEIGHT, SIDEBAR_BACKGROUND } from 'utils';

export default styled.tr`
  ${props =>
    props.bigTableHeading &&
    css`
      height: ${BIG_TABLE_HEIGHT};
    `} ${props =>
  props.bigTable &&
    css`
      border-top: 1px solid ${SIDEBAR_BACKGROUND};
      height: ${BIG_TABLE_HEIGHT};

      &:hover {
        background-color: rgba(0, 0, 0, 0.03);
      }
    `};
`;
