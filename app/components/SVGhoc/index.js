import React from 'react';
import PropTypes from 'prop-types';

const SVGhoc = (props) => {
  const {
    component: Component,
    height,
    width,
  } = props;
  return (
    <Component height={height} width={width} {...props} />
  );
};

SVGhoc.propTypes = {
  component: PropTypes.element.isRequired,
  height: PropTypes.oneOfType([
    PropTypes.string, PropTypes.number,
  ]).isRequired,
  width: PropTypes.oneOfType([
    PropTypes.string, PropTypes.number,
  ]).isRequired,
};

export default SVGhoc;
