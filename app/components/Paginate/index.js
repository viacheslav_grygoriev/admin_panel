import React from 'react';
import PropTypes from 'prop-types';
import ReactPaginate from 'react-paginate';
import { PaginateWrapper } from './styledComponents/styledComponents';

const Paginate = ({ handlePageClick, pageInfo, page }) => {
  const { TotalPages } = pageInfo;

  if (TotalPages <= 1) return <PaginateWrapper />;
  return (
    <PaginateWrapper>
      <ReactPaginate
        previousLabel="previous"
        nextLabel="next"
        breakClassName="break-me"
        pageCount={TotalPages}
        marginPagesDisplayed={2}
        pageRangeDisplayed={5}
        forcePage={page - 1}
        onPageChange={handlePageClick}
        containerClassName="pagination"
        activeClassName="active"
        disableInitialCallback
      />
    </PaginateWrapper>
  );
};

Paginate.propTypes = {
  handlePageClick: PropTypes.func.isRequired,
  pageInfo: PropTypes.shape({
    CurrentPage: PropTypes.number,
    FirstPage: PropTypes.number,
    LastPage: PropTypes.number,
    PageSize: PropTypes.number,
    TotalItems: PropTypes.number,
    TotalPages: PropTypes.number,
  }).isRequired,
  page: PropTypes.number,
};

export default Paginate;
