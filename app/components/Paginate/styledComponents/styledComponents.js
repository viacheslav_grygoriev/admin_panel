import styled from 'styled-components';

export const PaginateWrapper = styled.div`
      display: flex;
      justify-content: flex-end;
      .pagination {
        display: flex;
        list-style: none;
      };
      
      li {
        margin: 0 5px;
      };
      a {
        cursor: pointer;
      }
      .active {
        background-color: aqua
      };
  `;
