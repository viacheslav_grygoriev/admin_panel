import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';

const FlexItem = ({ className, children, flexDirection = 'column', justifyContent = 'flex-start', error }) => {
  const Wrapper = styled.div`
  display: flex;
  flex-direction: ${flexDirection};
  justify-content: ${justifyContent};
  ${() => error && css`
    outline: 1px solid red;
  `};
  padding: 0 10px;
  &:first-child {
    padding-left: 0;
  }
  &:last-child {
    padding-right: 0;
  }
`;
  return (
    <Wrapper className={className}>
      {children}
    </Wrapper>
  );
};

FlexItem.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.array,
  ]),
  className: PropTypes.string,
  flexDirection: PropTypes.string,
  justifyContent: PropTypes.string,
  error: PropTypes.bool,
};

export default FlexItem;
