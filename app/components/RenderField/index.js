import React from 'react';
import PropTypes from 'prop-types';

import Input from './styledComponents/Input';
import Label from './styledComponents/Label';
import P from './styledComponents/P';

function RenderField({
  input, type, label, meta: { error }, anyTouched,
}) {
  const errorText = anyTouched && error && <P>{error}</P>;
  return (
    <div>
      <Label htmlFor={input.name}>{label}</Label>
      <Input {...input} type={type} id={input.name} />
      {errorText}
    </div>
  );
}

RenderField.propTypes = {
  input: PropTypes.object.isRequired,
  type: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired,
  anyTouched: PropTypes.bool.isRequired,
};

export default RenderField;
