import NormalLabel from '../../Label/index';

export default NormalLabel.extend`
  margin-top: 10px;
  display: block;
`;
