import React from 'react';
import PropTypes from 'prop-types';
import { ModalWindow } from './styledComponents/StyledComponents';
import Portal from '../../components/Portal';


const ModalNotificationFunc = ({ children, type }) => ( // eslint-disable-line react/prop-types
  <ModalWindow type={type}>
    {children}
  </ModalWindow>
);

const ModalNotification = ({ type, children }) => (
  <Portal>
    {ModalNotificationFunc({ children, type })}
  </Portal>
);

ModalNotification.propTypes = {
  type: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.string,
  ]),
};

export default ModalNotification;
