import styled, { css } from 'styled-components';
import { NOTIFICATION_TYPE_ERROR, NOTIFICATION_TYPE_SUCCESS, NOTIFICATION_TYPE_WARNING } from '../../../utils/index';

export const ModalWindow = styled.div`
  position: fixed;
  top: 70px;
  right: 30px;
  display: flex;
  flex-direction: column;
  max-width: 40%;
  z-index: 13;
  padding: 20px 25px;
  ${props => props.type && css`
    ${(() => {
    switch (props.type) {
      case NOTIFICATION_TYPE_SUCCESS:
        return 'background: greenyellow';
      case NOTIFICATION_TYPE_WARNING:
        return 'background: orange';
      case NOTIFICATION_TYPE_ERROR:
        return 'background: orangered';
      default:
        return 'background: white';
    }
  })()}
`}`;
