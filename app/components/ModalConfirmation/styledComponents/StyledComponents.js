import styled from 'styled-components';
import { LIGHT_GRAY } from '../../../utils/index';

export const ModalBackground = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background: rgba(0,0,0,0.3);
  z-index: 11;
`;
export const ModalWindow = styled.div`
  position: fixed;
  top: 7vh;
  left: 50%;
  transform: translateX(-50%);
  display: flex;
  flex-direction: column;
  width: 60%;
  background: white;
  z-index: 12;
`;
export const Footer = styled.footer`
  display: flex;
  justify-content: space-between;
  align-self: flex-end;
  width: 100%;
  padding: 15px;
  margin-top: auto;
  border-top: 1px solid ${LIGHT_GRAY};
`;
export const MainContent = styled.main`
  display: flex;
  flex-direction: column;
  width: 100%;
  padding: 15px;
`;
export const Header = styled.header`
  display: flex;
  justify-content: space-between;
  align-self: flex-end;
  width: 100%;
  padding: 15px;
  margin-bottom: auto;
  border-bottom: 1px solid ${LIGHT_GRAY};
`;
