import React from 'react';
import PropTypes from 'prop-types';

import Button from './styledComponents/Button';
import H2 from './styledComponents/H2';
import Portal from '../../components/Portal';
import { ModalBackground, ModalWindow, Header, MainContent, Footer } from './styledComponents/StyledComponents';

const ModalConfirmationFunc = ({ header, children, onCancel, onOk }) => ( // eslint-disable-line react/prop-types
  <React.Fragment>
    <ModalBackground onClick={onCancel}/>
    <ModalWindow>
      <Header>
        <H2>{header}</H2>
        <Button onClick={onCancel}>
          <span aria-label="close modal window" role="img">╳</span>
        </Button>
      </Header>
      <MainContent>
        {children}
      </MainContent>
      <Footer>
        <Button onClick={onCancel}>Cancel</Button>
        <Button onClick={() => { onOk(); onCancel(); }}>Ok</Button>
      </Footer>
    </ModalWindow>
  </React.Fragment>
);
const ModalConfirmation = (props) => {
  const { header, children, onCancel, onOk } = props;
  return (
    <Portal>
      {props.on ? ModalConfirmationFunc({ header, children, onCancel, onOk }) : <div />}
    </Portal>
  );
};


ModalConfirmation.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.string,
  ]),
  header: PropTypes.string,
  onCancel: PropTypes.func,
  onOk: PropTypes.func,
  on: PropTypes.bool,
};

export default ModalConfirmation;
