import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import LabeledInput from '../LabeledInput/index';
import { setupToken } from '../../containers/SiteApp/actions';

// import request from '../../utils';


class Login extends Component {
  state = {
    login: '',
    password: '',
  };

  submitHandler = (e) => {
    const { onSetupToken } = this.props;
    e.preventDefault();
    // __DEV__&&console.warn("submit");
    onSetupToken({ login: e.target.elements[0].value, password: e.target.elements[1].value });
  };
  onChangeHandler = (e) => { // eslint-disable-line
    // __DEV__&&console.log("onChangeHandler e", e.target.value);
  };

  render() {
    const { login, password } = this.state;
    return (
      <React.Fragment>
        <h1>Login component</h1>
        <form onSubmit={this.submitHandler}>
          <LabeledInput
            name="login"
            label="login"
            value={login}
            // ref={(val) => {
            //   this.login = val;
            // }}
            onChange={this.onChangeHandler}
          />
          <LabeledInput
            type="password"
            name="password"
            label="password"
            value={password}
            // ref={(val) => {
            //   this.password = val;
            // }}
            onChange={this.onChangeHandler}
          />

          <input type="submit" value="submit"/>
        </form>
      </React.Fragment>
    );
  }
}

Login.propTypes = {
  onSetupToken: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => ({
  onSetupToken: data => dispatch(setupToken(data)),
});

export default connect(null, mapDispatchToProps)(Login);
