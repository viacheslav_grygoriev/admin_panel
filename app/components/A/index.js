/**
 * A link to a certain page, an anchor tag
 */
import styled from 'styled-components';
import { WHITE, WHITE_LINK, WHITE_LINK_HOVER, BLACK_LINK, BLACK_LINK_HOVER } from 'utils';

export default styled.a`
  text-decoration: none;
  cursor: pointer;
  color: ${props =>
    props[WHITE]
      ? WHITE_LINK
      : BLACK_LINK};
  &:hover {
    color: ${props =>
    props[WHITE]
      ? WHITE_LINK_HOVER
      : BLACK_LINK_HOVER};
  }
`;
