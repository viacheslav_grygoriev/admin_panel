import styled, { css } from 'styled-components';
import { ERROR_COLOR, GREEN } from 'utils';

export default styled.span`
  ${props => props.sortable && css`
    text-decoration: underline dotted;
    cursor: pointer;
    position: relative;
    margin-right: 15px;
    &:hover {
      text-decoration-style: solid;
    }
  `}
  ${props => props.active && css`
    text-decoration-style: solid;
  `}
  ${props => (props.active && props.orderBy) && css`
    &:after {
      text-decoration: none;
      position: absolute;
    }
  `}
  ${props => (props.active && props.orderBy === 'Asc') && css`
    &:after {
      content: "▲";
      color: ${ERROR_COLOR};
    }
  `}
  ${props => (props.active && props.orderBy === 'Desc') && css`
    &:after {
      content: "▼";
      color: ${GREEN};
    }
  `}    
`;
