import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import emailValidator from 'email-validator';

import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form/immutable';
import { propTypes, clearFields } from 'redux-form';
import { createStructuredSelector } from 'reselect';

import SelectField from 'components/SelectField/index';
import {
  errorSelector,
  loadingSelector,
  countriesSelector,
  statesSelector,
} from 'containers/AddNew/selectors';
import { sendCountriesRequest, addNewForm } from 'containers/AddNew/actions';
import Loading from 'components/Loading';
import RenderField from 'components/RenderField';
import { formSubmit } from 'utils';

import Button from './styledComponents/Button';
import P from '../SelectField/styledComponents/P';

const FlexWrapper = styled.div`
  margin-top: 20px;
  text-align: right;
`;

class UserRegisterForm extends React.Component {
  componentDidMount() {
    const { countriesRequest } = this.props;
    countriesRequest();
  }
  componentWillReceiveProps(nextProps) {
    const { states, unregisterFormField } = nextProps;
    if (!states || states.length === 0) {
      unregisterFormField('UserRegisterForm', false, false, 'state');
    }
  }
  render() {
    const {
      handleSubmit,
      pristine,
      reset,
      submitting,
      valid,
      anyTouched,
      errorSendData,
      loading,
      countriesArray,
      states,
    } = this.props;

    if (loading) return <Loading />;
    return (
      <form onSubmit={handleSubmit}>
        <Field
          name="username"
          type="text"
          component={RenderField}
          label="Username"
          anyTouched={anyTouched}
        />
        <Field
          name="email"
          type="email"
          component={RenderField}
          label="Email"
          anyTouched={anyTouched}
        />
        <Field
          name="password"
          type="password"
          component={RenderField}
          label="Password"
          anyTouched={anyTouched}
        />
        <Field
          name="confirmPassword"
          type="password"
          component={RenderField}
          label="Confirm password"
          anyTouched={anyTouched}
        />
        <Field
          name="country"
          component={SelectField}
          label="Country"
          items={countriesArray}
          firstOptionName="Select your country"
          anyTouched={anyTouched}
        />
        {states &&
          states.length !== 0 && (
          <Field
            name="state"
            component={SelectField}
            label="State"
            items={states}
            firstOptionName="Select your state"
            anyTouched={anyTouched}
          />
        )}
        <Field
          name="birthdate"
          type="date"
          component={RenderField}
          label="Date of birth"
          anyTouched={anyTouched}
        />
        <FlexWrapper>
          {errorSendData && <P>{errorSendData}</P>}
          <Button blue type="submit" disabled={!valid || submitting}>
            Submit
          </Button>
          <Button
            type="button"
            disabled={pristine || submitting}
            onClick={reset}
          >
            Clear Values
          </Button>
        </FlexWrapper>
      </form>
    );
  }
}

UserRegisterForm.propTypes = {
  ...propTypes,
  errorSendData: PropTypes.string.isRequired,
  loading: PropTypes.bool.isRequired,
  countriesRequest: PropTypes.func.isRequired,
  countriesArray: PropTypes.array,
  states: PropTypes.array,
};

const mapStateToProps = createStructuredSelector({
  errorSendData: errorSelector(),
  loading: loadingSelector(),
  countriesArray: countriesSelector(),
  states: statesSelector(),
});

const mapDispatchToProps = dispatch => ({
  countriesRequest: () => dispatch(sendCountriesRequest()),
  unregisterFormField: (form, keepTouched, persistentSubmitErrors, field) => dispatch(clearFields(form, keepTouched, persistentSubmitErrors, field)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(reduxForm({
  form: 'UserRegisterForm',
  validate,
  onSubmit: formSubmit(addNewForm),
})(UserRegisterForm));

function validate({
  username,
  email,
  password = '',
  confirmPassword,
  country,
  state,
  birthdate,
}) {
  const errors = {};
  const matchPattern = password.match(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{6,}$/);

  // username validation
  if (!username) errors.username = 'Username is required';

  // email validation
  if (!email) errors.email = 'Email is required';
  else if (!emailValidator.validate(email)) errors.email = 'Invalid email';

  // password validation
  if (!password) errors.password = 'Password is required';
  else if (!matchPattern) { errors.password = 'Password should consist of minimum 6 characters, at least one letter, one number and one special character'; }

  // password confirmation validation
  if (password && !confirmPassword && matchPattern) errors.confirmPassword = 'Password confirmation is required';
  else if (password !== confirmPassword) errors.confirmPassword = 'Passwords should be equal';

  // country validation
  if (!country) errors.country = 'Choose your country';

  // state validation
  if (country && !state) errors.state = 'Choose your state';

  // birth date validation
  if (!birthdate) errors.birthdate = 'Birth date is required';

  return errors;
}
