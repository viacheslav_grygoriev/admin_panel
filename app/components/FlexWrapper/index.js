import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';

const FlexWrapper = ({ className, children, flexDirection = 'row', justifyContent = 'flex-start', error, alignItems = 'flex-start' }) => {
  const Wrapper = styled.div`
  display: flex;
  flex-direction: ${flexDirection};
  justify-content: ${justifyContent};
  align-items: ${alignItems};
  ${() => error && css`
    outline: 1px solid red;
  `}  
`;
  return (
    <Wrapper className={className}>
      {children}
    </Wrapper>
  );
};

FlexWrapper.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.array,
  ]),
  className: PropTypes.string,
  flexDirection: PropTypes.string,
  justifyContent: PropTypes.string,
  alignItems: PropTypes.string,
  error: PropTypes.bool,
};

export default FlexWrapper;
